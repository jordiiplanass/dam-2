using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformCreator : MonoBehaviour
{
    [SerializeField] public GameObject[] platformPrefab;
    [SerializeField] public Transform referencePoint;
    [SerializeField] private GameObject lastCreatedPlatform;
    [SerializeField] float spaceBetweenPlatforms = 4;
    float lastPlatformWidth;
    // Start is called before the first frame update
    void Start()
    {
        //lastCreatedPlatform = Instantiate(platformPrefab, referencePoint.position, Quaternion.identity);   
    }

    // Update is called once per frame
    void Update()
    {
        if (lastCreatedPlatform.transform.position.x < referencePoint.position.x)
        {
            int randomNum = Random.Range(0, platformPrefab.Length);
            Vector3 targetCreationPoint = new Vector3(referencePoint.position.x + lastPlatformWidth + spaceBetweenPlatforms, 0, 0);
            lastCreatedPlatform = Instantiate(platformPrefab[randomNum], targetCreationPoint, Quaternion.identity);
            BoxCollider2D collider = lastCreatedPlatform.GetComponent<BoxCollider2D>();
            lastPlatformWidth = collider.bounds.size.x;
        }
    }
}
