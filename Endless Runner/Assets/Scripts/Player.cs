using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] public Rigidbody2D rb;
    [SerializeField] public float jump = 5;
    [SerializeField] Transform raycastOrigin;
    bool isJump;
    bool isGrounded;
    [SerializeField] public float distanceTravelled;
    [SerializeField] public int moneyCount = 0;
    [SerializeField] UIController uiController;
    [SerializeField] Animator anim;
    float lastYPos;
    // Start is called before the first frame update
    void Start()
    {
        lastYPos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        CheckForInput();
        if (transform.position.y < lastYPos){
            anim.SetBool("Falling", true);
        } else {
            anim.SetBool("Falling", false);
        }
        lastYPos = transform.position.y;
    }
    void FixedUpdate() {
        distanceTravelled += Time.deltaTime;
        CheckForGrounded();
        if (isJump){
            isJump = false;
            rb.AddForce(new Vector2(0,jump), ForceMode2D.Impulse);
        }
    }

    void CheckForInput(){
    if (isGrounded){
            if (Input.GetKeyDown(KeyCode.Space)){
                isJump = true;
                anim.SetTrigger("Jump");
            }
        }
    }
    void CheckForGrounded(){
        RaycastHit2D hit = Physics2D.Raycast(raycastOrigin.position, Vector2.down);
        // Debug.Log(hit.transform.name);
        if (hit.collider != null){
            if (hit.distance <= 0.1f){
                isGrounded = true;
                anim.SetBool("IsGrounded", true);
            } else {
                isGrounded = false;
                anim.SetBool("IsGrounded", false);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision){
        if (collision.transform.CompareTag("Obstacle")){
            Debug.Log("Collided with obstacle");
            uiController.ShowGameOverScreen();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision){
        if (collision.transform.CompareTag("Collectable")){
            moneyCount++;
            Destroy(collision.gameObject);
        }
    }
}
