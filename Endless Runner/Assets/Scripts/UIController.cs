using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] Text distanceTravelled;
    [SerializeField] Text moneyCount;
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] Player player;

    private void Update(){
        if (Input.GetKeyDown(KeyCode.T)){
            ShowGameOverScreen();
        }
    }

    public void ShowGameOverScreen(){
        gameOverScreen.SetActive(true);
        distanceTravelled.text = Mathf.Ceil(player.distanceTravelled).ToString();
        Debug.Log(player.moneyCount);
        moneyCount.text = player.moneyCount.ToString();
    }
    public void GameRestart(){
        gameOverScreen.SetActive(false);
        SceneManager.LoadScene("EndlessRunner");
        player.moneyCount = 0;
    }
}