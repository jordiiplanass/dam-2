/*
* ConversionData.java
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation v3.
*
* @see <a href="http://www.gnu.org/licenses/gpl.html">GNU License</a> for more information.
*
* Copyright (c) Joan S�culi <a href="mailto:jseculi@escoladeltreball.org">Joan S�culi</a>
*/
package conversion;

import java.util.Scanner;

/**
* Enter a description
*
* @author  <a href="mailto:jseculi@escoladeltreball.org">Joan S�culi</a>
* @version 1.0
* @since   03/11/2021  
* 
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation v3.
*
* @see <a href="http://www.gnu.org/licenses/gpl.html">GNU License</a> for more information.
*
*
*/
public class ConversionData {
	public static final byte BYTES_TO_BITS = 8;
	public static final short KB_TO_BYTES = 1024;
	public static final int MB_TO_BYTES = KB_TO_BYTES * 1024;   //1_048_576
	public static final int GB_TO_BYTES = MB_TO_BYTES * 1024;  	//1_073_741_824
	public static final long TB_TO_BYTES = GB_TO_BYTES * 1024L; //1_099_511_627_776
	public static final long PB_TO_BYTES = TB_TO_BYTES * 1024L;	//Petabyte  //1_125_899_906_842_624
	public static final long EB_TO_BYTES = PB_TO_BYTES * 1024L;	//Exabyte   //1_152_921_504_606_846_976
	/**
	* The main method test all methods of the class
	*
	* @author  <a href="mailto:jseculi@escoladeltreball.org">Joan S�culi</a>
	* @version 1.0
	* @since   03/11/2021 
	* @param args 	args are not used 
	* @return void
	*
	*/
	public static void main(String[] args) {

		System.out.println("Conversion values:");
		System.out.printf("KB: %,d Bytes%n" , KB_TO_BYTES);
		System.out.printf("MB: %,d Bytes%n" , MB_TO_BYTES);
		System.out.printf("GB: %,d Bytes%n" , GB_TO_BYTES);
		System.out.printf("TB: %,d Bytes%n" , TB_TO_BYTES);
		System.out.printf("PB: %,d Bytes%n" , PB_TO_BYTES);
		System.out.printf("EB: %,d Bytes%n%n" , EB_TO_BYTES);
		
		
		System.out.println("9_126_805_504 = " + formatBytes(9126805504L)); // 8.5 GB 
	
		System.out.println("1_310_720 = " + formatBytes(1_048_576L + 262_144L)); // 1_310_720 = 1.25 MB 
		
		
		System.out.println("1_125_899_906_842_624: " + formatBytes(1024L * 1024L * 1024L * 1024L * 1024L)); //1.0 PB
		
		
		/*
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number of bytes: ");
		Long number = scanner.nextLong();
		
		System.out.println(number + ": " + formatBytes(number));
		scanner.close();*/
	}
	public static String formatBytes(long bytes) {
		String sortida = "too big";
		if (bytes < 0) sortida = "Entry can't be negatives";
		else if (bytes >= 0 && bytes < KB_TO_BYTES) sortida = bytes + "B";
		else if (bytes >= KB_TO_BYTES && bytes < MB_TO_BYTES) sortida = bytes / KB_TO_BYTES + " KB";
		else if (bytes >= MB_TO_BYTES && bytes < GB_TO_BYTES) sortida = bytes / MB_TO_BYTES + " MB";
		else if (bytes >= GB_TO_BYTES && bytes < TB_TO_BYTES) sortida = bytes / GB_TO_BYTES + " GB";
		else if (bytes >= TB_TO_BYTES && bytes < PB_TO_BYTES) sortida = bytes / TB_TO_BYTES + " TB";
		else if (bytes >= PB_TO_BYTES && bytes < EB_TO_BYTES) sortida = bytes / PB_TO_BYTES + " PB";
		else if (bytes >= EB_TO_BYTES && bytes <= Long.MAX_VALUE) sortida = bytes / EB_TO_BYTES + " EB";
		return sortida;
	}


}
