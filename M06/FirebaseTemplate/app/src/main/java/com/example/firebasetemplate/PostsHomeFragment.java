package com.example.firebasetemplate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.firebasetemplate.databinding.FragmentPostsBinding;
import com.example.firebasetemplate.databinding.ViewholderPostBinding;
import com.example.firebasetemplate.model.Post;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class PostsHomeFragment extends AppFragment {

    private FragmentPostsBinding binding;
    private List<Post> postList = new ArrayList<>();



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentPostsBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.fab.setOnClickListener(v -> navController.navigate(R.id.newPostFragment));
        PostsAdapter adapter;
        binding.postsRecyclerView.setAdapter(adapter = new PostsAdapter());
        FirebaseFirestore.getInstance().collection("posts").addSnapshotListener((collectionSnaphot, e) -> {
           for (DocumentSnapshot documentSnapshot : collectionSnaphot){
               postList.add(documentSnapshot.toObject(Post.class));
           }
           adapter.notifyDataSetChanged();
        });

    }
    class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder>{

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(ViewholderPostBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.binding.contenido.setText(postList.get(position).content);
            holder.binding.autor.setText(postList.get(position).authorName);
        }

        @Override
        public int getItemCount() {
            return 0;
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            ViewholderPostBinding binding;
            public ViewHolder(@NonNull ViewholderPostBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}