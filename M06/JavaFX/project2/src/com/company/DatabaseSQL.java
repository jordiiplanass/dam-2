package com.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DatabaseSQL implements Database {
    private Connection conn;


    @Override
    public void connect() throws CannotConnectException {
        try{
         conn = DriverManager
                 .getConnection("jdbc:mysql://localhost/mydatabase?user=myuser&password=mypass");
        } catch (Exception e){
            throw new CannotConnectException();
        }
    }

    @Override
    public Stream<Movie> searchMovies() {

        List<Movie> movies = new ArrayList<>();
        try {
            ResultSet res = conn.createStatement().executeQuery("SELECT * FROM movies");
            while (res.next()) movies.add(new Movie(res.getString("title")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return movies.stream();
    }


    @Override
    public void insertMovie(String title) {
        try {
            PreparedStatement statement = conn.prepareStatement("INSERT INTO movies(title) VALUES(?)");
            statement.setString(1, title);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateMovie(String title, String newTitle){
        try {
            PreparedStatement statement = conn.prepareStatement("UPDATE movies SET title = (?) WHERE title = (?)");
            statement.setString(1, newTitle);
            statement.setString(2, title);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteMovie(String title) {
        try {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM movies WHERE title = (?)");
            statement.setString(1, title);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
