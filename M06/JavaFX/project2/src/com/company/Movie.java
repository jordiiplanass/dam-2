package com.company;

import org.bson.Document;

public class Movie {
    String title;

    public Movie(String title) {
        this.title = title;
    }

    static Movie fromDoc(Document doc) {
        return new Movie(doc.getString("title"));
    }

     static Document toDoc(Movie movie) {
        return new Document().append("title", movie.title);
    }
}
