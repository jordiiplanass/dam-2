/**
 * 27 sept. 2021
 * DataTree.java
 * Jordi Planas iam39426921
 * ---------------------------
 * Enunciat:
 * Fes un programa que rebi 
 * com a argument un directori 
 * i organitzi els seus fitxers 
 * niats en directoris segons l'any,
 * mes i dia de la seva data de l'última modificació.
 */

package Paths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

public class DataTree {

	public static void main(String[] args) throws IOException {
		Path dir = Paths.get("/tmp/directori");
		
		// Fem un flatten dels arxius
		
		// Movem tots els arxius a directori base
		Files.walk(dir)
			.filter(Files::isRegularFile)
			.forEach( file -> {
				try {
					Files.move(file, dir.resolve(file.getFileName()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		// Esborrars els directoris vuits que han quedat.
		Files.walk(dir)
			.filter(Files::isDirectory)
			.sorted(Comparator.reverseOrder())
			.forEach(direc -> {
				try {
					if (!direc.equals(dir)) {
						Files.delete(direc);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		// crear els directoris de date i movem els arxius a la seva data corresponem
		Files.walk(dir)
			.filter(Files::isRegularFile)
			.forEach( file -> {
				try {
					// emmagatzemar la data del arxiu en una variable tipus LocalDateTime de file
					LocalDateTime time = LocalDateTime.parse(Files.getLastModifiedTime(file).toString(), DateTimeFormatter.ISO_DATE_TIME);
					// Emmagatzemem el path en ordre descendent de la data del arxiu file
					Path nou_directori = Paths.get(dir.toString() + "/" + time.getYear() + "/" + time.getMonthValue() + "/" + time.getDayOfMonth());
					// Creem els directoris
					Files.createDirectories(nou_directori);
					// Movem els arxius als nous directoris
					Files.move(file, nou_directori.resolve(file.getFileName()));
				} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		
	}
}
