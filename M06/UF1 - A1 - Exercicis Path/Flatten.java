/**
 * 27 sept. 2021
 * Flatten.java
 * Jordi Planas iam39426921
 * ---------------------------
 * Enunciat:
 * Fes un programa que rebi com a argument la 
 * ruta d'un directori i "aplani" (flatten) el 
 * seu contingut, és a dir, que mogui a aquest 
 * mateix directori tots els fitxers que tingui 
 * niats. 
 * També haurà d'esborrar tots els directoris niats. 
 */
package Paths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class Flatten {
	public static void main(String[] args) throws IOException {
		/*
		 * Fes un programa que rebi com a argument la 
		 * ruta d'un directori i "aplani" (flatten) el 
		 * seu contingut, és a dir, que mogui a aquest 
		 * mateix directori tots els fitxers que tingui 
		 * niats. 
		 * També haurà d'esborrar tots els directoris niats. 
		 * Per exemple, si executem el programa amb 
		 * aquest directori "niats":
		 */
		
		Path path = Paths.get("/tmp/niats");
		// Moure l'arxiu en el pare de tots
		Files.walk(path)
			.filter(Files::isRegularFile)
			.forEach( file -> {
				try {
					Files.move(file, path.resolve(file.getFileName()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		// Esborrars els directoris vuits que han quedat.
		Files.walk(path)
			.filter(Files::isDirectory)
			.sorted(Comparator.reverseOrder())
			.forEach(direc -> {
				try {
					if (!direc.equals(path)) {
						Files.delete(direc);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});

		
	}
	
}
