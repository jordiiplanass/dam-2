/**
 * 27 sept. 2021
 * Treediff.java
 * Jordi Planas iam39426921
 * ---------------------------
 * Enunciat:
 * Fes un programa que rebi com a arguments dos directoris 
 * (DirectoriA i DirectoriB). El programa haura de comparar 
 * els fitxers niats (nested) dels dos directoris
 */
package Paths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class Treediff {
	// variable creada per a emmagatzemar si dos hash son iguals
	static boolean esRepeteix = false;
	// variable creada per a emmagatzemar si dos Path son iguals
	static boolean esRepeteixNom = false;
	
	public static void main(String[] args) throws IOException {
		
		
		Path directoriA = Paths.get("/tmp/papopepo/dirA");
		Path directoriB = Paths.get("/tmp/papopepo/dirB");
		
		
		
		// 1  Els fitxers del DirectoriA que no estan al DirectoriB
		
		/*
		 * Plantejament:
		 * 		Bucle doble, en el primer iterem tots els fitxers de directoriA,
		 * 		en el segon bucle, comparem cadascun dels fitxers de directoriB
		 *  	amb fileA utilitzant el hash dels fitxers, si no forma part del directoriB, 
		 *  	es mostra per pantalla.
		 */
		
		System.out.println("1  Els fitxers del DirectoriA que no estan al DirectoriB");
		
		Files.walk(directoriA)
			.filter(Files::isRegularFile)
			.forEach( fileA -> {
	
				esRepeteix = false; // si troba l'arxiu (hash), es modifica el valor de esRepeteix.
									// default false per cada arxiu de directoriA
				try {
					byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileA));
					// recorrem  dirB per comparar el seu contingut amb fileA
					try {
						Files.walk(directoriB)
						.filter(Files::isRegularFile)
						.forEach( fileB -> {
								try {
									
									byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileB));
							
									if (Arrays.equals(hash1, hash2)) esRepeteix = true;
									
								} catch (NoSuchAlgorithmException | IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});
						} catch (IOException e) {
							e.printStackTrace();
						}
					
					// diferent
					if (!esRepeteix) System.out.println(fileA);
					
				} catch (NoSuchAlgorithmException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			});
		
		// 2 Els fitxers del DirectoriA que estan al DirectoriB pero en una altra ruta
		
		/*
		 * Plantejament:
		 * 		Bucle doble, en el primer iterem tots els fitxers de directoriA,
		 * 		en el segon bucle, comparem cadascun dels fitxers de directoriB
		 *  	amb fileA utilitzant el hash dels fitxers i el directori del fitxer, 
		 *  	si forma part del directoriB amb un directori diferent es mostra per pantalla.
		 */
		
		System.out.println("2 Els fitxers del DirectoriA que estan al DirectoriB pero en una altra ruta");
		
		Files.walk(directoriA)
			.filter(Files::isRegularFile)
			.forEach( fileA -> {
				
				esRepeteixNom = false;	// si troba l'arxiu (Path), es modifica el valor de esRepeteix.
										// default false per cada arxiu de directoriA
				
				esRepeteix = false; 	// si troba l'arxiu (hash), es modifica el valor de esRepeteix.
				
				try {
					
					byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileA));
					
					Path nom1 = directoriA.relativize(fileA);
					
					try {
						
						Files.walk(directoriB)
						.filter(Files::isRegularFile)
						.forEach( fileB -> {
								try {
									
									byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileB));
									
									Path nom2 = directoriB.relativize(fileB);
									
									if (nom1.equals(nom2)) esRepeteixNom = true;
									
									if (Arrays.equals(hash1, hash2)) esRepeteix = true;
									
								} catch (NoSuchAlgorithmException | IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});
						} catch (IOException e) {
							e.printStackTrace();
						}
					// mateix hash i diferent Path
					if (esRepeteix & !esRepeteixNom) System.out.println(fileA);
					
				
				} catch (NoSuchAlgorithmException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			});
		
		// 3 Els fitxers del DirectoriB que no estan al DirectoriA
		
		/*
		 * Plantejament:
		 * 		Bucle doble, en el primer iterem tots els fitxers de directoriB,
		 * 		en el segon bucle, comparem cadascun dels fitxers de directoriA
		 *  	amb fileB utilitzant el hash dels fitxers, si no forma part del directoriA, 
		 *  	es mostra per pantalla.
		 */
		
		System.out.println("3 Els fitxers del DirectoriB que no estan al DirectoriA");
		Files.walk(directoriB)
			.filter(Files::isRegularFile)
			.forEach( fileB -> {
				
				esRepeteix = false; // si troba l'arxiu (hash), es modifica el valor de esRepeteix.
				
				try {
					
					byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileB));
					
					try {
						Files.walk(directoriA)
						.filter(Files::isRegularFile)
						.forEach( fileA -> {
								try {
									
									byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileA));
									
									if (Arrays.equals(hash1, hash2)) esRepeteix = true;
									
								} catch (NoSuchAlgorithmException | IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});
						} catch (IOException e) {
							e.printStackTrace();
						}
					if (!esRepeteix) System.out.println(fileB);
					
				
				} catch (NoSuchAlgorithmException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			});
		
		// 4. Els fitxers del DirectoriB que estan al DirectoriA però en una altra ruta
		
		/*
		 * Plantejament:
		 * 		Bucle doble, en el primer iterem tots els fitxers de directoriB,
		 * 		en el segon bucle, comparem cadascun dels fitxers de directoriA
		 *  	amb fileB utilitzant el hash dels fitxers i el directori del fitxer, 
		 *  	si forma part del directoriA amb un directori diferent es mostra per pantalla.
		 */
		
		System.out.println("4. Els fitxers del DirectoriB que estan al DirectoriA però en una altra ruta");
		Files.walk(directoriB)
			.filter(Files::isRegularFile)
			.forEach( fileB -> {
				
				esRepeteixNom = false;	// si troba l'arxiu (Path), es modifica el valor de esRepeteix.
										// default false per cada arxiu de directoriA

				esRepeteix = false; 	// si troba l'arxiu (hash), es modifica el valor de esRepeteix.
				
				try {
					byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileB));
					Path nom1 = directoriB.relativize(fileB);
					try {
						Files.walk(directoriA)
						.filter(Files::isRegularFile)
						.forEach( fileA -> {
								try {
									byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileA));
									Path nom2 = directoriA.relativize(fileA);
									if (nom1.equals(nom2)) esRepeteixNom = true;
									
									if (Arrays.equals(hash1, hash2)) esRepeteix = true;
									
								} catch (NoSuchAlgorithmException | IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});
						} catch (IOException e) {
							e.printStackTrace();
						}
					
					if (esRepeteix & !esRepeteixNom) System.out.println(fileB);
				
				} catch (NoSuchAlgorithmException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			});
		// 5 Els fitxers que estan a la mateixa ruta al DirectoriA i al DirectoriB	
		
		/*
		 * Plantejament:
		 * 		Bucle doble, en el primer iterem tots els fitxers de directoriA,
		 * 		en el segon bucle, comparem cadascun dels fitxers de directoriB
		 *  	amb fileA utilitzant el directori del fitxer, 
		 *  	si formen part del mateix directori relatiu, es mostra per pantalla
		 */
		
		System.out.println("5 Els fitxers que estan a la mateixa ruta al DirectoriA i al DirectoriB	");
		Files.walk(directoriA)
			.filter(Files::isRegularFile)
			.forEach( fileA -> {
				
				esRepeteixNom = false;
				
				Path nom1 = directoriA.relativize(fileA);
				
				try {
					Files.walk(directoriB)
					.filter(Files::isRegularFile)
					.forEach( fileB -> {
							Path nom2 = directoriB.relativize(fileB);
							
							if (nom1.equals(nom2)) esRepeteixNom = true;
							
						});
					} catch (IOException e) {
						e.printStackTrace();
					}
				
				if (esRepeteixNom) System.out.println(fileA);
				
			});
		
		
	}
}
