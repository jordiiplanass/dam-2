import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Fuse {
	public static void main(String[] args) throws IOException {
			
		Path path = Paths.get(args[0]);
		
		int numParticions = 0;
		Path path_part = Paths.get(path.toString() + ".part." + numParticions);
		
		while (Files.exists(path_part)) {
			numParticions++;
			path_part = Paths.get(path.toString() + ".part." + numParticions);
		}
		
		InputStream input;
		
		OutputStream os = Files.newOutputStream(path);
			
		for (int i = 0; i < numParticions ; i++) {	
			input = Files.newInputStream(Paths.get(path.toString() + ".part." + i));
			for (int a ; (a = input.read()) != -1 ; ) {
				os.write(a);
			}
		}
		os.close();
		System.out.println("Fuse done!");
			
	}
}
