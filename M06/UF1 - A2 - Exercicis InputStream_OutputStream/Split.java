import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Split {
	public static void main(String[] args) throws IOException{
		
		Path path = Paths.get(args[0]);
		int numParticions = Integer.parseInt(args[1]);
		
		
		int i = 0;
		Path file = Paths.get(path.toString() + ".part." + i);		
		OutputStream os = Files.newOutputStream(file);		
		
		InputStream input = Files.newInputStream(path);
		
		int fileSize = input.available();
		int partSize = (int) Math.ceil(fileSize / numParticions);
		int count = 0;
		
		for (int a ; (a = input.read()) != -1 ; ) {
		
			count++;
			if (count > partSize) {
				count = 0;
				i++;	
				file = Paths.get(path.toString() + ".part." + i);
				os = Files.newOutputStream(file);
			}
			os.write(a);
		}
		os.close();
		System.out.println("Split done!");
		
	}
	
}
