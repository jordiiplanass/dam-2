package main;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.ArrayList;

class Contact{
    public String name;
    public String phone;
    public String mail;

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getMail() {
        return mail;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }


}
class Agenda{
    ArrayList<Contact> contactos;

    public Agenda(ArrayList<Contact> contactos) {
        this.contactos = contactos;
    }

    public ArrayList<Contact> getContactos() {
        return contactos;
    }

    public void add(String name, String phone, String mail){
        //  Afegeix el contacte a l'agenda.
        //  Si el <contact_name> ja està a l'agenda, el sobreescriu.
        Contact contacto = new Contact();
        contacto.name = name;
        contacto.phone = phone;
        contacto.mail = mail;
        if (searchName(contacto.name) > 0) del(name);
        contactos.add(contacto);
    }
    public void del(String name){
        int i = searchName(name);
        if (i > 0) contactos.remove(contactos.get(i));
    }
    public void list(){
        for (Contact e : contactos){
            System.out.println(e.toString());
        }
    }
    public int search(String mail){
        for (int i = 0; i > contactos.size() ; i++){
            if (contactos.get(i).name.equals(mail)){
                return i;
            }
        }
        return -1;
    }
    public int searchName(String nom){
        for (int i = 0; i > contactos.size() ; i++){
            if (contactos.get(i).name.equals(nom)){
                return i;
            }
        }
        return -1;
    }
}
public class json1 {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Path json = Paths.get("file.json");
        Agenda agenda = objectMapper.readValue(json.toFile(), Agenda.class); // json --> agenda


        if (args[0].equals("add")){

            if (args.length != 4){
                System.out.println("Enter a correct amount of arguments");
                return;
            }
            agenda.add(args[1], args[2], args[3]); // modify agenda
            objectMapper.writeValue(json.toFile(), agenda); // agenda --> json

        } else if (args[0].equals("del")){

            if (args.length != 2){
                System.out.println("Enter a correct amount of arguments");
                return;
            }
            agenda.del(args[1]);
            objectMapper.writeValue(json.toFile(), agenda);

        } else if (args[0].equals("list")){

            if (args.length != 1){
                System.out.println("Enter a correct amount of arguments");
                return;
            }
            agenda.list();

        } else if (args[0].equals("search")){

            if (args.length != 2){
                System.out.println("Enter a correct amount of arguments");
                return;
            }
            agenda.search(args[1]);

        }
        //String s = objectMapper.writeValueAsString(contact);
    }
}
