package main;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;

// Obtenir la llista de llibres de cada autor
// Obtenir la llista de llibres de cada biblioteca

class Library{
    String name;
    String city;
    List<Book> bookList;

    public Library(String name, String city, List<Book> bookList) {
        this.name = name;
        this.city = city;
        this.bookList = bookList;
    }
}
class Book{
    String title;

    public Book(String title) {
        this.title = title;
    }
}
class Author{
    String name;
    String birthday;
    List<Book> bookList;

    public Author(String name, String birthday, List<Book> bookList) {
        this.name = name;
        this.birthday = birthday;
        this.bookList = bookList;
    }


}
class Genre{
    String title;
}
public class json2 {
    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        Path json = Paths.get("file.json");

        // Genero els llibres
        Book book1 = new Book("El color de la magia");
        Book book2 = new Book("Mort");
        Book book3 = new Book("Ritos iguales");
        Book book4 = new Book("Brujerías");

        // Creo un llistat de llibres
        List<Book> list1 = new ArrayList<>();
        list1.add(book1);
        list1.add(book2);
        list1.add(book3);
        list1.add(book4);

        // Creo l'autor
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime birth1 = LocalDateTime.parse("1948-04-28", formatter);

        Author author1 = new Author("Terry Pratchett", birth1.format(formatter), list1);

        // Creo la biblioteca
        Library library1 = new Library("Biblioteca de l'Eixample", "Barcelona", list1);

    }

}
