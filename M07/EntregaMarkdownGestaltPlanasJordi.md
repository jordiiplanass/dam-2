# TASCA 2: Exercici Markdown + Lleis Gestalt

Heu de fer un document de resum de les [Lleis de Gestalt aplicades al Disseny d'interfícies](https://moodle.escoladeltreball.org/mod/page/view.php?id=137289). Per cada una de les lleis de Gestalt feu una breu explicació, amb les vostres paraules, i busqueu una imatge que l'il·lustri i afegiu-la al document.

Al final adjunteu la webgrafia que heu fet servir.

El document l'heu de fer amb Markdown. Heu d'entregar el fitxer .md i el .pdf generat. (Les imatges no cal)

---
# Lleis de Gestalt

Les lleis de Gestalt tenen un origen psicològic, defineixen com les persones percibim els elements visuals i com els organitzem. 

Les lleis de Gestalt son les següents: 

- Llei de la proximitat
- Llei de la similitud
- Llei de tancament
- Llei de la continuïtat
- Llei fons figura
- Llei de la simplicitat
- Llei de la simetria
- Llei de l'experiència

![gestaltImage](https://filesedc.com/uploads//other/2019/02/1200/las-teorias-de-la-psicologia-gestalt-explican-como-las-personas-percibimos-lo-que-nos-rodea-en-base-a-6-leyes-distintas.jpeg)

---
## Llei de proximitat

Segons la llei de Gestalt, quan menys distància hi hagi entre objectes similars, més tendim a interpretar-ho com una unitat.

![gestaltProximity](https://www.uxtoast.com/dist/img/gestalt.jpg)

Per exemple, en aquesta imatge es poden apreciar dos grups diferents de punts, que assimilem ja que uns estan agrupats a una distancia diferent dels altres i perque varien els colors.

---
## Llei de la similitud
Segons la llei de Gestalt, quan certs objectes son semblars en forma, color i tamany el nostre cervell tendeix a agrupar-los encara que estiguin separats. 

![gestaltSimilarity](https://lawsofux.com/law-of-similarity/social.png)

Aqui podem apreciar dos grups diferents de punts, diferenciats per colors diferents, encara que tots els punts estiguin agrupats a la mateixa distància un al costat del altre.

---
## Llei de tancament
La llei de tancament declara que el cervell tendeix a tancar objectes inacabats quan s'assimilen a algun objecte conegut.

![gestaltClosure](https://uxcam.com/blog/wp-content/uploads/2018/10/10-2.png)

En aquesta imatge podem apreciar una figura d'una rodona i d'una estrella, encara que no hi haci cap figura d'una rodona o estrella, son lineas que no estan connectades entre elles, el nostre cervell ho interrelaciona a una figura coneguda per nosaltres.

---
## Llei de la continuïtat
La llei de continuïtat declara que els objectes ordenats en una linia recta o curva, tendim a agrupar-los.

![gestaltContinuity](https://uploads.toptal.io/blog/image/125749/toptal-blog-image-1522045527423-29ef6bc680c8c526755e30e417215ad4.png)

L'exemple ens mostra dues possibles interpretacions d'un conjunt de cercles de dos colors diferents. La primera de totes es de dues linies de dos colors, una recta i l'altre curva. En aquesta interpretació s'aplica la llei de continuïtat.

---
## Llei fons figura
Aquesta llei aprofita que el nostre cervell tendeix a interpretar els espais vuits per a mostrar multiples imatges.

![gestaltFigureGround](https://uploads.toptal.io/blog/image/125754/toptal-blog-image-1522045566612-f2bf411128e75b613e10889135c54bb6.jpg)

Aqui podem reconèixer dues figures molt clares sobre la neu, la d'un llop i la d'un ànec, però aplicant la llei de fons de figura, ens adonem que l'espai que genera el llop a la neu dona forma al que seria el cap d'un èsser humà.

---
## Llei de la simplicitat
La llei de simplicitat declara que els humans interpretem els objectes de la forma més senzilla possible.

![gestaltSimplicity](https://media.istockphoto.com/vectors/snowflake-icon-iconic-series-vector-id885844174?k=20&m=885844174&s=612x612&w=0&h=BC-c4rR9xGYSTBMN6mN1QYg0nmCkXwTHkyRKjUrqyAc=)

Aquesta figura tant senzilla, la interpreten automàticament com un floc de neu, internament ja la autorrelacionem amb la neu i el fred, per molt que només sigui una figura geomètrica basica.

---
## Llei de la simetria
La llei de simetria indica que la simetria es percebuda pels humans de forma positiva, inconscientment més agradable a la vista que una imatge asimetrica.

![gestaltSymmetry](https://m.media-amazon.com/images/I/61l+HTCjCvL._AC_SL1500_.jpg)

En aquest logo es pot apreciar clarament la simetria.

---
## Llei de l'experiència
Segons els principis psicològics de Gestalt, determina que un humà es basa en les experiències passades per a determinar què percep.

![gestaltExperiente](https://lh4.googleusercontent.com/8ukEtIgBfSL7Lj8d4bsQrdF53On0-hrsVqM_HeuFWl0mNP6g_tjIgkuEHo_WYqrjaQW2OowP3iRnPoEsgs0hmVDlrtQuicn4wvxQd0PseAzskBObsHiB09Jj0xi2gYDuG8Tcq_7g=s0)

Aqui tenim un rectangle gris amb tres cercles col·locats a una certa distància un del altre, tots tres de colors diferents. Nosaltres, amb els nostres anys d'experiència, relacionem aquesta figura a la de un semàfor, encara que no tinguem cap contexte ni tampoc formi tota l'estructura d'un semàfor.

[//]: # (Les fonts que he utilitzat per a documentar-me i realitzar la pràctica)
   [fuente1]: <https://www.verywellmind.com/gestalt-laws-of-perceptual-organization-2795835>
   [fuente2]: <https://www.uxtoast.com/design-tips/gestalt-principles-in-ui>
   [fuente3]: <https://www.getfeedback.com/resources/ux/gestalt-laws-simplicity-symmetry-experience/#:~:text=The%20Law%20of%20Simplicity%20indicates,an%20advantage%20on%20a%20website.>

   
[///]: # (Comentari Final: He utilitzat imatges d'internet per a no tindre que adjuntar cap document adicional a l'entrega.)