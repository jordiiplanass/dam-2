package com.example.tasca5;

import java.util.stream.Stream;

public interface Database {

    void connect() throws com.example.tasca5.CannotConnectException;

    Stream<Movie> searchMovies();

    void insertMovie(String title);

    void updateMovie(String title, String newTitle);

    void deleteMovie(String title);


}
