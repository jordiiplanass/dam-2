package com.example.tasca5;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;


public class DatabaseMongo implements com.example.tasca5.Database {
    public static String uri = "mongodb://localhost";
    private MongoDatabase database;

    public void connect() throws com.example.tasca5.CannotConnectException {
        try {
            MongoClient mongoClient = MongoClients.create(uri);
            database = mongoClient.getDatabase("sampledb");
        } catch (Exception e){
            throw new com.example.tasca5.CannotConnectException();
        }
    }

    @Override
    public Stream<Movie> searchMovies() {
        return StreamSupport
                .stream(
                    database.getCollection("movies")
                        .find()
                        .projection(excludeId())
                            .spliterator(),false)
                .map(Movie::fromDoc);
    }

    @Override
    public void insertMovie(String title) {
        database.getCollection("movies").insertOne(Movie.toDoc(new Movie(title)));
    }



    @Override
    public void updateMovie(String title, String newTitle) {
        database.getCollection("movies")
                .updateMany(eq("title", title), new Document("$set", new Document("title", newTitle)));

    }

    @Override
    public void deleteMovie(String title) {
        database.getCollection("movies").deleteMany(eq("title", title));
    }
}
