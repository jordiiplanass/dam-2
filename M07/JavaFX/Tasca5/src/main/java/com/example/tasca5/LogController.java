package com.example.tasca5;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

import static java.lang.Thread.sleep;

public class LogController {
    @FXML
    private TextField textField1;

    @FXML
    private TextField textField2;

    @FXML
    private Button logButton;

    @FXML
    private Button register;



    public void onLogin(ActionEvent actionEvent) {
        // TODO -- check log In
        if (textField1.getText().length() > 0
                && textField2.getText().length() > 0
        ) {
            // Per a simular que comprova si existeix l'usuari
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main-view.fxml"));
            Scene scene;
            try {
                Stage stage2 = (Stage) logButton.getScene().getWindow();
                stage2.close();
                scene = new Scene(loader.load(), 1800, 800);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Introduce los campos correspondentes");
            alert.setTitle("Log In");
            alert.showAndWait();
        }
    }
    public void onRegister(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("register-view.fxml"));
        Scene scene;
        try {
            scene = new Scene(loader.load(),266 , 361);
            Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow() ;
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}