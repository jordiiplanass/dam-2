package com.example.tasca5;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        System.out.println("BASE DE DATOS \nmongoDB / sql?");
        String dbType = scanner.nextLine();

        com.example.tasca5.Database db = null;

        if ("mongoDB".equalsIgnoreCase(dbType)){
            db = new com.example.tasca5.DatabaseMongo();
        } else if ("sql".equalsIgnoreCase(dbType)){
            db = new com.example.tasca5.DatabaseSQL();
        }

        try {
            db.connect();
        } catch (com.example.tasca5.CannotConnectException e) {
            System.out.println("Cannot connect to database");
            return;
        }
        test(db);

    }
    private static void test(com.example.tasca5.Database db) {
        System.out.println("Search");
        db.searchMovies().forEach(movie -> {System.out.println(movie.title);});
        db.insertMovie("Pepe");
        System.out.println("A");
        db.searchMovies().forEach(movie -> {System.out.println(movie.title);});
        db.updateMovie("Pepe", "Pepa");
        System.out.println("B");
        db.searchMovies().forEach(movie -> {System.out.println(movie.title);});
        db.deleteMovie("Pepa");
        System.out.println("C");
        db.searchMovies().forEach(movie -> {System.out.println(movie.title);});
    }
}

