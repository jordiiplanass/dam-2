package com.example.tasca5;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class RegisterView {
    @FXML
    private TextField r_userEntry;
    @FXML
    private TextField r_mailEntry;
    @FXML
    private PasswordField r_mailConfirm;
    @FXML
    private PasswordField r_passEntry;
    @FXML
    private PasswordField r_passConfirm;
    @FXML
    private Button r_registerButton;

    public void register(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        if (r_userEntry.getText().length() < 1
            || r_mailEntry.getText().length() < 1
            || r_mailConfirm.getText().length() < 1
            || r_passEntry.getText().length() < 1
            || r_passConfirm.getText().length() < 1) {
            alert.setHeaderText("Rellena todos los campos");
            alert.setTitle("Register");
            alert.showAndWait();
        } else if (!r_mailEntry.getText().equals(r_mailConfirm.getText())
                || !r_passEntry.getText().equals(r_passConfirm.getText())
        ) {
            alert.setHeaderText("Los campos de confirmar no corresponden.");
            alert.setContentText("Mail: " + r_mailEntry.getText() + " " + r_mailConfirm.getText() + "\nPass: " + r_passEntry.getText() + " " + r_passConfirm.getText());
            alert.setTitle("Register");
            alert.showAndWait();
        } else {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Registrado con exito.");
            alert.setTitle("Register");
            alert.showAndWait();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("log-view.fxml"));
            Scene scene;
            try {
                scene = new Scene(loader.load(), 266, 361);
                Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
