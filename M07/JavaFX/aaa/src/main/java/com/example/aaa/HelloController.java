package com.example.aaa;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    Button button1;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }

    @FXML
    private void clickChange(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("screen2.fxml"));
        Scene scene;
        try{
            scene = new Scene(loader.load(), 600, 600);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch  (IOException e){
            e.printStackTrace();
        }
    }
}