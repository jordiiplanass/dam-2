package com.company;

import java.util.stream.Stream;

public interface Database {

    void connect() throws CannotConnectException;

    Stream<Movie> searchMovies();

    void insertMovie(String title);

    void updateMovie(String title, String newTitle);

    void deleteMovie(String title);


}
