package account;

/*
 * Window -> Preferences -> Java -> CodeStyle -> CodeTemplates -> Code -> Comments -> Methods
 * Llavors escrius els parametres que vulguis afegir als metodes 
 */

import java.text.NumberFormat;

/*
 * @author jordi
 */

public class Account {
    private NumberFormat fmt = NumberFormat.getCurrencyInstance();
    private final float interesAnual = 0.1f; // interest rate of 10%
    private long acctNumber;
    private float balance;
    public final String name;

    /**
     * @param owner
     * @param account
     * @param initial
     */
    public Account(String owner, long account, float initial) {
        name = owner;
        acctNumber = account;
        balance = initial;
    }

    /**
     * @param amount
     * @return es superior a 0
     * {@link #treureDiners(float, float)}
     */
    public boolean ingresar(float amount) {
        boolean result = true;
        if (amount < 0) {
            result = false;
        } else {
            balance = balance + amount;
        }
        return result;
    }

    /**
     * @param amount
     * @param comisio
     * @return retirat correctament
     * {@link #ingresar(float)}
     * @see #ingresar(float)
     */
    public boolean treureDiners(float amount, float comisio) {
        boolean isValid = false;
        if (isValidWithdrawl(amount, comisio)) {
            amount += comisio;
            balance = balance - amount;
            isValid = true;
        }
        return isValid;
    }

    //2
    /**
     * @param compteDesti
     * @param quantitatTransferir
     * @return transferit correctament
     * @see #transfer(Account, float)
     */
    public boolean transfer (Account compteDesti, float quantitatTransferir){
        boolean isValid = false;
        if (this.balance > quantitatTransferir){
            compteDesti.balance += quantitatTransferir;
            this.balance -= quantitatTransferir;
            isValid = true;
        }
        return isValid;
    }

    /**
     * @param amount
     * @param comisio
     * @return hi ha prou diners i la suma del amount i la comissió es més petit que el balance
     */
    boolean isValidWithdrawl(float amount, float comisio) {
        return amount >= 0 && comisio >= 0 && amount+comisio <= balance;
    }

    public void afegirInteresAnual() {
        balance += (balance * interesAnual);
    }

    /**
     * @return balance
     */
    public float getBalance() {
        return balance;
    }

    /**
     * @return acctNumber
     */
    public long getAccountNumber() {
        return acctNumber;
    }

    /**
     * @return balance
     */
    public String toString() {
        return (acctNumber + "\t" + name + "\t" + fmt.format(balance));
    }
}
