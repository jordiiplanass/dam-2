# TASCA 3. Disseny d'una App. Mockup
Heu de dissenyar una app i fer el Mockup amb alta fidelitat. Heu de fer el Mockup de 5 pantalles (com vam fer amb el Wireframe)

Us podeu basar en un dels exemples vistos a classe d'Android, podeu fer el Mockup d'una App exsitent, o podeu fer una App totalment nova

En el document ha de constar:
- Objectiu o funció general de la app
- Target al que va enfocat
- Funcionalitats que ofereix la app. 
- Diagrama de casos d'ús.
- Guia d'estils (els colors i fonts que heu utilitzat)
- Mockup de 5 pantalles i wireflow o storyboard de navegació entre elles.
- Webgrafia. Eines i pàgines utilitzades i links
- El link al Mockup

Sobre el Mockup
- determinar una paleta de colors, treballar almenys 2 colors (a banda del color de fons per exemple per seccions),  amb almenys 2 intensitats (per exemple per objecte seleccionat o no...)
- escollir un tipus de lletra, i els colors pels textos, títols, botons.. Vigilar els contrastos.
- fer un logo per l'aplicació
- fer un jocs d'icones (3 o 4) i botons

---
## 1. Objectiu o funció general de la app
L'objectiu de l'aplicació es tindre un espai on reproduir música local (en fitxers) amb un diseny agradable i fàcil d'utilitzar.

## 2. Target al que va enfocat
El públic al que va enfocat és l'usuari que vol reproduir musica sense connexió a internet i sense cap empresa intermediaria, només els seus fitxers i el seu telèfon.

## 3. Funcionalitats que ofereix la app. 
L'aplicació et permet accedir a totes les cançons del teu dispositiu organitzat en diferentes finestres. Una finestra principal on et mostra de forma agrupada què escoltes normalment. Una altre finestra que et mostra la teva musica de forma més organitzada i una última d'usuari on emmagatzem un registre bàsic de l'ús del usuari.

## 4. Diagrama de casos d'ús.

![casUs](CasUs.png)

## 5. Guia d'estils (els colors i fonts que heu utilitzat)

![Colors](Colors.png)
### Tonalitats:
> Groc1: 978832
>
> Groc2: C5AF2C
>
> Groc3: F1D00A
>
> Blau1: 0F1B3D
> 
> Blau2: 172650
>
> Blau3: 21325E
>
> Blau4: 3E497A
>
> Blau5: 8088AE
>
> Blanc: F0F0F0

### Fonts:

![Fonts](Fonts.PNG)
> Inter
>
> Gilroy-Regular
>
> Roboto

## 6. Mockup de 5 pantalles i wireflow o storyboard de navegació entre elles.

### MockUp:

![MockUp](MockUp.png)

### WireFlow:

![WireFlow](WireFlow.png)

## 7. Webgrafia. Eines i pàgines utilitzades i links

He utilitzat l'eina Figma.

### Diseny i idees:

https://www.figma.com/file/t4vmNrG9jaID8eNDgMcIx7/Music-Player-UI-KIT%F0%9F%8E%A7-Community

https://www.figma.com/file/jMp1BIztVBjJVDnFsHAIIl/Neomorphism-Music-Player-Community

https://www.figma.com/file/KStuc13PZI7HiISJM23Tjk/Material-2-Design-Kit-Community

https://www.figma.com/file/OZGiSXG6lKtXQkCxRlGk5t/%E2%9D%96-Material-You-and-Android-12-Community

https://www.figma.com/file/sJhfkPeSZDD6H4XR5TvdCE/Inter-Poster-Community-Community

https://www.figma.com/file/OvvNbkNhxJ4EStIw6eyuZF/Android-Navigations-Community

https://play.google.com/store/apps/details?id=com.kodarkooperativet.blackplayerex&gl=ES

https://play.google.com/store/apps/details?id=com.rhmsoft.pulsar&gl=ES

https://play.google.com/store/apps/details?id=com.kabouzeid.gramophone&gl=ES

### Icons:

https://www.figma.com/file/Yms83UklaN9Ylsp2MjQdwz/Material-Design-Icons-Community

## 8. El link al Mockup

https://www.figma.com/file/hMhuiLyw3wFwqvASUpiwcL/MockUp-Tasca1

## 9. Icona de la app:

![Icon](Icono.png)
