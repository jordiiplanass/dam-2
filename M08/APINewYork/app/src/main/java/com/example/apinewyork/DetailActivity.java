package com.example.apinewyork;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    public ImageView imageSource;
    public TextView ddisplay_title;
    public TextView dbyline;
    public TextView dheadline;
    public TextView dpublication_date;
    public TextView dsummary_short;
    public Button durl;
    Result result;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        hook();

        Intent intent = getIntent();
        if (intent != null){
            result = getIntent().getExtras().getParcelable(Variables.INTENT_DETAIL);
            setData();
            durl.setOnClickListener(click -> {
                Intent web = new Intent(Intent.ACTION_VIEW);
                web.setData(Uri.parse(result.getLink().getUrl()));
                startActivity(web);
            });
        }

    }

    private void hook() {
        imageSource = findViewById(R.id.imageSource);
        ddisplay_title = findViewById(R.id.ddisplay_title);
        dbyline = findViewById(R.id.dbyline);
        dheadline = findViewById(R.id.dheadline);
        dpublication_date = findViewById(R.id.dpublication_date);
        dsummary_short = findViewById(R.id.dsummary_short);
        durl = findViewById(R.id.durl);
    }

    private void setData() {
        if (result.getMultimedia() != null){
            Picasso.get().load(result.getMultimedia().getSrc())
                    .fit()
                    .centerCrop()
                    .into(imageSource);
        }
        ddisplay_title.setText(result.getDisplay_title());
        ddisplay_title.setText(result.getDisplay_title());
        dbyline.setText(result.getByline());
        dheadline.setText(result.getHeadline());
        dpublication_date.setText(result.getPublication_date());
        dsummary_short.setText(result.getSummary_short());
    }
}