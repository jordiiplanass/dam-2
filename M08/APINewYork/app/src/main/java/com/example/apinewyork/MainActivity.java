package com.example.apinewyork;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String JSON_PART1 = "https://api.nytimes.com/svc/movies/v2/reviews/search.json?query=";
    private static final String JSON_PART2 = "&api-key=9oJqfxpg91SqMXlMGGsc4THVK13tZ2hb";
    TextInputEditText searchInputText;
    ImageView searchButton;
    List<Result> results;
    String input = "";
    String JSON = "";
    MyAdapter myAdapter;
    private RecyclerView newsRecycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hook();
        searchButton.setOnClickListener(v -> {
            if (searchInputText.getText().length() != 0){
                input = searchInputText.getText().toString();
                JSON = JSON_PART1 + input + JSON_PART2;
                getResults();
            } else {
                Toast.makeText(this, "No Input Found", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hook() {
        newsRecycler = findViewById(R.id.newsRecycler);
        searchInputText = findViewById(R.id.searchInputText);
        searchButton = findViewById(R.id.searchButton);
    }

    private void getResults() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
            Request.Method.GET,
            JSON,
            null,
                new Response.Listener<JSONObject>() { //Response listener
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.optString("status");
                            String num_results = response.optString("num_results");
                            JSONArray jsonArray = response.optJSONArray("results");
                            if (Integer.parseInt(num_results) < 1) {
                                Toast.makeText(MainActivity.this, "No Entries Found", Toast.LENGTH_SHORT).show();
                                newsRecycler.setVisibility(View.INVISIBLE);
                            } else {
                                results = Arrays.asList( new GsonBuilder().create().fromJson(jsonArray.toString(),
                                        Result[].class));
                                newsRecycler.setLayoutManager(new
                                        LinearLayoutManager(getApplicationContext()));
                                myAdapter = new MyAdapter(getApplicationContext(), results);
                                newsRecycler.setAdapter(myAdapter);
                                newsRecycler.setVisibility(View.VISIBLE);
                            }

                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
        }
        );
        queue.add(jsonObjectRequest);
    }
}