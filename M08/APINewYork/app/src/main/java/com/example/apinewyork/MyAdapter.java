package com.example.apinewyork;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context context;
    private List<Result> results;

    public MyAdapter(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.news_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.rdisplay_title.setText(results.get(position).getDisplay_title());
        holder.rsummary_short.setText(results.get(position).getSummary_short());
        holder.rpublication_date.setText(results.get(position).getPublication_date());
        if (results.get(position).getMultimedia() == null){
            holder.rsrc.setImageResource(R.drawable.default_void);
        } else {
            Picasso.get().load(results.get(position).getMultimedia().getSrc())
                    .fit()
                    .centerCrop()
                    .into(holder.rsrc);
        }
        holder.new_entry.setOnClickListener(card -> {
            Intent intent = new Intent(context, DetailActivity.class);
            Result result = results.get(position);
            intent.putExtra(Variables.INTENT_DETAIL, result);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView rdisplay_title;
        TextView rsummary_short;
        TextView rpublication_date;
        ImageView rsrc;
        CardView new_entry;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rdisplay_title = itemView.findViewById(R.id.rdisplay_title);
            rsummary_short = itemView.findViewById(R.id.rsummary_short);
            rpublication_date = itemView.findViewById(R.id.rpublication_date);
            rsrc = itemView.findViewById(R.id.rsrc);
            new_entry = itemView.findViewById(R.id.new_entry);
        }
    }
}
