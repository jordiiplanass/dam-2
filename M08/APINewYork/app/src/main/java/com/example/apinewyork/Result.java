package com.example.apinewyork;

import android.os.DropBoxManager;
import android.os.Parcel;
import android.os.Parcelable;

public class Result implements Parcelable {
    private String display_title;
    private String mpaa_rating;
    private int critics_pick;
    private String byline;
    private String headline;
    private String summary_short;
    private String publication_date;
    private String opening_date;
    private String date_updated;
    private Link link;
    private Multimedia multimedia;



    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public Result(Parcel in) {
        display_title = in.readString();
        mpaa_rating = in.readString();
        critics_pick = in.readInt();
        byline = in.readString();
        headline = in.readString();
        summary_short = in.readString();
        publication_date = in.readString();
        opening_date = in.readString();
        date_updated = in.readString();
        link = in.readParcelable(Link.class.getClassLoader());
        multimedia = in.readParcelable(Multimedia.class.getClassLoader());
    }

    public String getDisplay_title() {
        return display_title;
    }

    public void setDisplay_title(String display_title) {
        this.display_title = display_title;
    }

    public String getMpaa_rating() {
        return mpaa_rating;
    }

    public void setMpaa_rating(String mpaa_rating) {
        this.mpaa_rating = mpaa_rating;
    }

    public int getCritics_pick() {
        return critics_pick;
    }

    public void setCritics_pick(int critics_pick) {
        this.critics_pick = critics_pick;
    }

    public String getByline() {
        return byline;
    }

    public void setByline(String byline) {
        this.byline = byline;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getSummary_short() {
        return summary_short;
    }

    public void setSummary_short(String summary_short) {
        this.summary_short = summary_short;
    }

    public String getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(String publication_date) {
        this.publication_date = publication_date;
    }

    public String getOpening_date() {
        return opening_date;
    }

    public void setOpening_date(String opening_date) {
        this.opening_date = opening_date;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Multimedia getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(Multimedia multimedia) {
        this.multimedia = multimedia;
    }

    public Result() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        /*
        private String display_title;
    private String mpaa_rating;
    private int critics_pick;
    private String byline;
    private String headline;
    private String summary_short;
    private String publication_date;
    private String opening_date;
    private String date_updated;
    private Link link;
    private Multimedia multimedia;

         */
        parcel.writeString(display_title);
        parcel.writeString(mpaa_rating);
        parcel.writeInt(critics_pick);
        parcel.writeString(byline);
        parcel.writeString(headline);
        parcel.writeString(summary_short);
        parcel.writeString(publication_date);
        parcel.writeString(opening_date);
        parcel.writeString(date_updated);
        parcel.writeParcelable(link, i);
        parcel.writeParcelable(multimedia, i);

        /*
        parcel.writeString(display_title);
        parcel.writeString(mpaa_rating);
        parcel.writeInt(critics_pick);
        parcel.writeString(byline);
        parcel.writeString(headline);
        parcel.writeString(summary_short);
        parcel.writeString(publication_date);
        parcel.writeString(opening_date);
         */
    }
}
