package com.example.animationgif;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity2 extends AppCompatActivity {
    private ImageView imageView2;
    AnimationDrawable animation;
    String image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        imageView2 = findViewById(R.id.imageView2);
        animation = new AnimationDrawable();
        setFrames();
        animation.setOneShot(false);
        imageView2.setImageDrawable(animation);
        animation.start();
        imageView2.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity2.this, MainActivity.class);
            startActivity(intent);
        });
    }
    public void setFrames() {
        for (int i = 0; i <= 141; i++) {
            image = "descarga0";
            animation.addFrame(
                    ResourcesCompat.getDrawable(
                            getResources(),
                            getResources().getIdentifier(image + i , "drawable", getPackageName()),
                            null),
                    40);
        }
    }
}