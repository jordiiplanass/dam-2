package com.example.apunts_examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity3 extends AppCompatActivity {
    private Spinner spinner;
    private TextView textView3;
    private TextView textView4;

    private Button button3;

    ArrayList<myItem> items = new ArrayList<>();
    myItem pepe = new myItem("1", "2");
    myItem pepe2 = new myItem("12", "23");
    myItem pepe3 = new myItem("1225", "24");
    myItem pepe4 = new myItem("122", "247");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        //hook
        spinner = findViewById(R.id.spinner1);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        button3 = findViewById(R.id.button3);
        // list
        items.add(pepe);
        items.add(pepe2);
        items.add(pepe3);
        items.add(pepe4);

        MyAdapter myAdapter = new MyAdapter(this, items);
        spinner.setAdapter(myAdapter);



        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                myItem selectedCondition = (myItem) parent.getItemAtPosition(position);
                textView3.setText(selectedCondition.getText1());
                textView4.setText(selectedCondition.getText2());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity3.this, "aaaa", Toast.LENGTH_SHORT).show();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity3.this, MainActivity4.class);
                startActivity(intent);
            }
        });
    }
}