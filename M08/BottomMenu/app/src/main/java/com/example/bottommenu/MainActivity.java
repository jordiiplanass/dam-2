package com.example.bottommenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager2.adapter.FragmentViewHolder;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";


    private FrameLayout frameLayout;
    private BottomNavigationView bottomNavigationView;
    private fragment_home fragment_home;
    private fragment_profile fragment_profile;
    private fragment_settings fragment_settings;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        frameLayout = findViewById(R.id.frameLayout);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        fragment_home = fragment_home.newInstance(ARG_PARAM1, "Primavera");
        fragment_profile = fragment_profile.newInstance(ARG_PARAM2, "Estiu");
        fragment_settings = fragment_settings.newInstance(ARG_PARAM3, "Gatete");

        setFragment(fragment_home);


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        bottomNavigationView.setBackgroundResource(R.color.color1);
                        setFragment(fragment_home);
                        break;
                    case R.id.profile:
                        bottomNavigationView.setBackgroundResource(R.color.color2);
                        setFragment(fragment_profile);
                        break;
                    case R.id.settings:
                        bottomNavigationView.setBackgroundResource(R.color.color3);
                        setFragment(fragment_settings);
                        break;
                }
            return true;
            }
        });
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }
}