using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{
    [SerializeField] Transform player;
    [SerializeField] Vector3 cameraVelocity;
    [SerializeField] float smothTime = 1;
    [SerializeField] bool lookAtPlayer;
    [SerializeField] float maxLimit = 0;
    [SerializeField] float lowerLimit = 0;
    [SerializeField] float offset = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player.position.y > lowerLimit && player.position.y < maxLimit){
            Vector3 targetPosition = new Vector3 (transform.position.x, player.position.y + offset, transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref cameraVelocity, smothTime);
            if (lookAtPlayer){
                transform.LookAt(player);
            }
        } 
    }
}
