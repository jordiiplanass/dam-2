# Seven Half Game

## A classic card game for android devices

### The rules

Seven and a Half is a Spanish card game, played with the standard deck of forty cards. This is a classic of card games, very easy to play and at the same time very fun.

The instructions of the game Seven and a half are as follows:

All cards less or equals to 7 value their number, the rest of the cards (8, 9, 10, 11 and 12) value 0.5

All cards are played discovered.

There are two options in this game, you can play against the BANK (1 PLAYER) or you can play with a friend in the same mobile phone (2 PLAYERS).

### 1 PLAYER:

- The player competes against the BANK, the goal of this game is to sum seven and a half or a close number without exceeding.
- The player can ask for more cards, one by one, in order to get 7 and a half, or just stand at any moment.
- If the player gets more than seven and a half, then the BANK automatically wins.
- If the player is planted with less or equals than seven and a half, then the BANK plays.
- The BANK will always try to get more than the result of the player.
- If the BANK gets more or equal than the player and less or equal than seven an a half, then the BANK wins.
- If the BANK gets more than seven and a half and the player got less or equal than seven and a half then the player wins.
### 2 PLAYERS:

- In this option tow people can play in the same phone.
- First the bank is assigned randomly.
- The first player to play is the one who is not the BANK
- The first player can ask for more cards, one by one, in order to get 7 and a half, or just stand at any moment.
- If the first player gets more than seven and a half, the second player (the BANK) automatically wins.
- If the first player is planted with less or equals than seven and a half, then the turn passes to the second player (the BANK).
- The second player (the BANK) can ask for more cards, one by one, in order to get more than the first player and less or equals than 7 and a half, or just stand at any moment.
- If the second player (the BANK) gets more or equal than the first player and less or equal than seven an a half, then the second player (the BANK) wins.
- If the second player (the BANK) gets more than seven and a half and the first player got less or equal than seven and a half then the first player wins.

###### This application has been made for educational purposes.
###### Concept and design made by [Joan Séculi](https://www.joanseculi.com/)
