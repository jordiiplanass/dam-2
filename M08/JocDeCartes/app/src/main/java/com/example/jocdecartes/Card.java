package com.example.jocdecartes;

import android.widget.ImageView;

public class Card {
    int image;
    double value;

    public Card(int image, double value) {
        this.image = image;
        this.value = value;
    }
}
