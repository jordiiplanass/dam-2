package com.example.jocdecartes;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    public static final String FILE_SHARED_NAME = "MySharedFile";

    ImageView logo;
    Button buttonOnePlayer;
    Button buttonTwoPlayers;

    ObjectAnimator animation1;
    ObjectAnimator animation2;
    ObjectAnimator animation3;
    ObjectAnimator animation4;
    ObjectAnimator animation5;
    ObjectAnimator animation6;

    boolean resume = false;
    boolean resume2 = false;
    boolean click1 = false;
    boolean click2 = false;
    int points1;
    int points2;
    int points3;
    int points4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // hook
        logo = findViewById(R.id.logo);
        buttonOnePlayer = findViewById(R.id.buttonOnePlayer);
        buttonTwoPlayers = findViewById(R.id.buttonTwoPlayers);

        // sound
        final MediaPlayer clink = MediaPlayer.create(this,R.raw.bell_small_001);
        clink.start();

        // animations
        logoAnimation(logo);
        buttonTransition(buttonOnePlayer, buttonTwoPlayers, 0);

        // intent
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        points1 = sharedPreferences.getInt("singlegame_bank", 0);
        points2 = sharedPreferences.getInt("singlegame_player", 0);
        points3 = sharedPreferences.getInt("twogame_bank", 0);
        points4 = sharedPreferences.getInt("twogame_player", 0);

        if (points1 + points2 == 0){
            resume = true;
        }
        if (points3 + points4 == 0){
            resume2 = true;
        }

        // resume si o no


        buttonOnePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resume){
                    // de zero
                    Intent intent = new Intent(MainActivity.this, OnePlayer.class);
                    startActivity(intent);
                    reinicio();
                } else if (click1) {
                    // continuar la partida singleplayer
                    Intent intent = new Intent(MainActivity.this, OnePlayer.class);
                    startActivity(intent);
                    reinicio();
                } else if (click2) {
                    // continuar la partida 2 Players
                    Intent intent = new Intent(MainActivity.this, TwoPlayers.class);
                    startActivity(intent);
                    reinicio();
                } else {
                    // animacion de resume o continuar la partida
                    buttonTransition(buttonOnePlayer, buttonTwoPlayers, -3000);
                    buttonOnePlayer.setText(R.string.resumeGame);
                    buttonTwoPlayers.setText(R.string.newGame);
                    click1 = true;
                }
            }
        });
        buttonTwoPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (click1){
                    // singleplayer de zero
                    Intent intent = new Intent(MainActivity.this, TwoPlayers.class);
                    editor.putInt("singlegame_bank", 0);
                    editor.putInt("singlegame_player", 0);
                    editor.commit();
                    startActivity(intent);
                    reinicio();
                } else if (resume2 || click2) {
                    // DE ZERO
                    Intent intent = new Intent(MainActivity.this, TwoPlayers.class);
                    editor.putInt("twogame_bank", 0);
                    editor.putInt("twogame_player", 0);
                    editor.commit();
                    startActivity(intent);
                    reinicio();
                } else {
                    buttonTransition(buttonOnePlayer, buttonTwoPlayers, -3000);
                    buttonOnePlayer.setText(R.string.resumeGame);
                    buttonTwoPlayers.setText(R.string.newGame);
                    click2 = true;
                }
            }
        });
    }
    public void logoAnimation(ImageView imageView){
        animation1 = ObjectAnimator.ofFloat(imageView, "rotation", 360f).setDuration(2000);

        animation2 = ObjectAnimator.ofFloat(imageView, "scaleX", 7f);
        animation2.setStartDelay(2000);
        animation2.setDuration(1500);

        animation3 = ObjectAnimator.ofFloat(imageView, "scaleY", 7f);
        animation3.setStartDelay(2000);
        animation3.setDuration(1500);

        animation4 = ObjectAnimator.ofFloat(imageView, "translationX", logo.getX(), logo.getX()+300f);
        animation4.setStartDelay(2000);
        animation4.setDuration(1500);
        // start animation
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(animation1, animation2, animation3, animation4);
        animationSet.start();
    }
    public void buttonTransition(Button button1, Button button2, int retras){
        animation1 = ObjectAnimator.ofFloat(button1, "x", -500f);
        animation2 = ObjectAnimator.ofFloat(button2, "x", 2000f);

        animation3 = ObjectAnimator.ofFloat(button1, "translationX", 0);
        animation3.setStartDelay(3500+retras);
        animation3.setDuration(1000);

        animation4 = ObjectAnimator.ofFloat(button2, "translationX", 0);
        animation4.setStartDelay(3500+retras);
        animation4.setDuration(1000);

        animation5 = ObjectAnimator.ofFloat(button1, "alpha",0f, 1f);
        animation5.setStartDelay(3500+retras);

        animation6 = ObjectAnimator.ofFloat(button2, "alpha",0f, 1f);
        animation6.setStartDelay(3500+retras);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(animation1, animation2, animation3,
                animation4, animation5, animation6);
        animationSet.start();
    }
    public void reinicio(){
        buttonTransition(buttonOnePlayer, buttonTwoPlayers, 0);
        buttonOnePlayer.setText(R.string.onePlayer);
        buttonTwoPlayers.setText(R.string.twoPlayers);
    }
}