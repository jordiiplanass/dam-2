package com.example.jocdecartes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class TwoPlayers extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String FILE_SHARED_NAME = "MySharedFile";

    private Toolbar myToolbar;
    private DrawerLayout drawerLayout;
    private NavigationView nav_menu;
    private TextView bankpoints;
    private TextView playerpoints;
    private TextView gamepoints1;
    private TextView gamepoints2;
    private TextView popUpText;

    private ImageView cardbank1;
    private ImageView cardbank2;
    private ImageView cardbank3;
    private ImageView cardbank4;
    private ImageView cardbank5;
    private ImageView cardbank6;
    private ImageView cardbank7;
    private ImageView cardbank8;
    private ImageView cardbank9;
    private ImageView cardbank10;
    private ImageView cardbank11;
    private ImageView cardbank12;
    private ImageView cardbank13;
    private ImageView cardbank14;
    private ImageView cardbank15;
    private ImageView cardplayer1;
    private ImageView cardplayer2;
    private ImageView cardplayer3;
    private ImageView cardplayer4;
    private ImageView cardplayer5;
    private ImageView cardplayer6;
    private ImageView cardplayer7;
    private ImageView cardplayer8;
    private ImageView cardplayer9;
    private ImageView cardplayer10;
    private ImageView cardplayer11;
    private ImageView cardplayer12;
    private ImageView cardplayer13;
    private ImageView cardplayer14;
    private ImageView cardplayer15;
    private ImageView cardPile;

    private Button playButton;
    private Button replayButton;
    private Button stopButton;

    private ObjectAnimator objectAnimator1;
    private ObjectAnimator objectAnimator2;
    private ObjectAnimator objectAnimator3;
    private ObjectAnimator objectAnimator4;
    private ObjectAnimator objectAnimator5;

    private ArrayList<ImageView> cartasbank = new ArrayList<>();
    private ArrayList<ImageView> cartasplayer = new ArrayList<>();
    private ArrayList<MediaPlayer> soundEffectsWin = new ArrayList<>();
    private ArrayList<MediaPlayer> soundEffectsLoose = new ArrayList<>();

    private ArrayList<Card> deck = new ArrayList<>();

    boolean soundState = true;
    boolean darktheme = true;
    int playerDeckIndex = 0;
    int bankDeckIndex = 0;

    int bank_points = 0;
    int player_points = 0;
    double points1 = 0;
    double points2 = 0;
    boolean bucle = true;
    boolean turnodos = false;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.volume:
                if (soundState){
                    // volume on
                    item.setIcon(R.drawable.volume_off);
                    soundState = false;
                } else {
                    // volume off
                    item.setIcon(R.drawable.volume_on);
                    soundState = true;
                }
                break;
            case R.id.theme:
                if (darktheme){
                    drawerLayout.setBackgroundResource(R.drawable.background2);
                    item.setIcon(R.drawable.ic_dark);
                    darktheme = false;
                } else {
                    drawerLayout.setBackgroundResource(R.drawable.background1);
                    item.setIcon(R.drawable.ic_light);
                    darktheme = true;
                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_players);

        //hook
        hook();

        // load sound effects
        initSound();

        //Toolbar
        setSupportActionBar(myToolbar);

        //Navigation Menu
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(TwoPlayers.this,
                drawerLayout,
                myToolbar,
                R.string.menuopen,
                R.string.menuclose);
        nav_menu.setNavigationItemSelectedListener(this);

        // New Game
        initPoints();
        animacionExpanse(playButton);

        // Play
        initCards();
        alphaZero();
        replayButton.setClickable(false);
        stopButton.setClickable(false);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initCards();
                alphaZero();
                points1 = 0;
                gamepoints1.setText(String.valueOf(points1));
                gamepoints2.setText(String.valueOf(0));
                replayButton.setClickable(false);
                stopButton.setClickable(false);
                playButton.setAlpha(0);
                playButton.setActivated(false);
                playButton.setEnabled(false);
                playButton.setClickable(false);

                playerDeckIndex = 0;
                Card card1 = getRandomCard(deck);
                animacionCardToPosition(cartasplayer.get(playerDeckIndex), card1);
                playerDeckIndex++;
                points1 += card1.value;

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gamepoints1.setText(String.valueOf(points1));
                        nextRoundButtons();
                    }
                }, 2000);


            }
        });
        replayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnodos){
                    replayButton.setAlpha(0);
                    replayButton.setClickable(false);
                    stopButton.setAlpha(0);
                    stopButton.setClickable(false);
                    Card randomCard = getRandomCard(deck);
                    animacionCardToPosition(cartasbank.get(playerDeckIndex++), randomCard);
                    points2 += randomCard.value;
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            gamepoints2.setText(String.valueOf(points2));
                            if (points2 > 7.5) {
                                win();
                                bucle = false;
                            } else if (points2 == points1) {
                                lost();
                                bucle = false;
                            } else if (points2 > points1 && points2 <= 7.5){
                                lost();
                                bucle = false;
                            } else {
                                nextRoundButtons();
                            }
                        }
                    }, 3000);
                } else {
                    replayButton.setAlpha(0);
                    replayButton.setClickable(false);
                    stopButton.setAlpha(0);
                    stopButton.setClickable(false);
                    Card randomCard = getRandomCard(deck);
                    animacionCardToPosition(cartasplayer.get(playerDeckIndex++), randomCard);
                    points1 += randomCard.value;
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            gamepoints1.setText(String.valueOf(points1));
                            if (points1 > 7.5){
                                lost();
                            }else {
                                nextRoundButtons();
                            }
                        }
                    }, 3000);
                }

            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!turnodos){
                    replayButton.setAlpha(0);
                    replayButton.setClickable(false);
                    stopButton.setAlpha(0);
                    stopButton.setClickable(false);
                    points2 = 0;
                    gamepoints1.setText(String.valueOf(points1));
                    gamepoints2.setText(String.valueOf(0));
                    replayButton.setClickable(false);
                    stopButton.setClickable(false);
                    playButton.setAlpha(0);
                    playButton.setActivated(false);
                    playButton.setEnabled(false);
                    playButton.setClickable(false);
                    turnodos = true;
                    playerDeckIndex = 0;
                    Card card1 = getRandomCard(deck);
                    animacionCardToPosition(cartasbank.get(playerDeckIndex++), card1);
                    points2 += card1.value;

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            gamepoints2.setText(String.valueOf(points2));
                            nextRoundButtons();
                        }
                    }, 2000);

                } else {
                    check();
                }
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "7 1/2 IS THE BEST APP EVER");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.rate:
                Toast.makeText(TwoPlayers.this, "Not available", Toast.LENGTH_SHORT).show();
                break;
            case R.id.contact:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + "jordiplanas86@gmail.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Seven Half Opinion");
                startActivity(intent);
                break;
            case R.id.about:
                String url = "https://github.com/jplanasmartinez/jplanasmartinez";
                Uri uri = Uri.parse(url);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
                break;
            case R.id.website:
                String url2 = "https://gitlab.com/jordiiplanass/dam-2/-/tree/main/M08/JocDeCartes";
                Uri uri2 = Uri.parse(url2);
                startActivity(new Intent(Intent.ACTION_VIEW, uri2));
                break;
            case R.id.donate:
                String url3 = "https://paypal.me/jordiplanas54";
                Uri uri3 = Uri.parse(url3);
                startActivity(new Intent(Intent.ACTION_VIEW, uri3));
        }
        return true;
    }

    public void hook(){
        cardbank1 = findViewById(R.id.cardbank1);
        cardbank2 = findViewById(R.id.cardbank2);
        cardbank3 = findViewById(R.id.cardbank3);
        cardbank4 = findViewById(R.id.cardbank4);
        cardbank5 = findViewById(R.id.cardbank5);
        cardbank6 = findViewById(R.id.cardbank6);
        cardbank7 = findViewById(R.id.cardbank7);
        cardbank8 = findViewById(R.id.cardbank8);
        cardbank9 = findViewById(R.id.cardbank9);
        cardbank10 = findViewById(R.id.cardbank10);
        cardbank11 = findViewById(R.id.cardbank11);
        cardbank12 = findViewById(R.id.cardbank12);
        cardbank13 = findViewById(R.id.cardbank13);
        cardbank14 = findViewById(R.id.cardbank14);
        cardbank15 = findViewById(R.id.cardbank15);
        cardplayer1 = findViewById(R.id.cardplayer1);
        cardplayer2 = findViewById(R.id.cardplayer2);
        cardplayer3 = findViewById(R.id.cardplayer3);
        cardplayer4 = findViewById(R.id.cardplayer4);
        cardplayer5 = findViewById(R.id.cardplayer5);
        cardplayer6 = findViewById(R.id.cardplayer6);
        cardplayer7 = findViewById(R.id.cardplayer7);
        cardplayer8 = findViewById(R.id.cardplayer8);
        cardplayer9 = findViewById(R.id.cardplayer9);
        cardplayer10 = findViewById(R.id.cardplayer10);
        cardplayer11 = findViewById(R.id.cardplayer11);
        cardplayer12 = findViewById(R.id.cardplayer12);
        cardplayer13 = findViewById(R.id.cardplayer13);
        cardplayer14 = findViewById(R.id.cardplayer14);
        cardplayer15 = findViewById(R.id.cardplayer15);
        cardPile = findViewById(R.id.cardPile);

        playButton = findViewById(R.id.playButton);
        replayButton = findViewById(R.id.replayButton);
        stopButton = findViewById(R.id.stopButton);
        myToolbar = findViewById(R.id.myToolbar);
        drawerLayout = findViewById(R.id.drawerLayout);
        nav_menu = findViewById(R.id.nav_menu);
        bankpoints = findViewById(R.id.bankpoints);
        playerpoints = findViewById(R.id.playerpoints);
        gamepoints1 = findViewById(R.id.gamepoints1);
        gamepoints2 = findViewById(R.id.gamepoints2);
        popUpText = findViewById(R.id.popUpText);
    }
    public void initSound(){
        soundEffectsWin.add(MediaPlayer.create(this,R.raw.win1));
        soundEffectsWin.add(MediaPlayer.create(this,R.raw.win2));
        soundEffectsWin.add(MediaPlayer.create(this,R.raw.win3));
        soundEffectsLoose.add(MediaPlayer.create(this,R.raw.loose1));
        soundEffectsLoose.add(MediaPlayer.create(this,R.raw.loose2));
        soundEffectsLoose.add(MediaPlayer.create(this,R.raw.loose3));
    }
    public void initPoints(){
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        bank_points = sharedPreferences.getInt("twogame_bank", 0);
        player_points = sharedPreferences.getInt("twogame_player", 0);

        editor.commit();
        bankpoints.setText(String.valueOf(bank_points));
        playerpoints.setText(String.valueOf(player_points));
        gamepoints1.setText(String.valueOf(0));
        gamepoints2.setText(String.valueOf(0));
    }
    public void initCards(){
        // ImageView de les posicions de les cartes
        cartasbank.add(cardbank1);
        cartasbank.add(cardbank2);
        cartasbank.add(cardbank3);
        cartasbank.add(cardbank4);
        cartasbank.add(cardbank5);
        cartasbank.add(cardbank6);
        cartasbank.add(cardbank7);
        cartasbank.add(cardbank8);
        cartasbank.add(cardbank9);
        cartasbank.add(cardbank10);
        cartasbank.add(cardbank11);
        cartasbank.add(cardbank12);
        cartasbank.add(cardbank13);
        cartasbank.add(cardbank14);
        cartasbank.add(cardbank15);

        cartasplayer.add(cardplayer1);
        cartasplayer.add(cardplayer2);
        cartasplayer.add(cardplayer3);
        cartasplayer.add(cardplayer4);
        cartasplayer.add(cardplayer5);
        cartasplayer.add(cardplayer6);
        cartasplayer.add(cardplayer7);
        cartasplayer.add(cardplayer8);
        cartasplayer.add(cardplayer9);
        cartasplayer.add(cardplayer10);
        cartasplayer.add(cardplayer11);
        cartasplayer.add(cardplayer12);
        cartasplayer.add(cardplayer13);
        cartasplayer.add(cardplayer14);
        cartasplayer.add(cardplayer15);


        // cartes

        deck.add(new Card(R.drawable.clubs01, 1));
        deck.add(new Card(R.drawable.clubs02, 2));
        deck.add(new Card(R.drawable.clubs03, 3));
        deck.add(new Card(R.drawable.clubs04, 4));
        deck.add(new Card(R.drawable.clubs05, 5));
        deck.add(new Card(R.drawable.clubs06, 6));
        deck.add(new Card(R.drawable.clubs07, 7));
        deck.add(new Card(R.drawable.clubs08, 0.5));
        deck.add(new Card(R.drawable.clubs09, 0.5));
        deck.add(new Card(R.drawable.clubs10, 0.5));
        deck.add(new Card(R.drawable.clubs11, 0.5));
        deck.add(new Card(R.drawable.clubs12, 0.5));

        deck.add(new Card(R.drawable.cups01, 1));
        deck.add(new Card(R.drawable.cups02, 2));
        deck.add(new Card(R.drawable.cups03, 3));
        deck.add(new Card(R.drawable.cups04, 4));
        deck.add(new Card(R.drawable.cups05, 5));
        deck.add(new Card(R.drawable.cups06, 6));
        deck.add(new Card(R.drawable.cups07, 7));
        deck.add(new Card(R.drawable.cups08, 0.5));
        deck.add(new Card(R.drawable.cups09, 0.5));
        deck.add(new Card(R.drawable.cups10, 0.5));
        deck.add(new Card(R.drawable.cups11, 0.5));
        deck.add(new Card(R.drawable.cups12, 0.5));

        deck.add(new Card(R.drawable.golds01, 1));
        deck.add(new Card(R.drawable.golds02, 2));
        deck.add(new Card(R.drawable.golds03, 3));
        deck.add(new Card(R.drawable.golds04, 4));
        deck.add(new Card(R.drawable.golds05, 5));
        deck.add(new Card(R.drawable.golds06, 6));
        deck.add(new Card(R.drawable.golds07, 7));
        deck.add(new Card(R.drawable.golds08, 0.5));
        deck.add(new Card(R.drawable.golds09, 0.5));
        deck.add(new Card(R.drawable.golds10, 0.5));
        deck.add(new Card(R.drawable.golds11, 0.5));
        deck.add(new Card(R.drawable.golds12, 0.5));

        deck.add(new Card(R.drawable.swords01, 1));
        deck.add(new Card(R.drawable.swords02, 2));
        deck.add(new Card(R.drawable.swords03, 3));
        deck.add(new Card(R.drawable.swords04, 4));
        deck.add(new Card(R.drawable.swords05, 5));
        deck.add(new Card(R.drawable.swords06, 6));
        deck.add(new Card(R.drawable.swords07, 7));
        deck.add(new Card(R.drawable.swords08, 0.5));
        deck.add(new Card(R.drawable.swords09, 0.5));
        deck.add(new Card(R.drawable.swords10, 0.5));
        deck.add(new Card(R.drawable.swords11, 0.5));
        deck.add(new Card(R.drawable.swords12, 0.5));
    }
    // Estetico
    public void alphaZero(){
        for (ImageView card : cartasplayer){
            card.setImageResource(R.drawable.back);
            objectAnimator1 = ObjectAnimator.ofFloat(card, "alpha", 0f);
            objectAnimator1.setDuration(0);
            objectAnimator1.start();
        }
        for (ImageView card : cartasbank){
            card.setImageResource(R.drawable.back);
            objectAnimator1 = ObjectAnimator.ofFloat(card, "alpha", 0f);
            objectAnimator1.setDuration(0);
            objectAnimator1.start();
        }
        popUpText.setAlpha(0);
        replayButton.setAlpha(0);
        stopButton.setAlpha(0);
    }
    public void animacionExpanse(View button){
        objectAnimator1 = ObjectAnimator.ofFloat(button, "scaleX", 0f, 0f);
        objectAnimator2 = ObjectAnimator.ofFloat(button, "scaleY", 0f, 0f);
        objectAnimator3 = ObjectAnimator.ofFloat(button, "scaleX", 0f, 1f);
        objectAnimator4 = ObjectAnimator.ofFloat(button, "scaleY", 0f, 1f);
        objectAnimator3.setStartDelay(500);
        objectAnimator4.setStartDelay(500);
        objectAnimator3.setDuration(1000);
        objectAnimator4.setDuration(1000);
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(objectAnimator1, objectAnimator2,
                objectAnimator3, objectAnimator4);
        animationSet.start();
    }
    public void animacionExpanse2(View button){
        objectAnimator1 = ObjectAnimator.ofFloat(button, "scaleX", 1f, 2f);
        objectAnimator2 = ObjectAnimator.ofFloat(button, "scaleY", 1f, 2f);
        objectAnimator3 = ObjectAnimator.ofFloat(button, "scaleX", 2f, 1f);
        objectAnimator4 = ObjectAnimator.ofFloat(button, "scaleY", 2f, 1f);
        objectAnimator1.setDuration(300);
        objectAnimator2.setDuration(300);
        objectAnimator3.setStartDelay(300);
        objectAnimator4.setStartDelay(300);
        objectAnimator3.setDuration(300);
        objectAnimator4.setDuration(300);
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(objectAnimator1, objectAnimator2,
                objectAnimator3, objectAnimator4);
        animationSet.start();
    }
    public void animacionCardToPosition(ImageView card, Card carta){
        objectAnimator1 = ObjectAnimator.ofFloat(card,"X", cardPile.getX(), card.getX());
        objectAnimator2 = ObjectAnimator.ofFloat(card,"Y", cardPile.getY(), card.getY());
        objectAnimator3 = ObjectAnimator.ofFloat(card,"alpha", 0f,1f);
        objectAnimator4 = ObjectAnimator.ofFloat(card,"rotationY", 180f,360f);
        objectAnimator1.setDuration(1000);
        objectAnimator2.setDuration(1000);
        objectAnimator3.setDuration(0);
        objectAnimator4.setDuration(1000);
        objectAnimator1.setStartDelay(500);
        objectAnimator2.setStartDelay(500);
        objectAnimator3.setStartDelay(500);
        objectAnimator4.setStartDelay(1500);
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
        animationSet.start();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                card.setImageResource(carta.image);
            }
        }, 2000);
    }
    public void nextRoundButtons(){
        objectAnimator1 = ObjectAnimator.ofFloat(replayButton,"X",replayButton.getX() - 500, replayButton.getX());
        objectAnimator2 = ObjectAnimator.ofFloat(stopButton,"X",stopButton.getX() + 500, stopButton.getX());
        objectAnimator3 = ObjectAnimator.ofFloat(replayButton,"alpha",0f, 1f);
        objectAnimator4 = ObjectAnimator.ofFloat(stopButton,"alpha",0f, 1f);
        objectAnimator1.setDuration(1000);
        objectAnimator2.setDuration(1000);
        objectAnimator3.setDuration(0);
        objectAnimator4.setDuration(0);
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
        animationSet.start();
        replayButton.setClickable(true);
        replayButton.setEnabled(true);
        stopButton.setClickable(true);
        stopButton.setEnabled(true);
    }
    public void playRandomSound(ArrayList<MediaPlayer> list){
        int index = (int) (Math.random() * list.size());
        list.get(index).start();
    }
    // Logica
    public boolean check(){
        bucle = true;

        if (points2 > 7.5) {
            win();
            bucle = false;
        } else if (points2 == points1) {
            lost();
            bucle = false;
        } else if (points2 > points1 && points2 <= 7.5){
            lost();
            bucle = false;
        }


        return bucle;
    }
    public void win(){
        // You have won text
        popUpText.setText(R.string.winplayer1);
        objectAnimator1 = ObjectAnimator.ofFloat(popUpText, "alpha",0f, 1f);
        objectAnimator1.setDuration(0);
        objectAnimator1.start();
        animacionExpanse(popUpText);
        // Sound effect
        if (soundState) playRandomSound(soundEffectsWin);
        // Increase bank points
        player_points++;
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("twogame_player", player_points);
        editor.commit();
        animacionExpanse2(playerpoints);
        playerpoints.setText(String.valueOf(player_points));
        // end
        end();
    }
    public void lost(){
        // You have lost text
        popUpText.setText(R.string.winplayer2);
        objectAnimator1 = ObjectAnimator.ofFloat(popUpText, "alpha",0f, 1f);
        objectAnimator1.setDuration(0);
        objectAnimator1.start();
        animacionExpanse(popUpText);
        // Sound effect
        if (soundState) playRandomSound(soundEffectsLoose);
        // Increase bank points
        bank_points++;
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("twogame_bank", bank_points);
        editor.commit();
        animacionExpanse2(bankpoints);
        bankpoints.setText(String.valueOf(bank_points));
        //end
        end();
    }
    public void end(){
        // play again button
        playButton.setAlpha(1);
        playButton.setActivated(true);
        playButton.setEnabled(true);
        playButton.setClickable(true);
        playButton.setText(R.string.play2);
        animacionExpanse(playButton);
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("resume1", true);
        editor.commit();
        turnodos = false;
    }
    public Card getRandomCard(ArrayList<Card> cards){
        int index = (int) (Math.random() * cards.size());
        return cards.get(index);
    }
}