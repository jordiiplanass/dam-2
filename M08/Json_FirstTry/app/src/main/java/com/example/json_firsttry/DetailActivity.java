package com.example.json_firsttry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView textName;
    private TextView textHeight;
    private TextView textProminence;
    private TextView textZone;
    private ImageView imageView;
    private TextView textCountry;

    String name, height, prominence, zone, url, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        hook();
        getData();
        setData();
    }

    private void hook() {
        textName = findViewById(R.id.textName);
        textHeight = findViewById(R.id.textHeight);
        textProminence = findViewById(R.id.textProminence);
        textZone = findViewById(R.id.textZone);
        imageView = findViewById(R.id.imageView);
        textCountry = findViewById(R.id.textCountry);
    }
    private void getData() {
        if (getIntent().hasExtra("name") &&
                getIntent().hasExtra("height")) {
            name = getIntent().getStringExtra("name");
            height = getIntent().getStringExtra("height");
            prominence = getIntent().getStringExtra("prominence");
            zone = getIntent().getStringExtra("zone");
            url = getIntent().getStringExtra("url");
            country = getIntent().getStringExtra("country");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }
    private void setData() {
        textName.setText(name);
        textHeight.setText(height);
        textProminence.setText(prominence);
        textZone.setText(zone);
        textCountry.setText(country);
        Picasso.get().load(url)
                .fit()
                .centerCrop()
                .into(imageView);
    }
}