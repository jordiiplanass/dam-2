package com.example.json_firsttry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static String JSON_URL = "https://run.mocky.io/v3/9bdf7700-f227-49ea-ae61-b22ad985294b";
    List peaks = new ArrayList<>();
    private RecyclerView recyclerView;
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hook();
        getPeaks();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyAdapter myAdapter2 = new MyAdapter(this, peaks);
        recyclerView.setAdapter(myAdapter2);

    }

    private void getPeaks(){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject peakObject = response.getJSONObject(i);
                                Peak peak = new Peak();
                                peak.setName(peakObject.getString("name"));
                                peak.setHeight(peakObject.getString("height"));
                                peak.setProminence(peakObject.getString("prominence"));
                                peak.setZone(peakObject.getString("zone"));
                                peak.setUrl(peakObject.getString("url"));
                                peak.setCountry(peakObject.getString("country"));
                                peaks.add(peak);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        recyclerView.setLayoutManager(new
                                LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), peaks);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }

    private void hook() {
        recyclerView = findViewById(R.id.recyclerView);
    }
}