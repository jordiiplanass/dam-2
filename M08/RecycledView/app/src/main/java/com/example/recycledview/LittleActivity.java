package com.example.recycledview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LittleActivity extends AppCompatActivity {

    private TextView textTitle;
    private TextView textSubtitle;
    private ImageView image;
    String title;
    String subtitle;
    int myImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_little);
        textTitle = findViewById(R.id.textTitle);
        textSubtitle = findViewById(R.id.textSubtitle);
        image = findViewById(R.id.image);
        getData();

        setData();
    }
    public void getData() {
        if (getIntent().hasExtra("image") &&
                getIntent().hasExtra("title") &&
                getIntent().hasExtra("desc")) {
            title = getIntent().getStringExtra("title");
            subtitle = getIntent().getStringExtra("subtitle");
            myImage = getIntent().getIntExtra("image", 1);
        }else {
            Toast.makeText(this, "No data to display.", Toast.LENGTH_SHORT).show();
        }
    }
    public void setData(){
        textTitle.setText(title);
        textSubtitle.setText(subtitle);
        image.setImageResource(myImage);
    }

}