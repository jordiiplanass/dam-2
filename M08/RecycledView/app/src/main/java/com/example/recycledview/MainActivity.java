package com.example.recycledview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    String title[];
    String subtitle[];

    int image[] =  {
            R.drawable.arcoiris, R.drawable.chupete, R.drawable.corona,
            R.drawable.unicornio, R.drawable.varitamagica
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        title = getResources().getStringArray(R.array.title);
        subtitle = getResources().getStringArray(R.array.subtitle);

        MyAdapter myAdapter = new MyAdapter(title, subtitle, image, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}