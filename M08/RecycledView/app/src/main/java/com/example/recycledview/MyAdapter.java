package com.example.recycledview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private String[] title;
    private String[] subtitle;
    private int[] image;
    private Context context;

    public MyAdapter(String[] title, String[] subtitle, int[] image, Context context) {
        this.title = title;
        this.subtitle = subtitle;
        this.image = image;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.myrow, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.myTextTitleRow.setText(title[position]);
        holder.myTextSubtitleRow.setText(subtitle[position]);
        holder.myImageRow.setImageResource(image[position]);
        holder.layoutRow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LittleActivity.class);
                intent.putExtra("title", title[holder.getAdapterPosition()]);
                intent.putExtra("desc", subtitle[holder.getAdapterPosition()]);
                intent.putExtra("image", image[holder.getAdapterPosition()]);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return title.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView myTextTitleRow;
        TextView myTextSubtitleRow;
        ImageView myImageRow;
        ConstraintLayout layoutRow;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myTextTitleRow = itemView.findViewById(R.id.titol);
            myTextSubtitleRow = itemView.findViewById(R.id.subtitol);
            myImageRow = itemView.findViewById(R.id.imageView);
            layoutRow = itemView.findViewById(R.id.layoutRow);
        }
    }
}
