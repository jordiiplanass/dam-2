package com.example.sharepreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    public static final String FILE_SHARED_NAME = "MySharedFile";

    private TextInputEditText text1;
    private TextInputEditText text2;
    private TextInputEditText text3;

    private Button save;
    private Button read;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        save = findViewById(R.id.save);
        read = findViewById(R.id.read);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString("Name", text1.getText().toString());
                editor.putString("Mail", text2.getText().toString());
                editor.putString("PhoneNumber", text3.getText().toString());
                editor.commit();
                Toast.makeText(MainActivity.this ,"Data Saved", Toast.LENGTH_SHORT).show();
            }
        });
        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharPrefs = getSharedPreferences(FILE_SHARED_NAME, MODE_PRIVATE);
                String valorName = sharPrefs.getString("Name","No hi ha dades");
                String valorEmail = sharPrefs.getString("Mail","No hi ha dades");
                String valorPhone = sharPrefs.getString("PhoneNumber","No hi ha dades");
                Toast.makeText(getApplicationContext(), "Fields Read: " + valorName + " " +
                        valorEmail + " " + valorPhone, Toast.LENGTH_SHORT).show();
                text1.setText(valorName);
                text2.setText(valorEmail);
                text3.setText(valorPhone);


            }
        });
    }
}