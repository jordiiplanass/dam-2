package com.example.soccer;


import android.os.Parcel;
import android.os.Parcelable;

public class Country implements Parcelable {
    public String name;
    public int flag;

    public Country(String name, int flag) {
        this.name = name;
        this.flag = flag;
    }

    protected Country(Parcel in) {
        name = in.readString();
        flag = in.readInt();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(flag);
    }
}
