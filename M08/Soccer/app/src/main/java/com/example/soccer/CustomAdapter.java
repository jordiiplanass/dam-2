package com.example.soccer;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter {

    LayoutInflater layoutInflater;

    public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Country> countries) {
        super(context, resource, countries);
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = layoutInflater.inflate(R.layout.custom_spinner_row, null, true);
        Country country = (Country) getItem(position);
        ImageView spinnerImage = rowView.findViewById(R.id.spinnerImage);
        TextView spinnerText = rowView.findViewById(R.id.spinnerText);
        spinnerImage.setImageResource(country.flag);
        spinnerText.setText(country.name);
        return rowView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_spinner_row, null, true);
        }
        Country country = (Country) getItem(position);
        ImageView spinnerImage = convertView.findViewById(R.id.spinnerImage);
        TextView spinnerText = convertView.findViewById(R.id.spinnerText);
        spinnerImage.setImageResource(country.flag);
        spinnerText.setText(country.name);
        return convertView;
    }
}
