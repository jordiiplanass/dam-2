package com.example.soccer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter2 extends RecyclerView.Adapter<CustomAdapter2.MyViewHolder> {
    private Context context;
    private ArrayList<CountryExtended> countryExtendeds;

    public CustomAdapter2(Context context, ArrayList<CountryExtended> countryExtendeds) {
        this.context = context;
        this.countryExtendeds = countryExtendeds;
    }

    @NonNull
    @Override
    public CustomAdapter2.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.custom_recycler_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapter2.MyViewHolder holder, int position) {
        Picasso.get()
                .load(countryExtendeds.get(position).strBadge)
                .fit()
                .centerCrop()
                .into(holder.mainImage);
        holder.title.setText(countryExtendeds.get(position).strLeague);
        holder.description.setText(countryExtendeds.get(position).strDescriptionEN);
        holder.buttonWeb.setOnClickListener(v ->{
            String website = countryExtendeds.get(position).strWebsite;
            if (website.length() > 0){

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("https://" + countryExtendeds.get(position).strWebsite));
                context.startActivity(intent);
                //Toast.makeText(context, website, Toast.LENGTH_SHORT).show();
            }
        });
        holder.buttonImages.setOnClickListener(v -> {
            Toast.makeText(context, "todo", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return countryExtendeds.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mainImage;
        TextView title;
        TextView description;
        Button buttonWeb;
        Button buttonImages;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mainImage = itemView.findViewById(R.id.mainImage);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            buttonWeb = itemView.findViewById(R.id.buttonWeb);
            buttonImages = itemView.findViewById(R.id.buttonImages);
        }
    }
}
