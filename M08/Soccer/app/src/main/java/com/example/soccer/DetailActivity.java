package com.example.soccer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CustomAdapter2 customAdapter;
    ArrayList<CountryExtended> entries = new ArrayList<>();
    String url;
    // www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=England&s=Soccer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        hookData();

        Country country = getIntent().getParcelableExtra("data");
        url = "https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=" + country.name + "&s=Soccer";
        //getAllEntries();
        getAllEntries();
    }

    private void hookData() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    private void getAllEntries() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject  response) {
                    try {

                        JSONArray jsonArray = response.getJSONArray("countrys");

                        for (int i = 0; i < response.getJSONArray("countrys").length(); i++) {
                            try {
                                JSONObject peakObject = jsonArray.getJSONObject(i);
                                CountryExtended countryExtended = new CountryExtended(); //empty constructor
                                countryExtended.strBadge = peakObject.getString("strBadge");
                                countryExtended.strLeague = peakObject.getString("strLeague");
                                countryExtended.strDescriptionEN = peakObject.getString("strDescriptionEN");
                                countryExtended.strWebsite = peakObject.getString("strWebsite");
                                countryExtended.strFanart1 = peakObject.getString("strFanart1");
                                countryExtended.strFanart2 = peakObject.getString("strFanart2");
                                countryExtended.strFanart3 = peakObject.getString("strFanart3");
                                countryExtended.strFanart4 = peakObject.getString("strFanart4");

                                entries.add(countryExtended);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    recyclerView.setLayoutManager(new
                            LinearLayoutManager(getApplicationContext()));
                    CustomAdapter2 adaptador = new CustomAdapter2(getApplicationContext(), entries);
                    recyclerView.setAdapter(adaptador);
                }

            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(DetailActivity.this, "El pepe", Toast.LENGTH_SHORT).show();
                }
            });

        queue.add(jsonObjectRequest);
    }
}