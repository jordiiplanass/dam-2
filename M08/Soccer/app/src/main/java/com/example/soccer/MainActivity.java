package com.example.soccer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner;
    private Button buttonDetail;

    private ArrayList<Country> countries;
    private Country selectedCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        spinnerData();
        buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("data", selectedCountry);
                startActivity(intent);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get Selected value name from the list
                selectedCountry = (Country) parent.getItemAtPosition(position);
                Toast.makeText(MainActivity.this, selectedCountry.name , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, "Nothing Selected",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }




    private void initData() {
        buttonDetail = findViewById(R.id.buttonDetail);
        countries = new ArrayList<>();
        countries.add(new Country("Argentina", R.drawable.argentina));
        countries.add(new Country("Australia", R.drawable.australia));
        countries.add(new Country("Canada", R.drawable.canada));
        countries.add(new Country("Congo", R.drawable.congo));
        countries.add(new Country("Egypt", R.drawable.egypt));
        countries.add(new Country("France", R.drawable.france));
        countries.add(new Country("Germany", R.drawable.germany));
        countries.add(new Country("Greece", R.drawable.greece));
        countries.add(new Country("India", R.drawable.india));
        countries.add(new Country("Indonesia", R.drawable.indonesia));
        countries.add(new Country("Ireland", R.drawable.ireland));
        countries.add(new Country("Italy", R.drawable.italy));
        countries.add(new Country("Kenya", R.drawable.kenya));
        countries.add(new Country("Mexico", R.drawable.mexico));
        countries.add(new Country("Newzeland", R.drawable.newzeland));
        countries.add(new Country("Norway", R.drawable.norway));
        countries.add(new Country("Poland", R.drawable.poland));
        countries.add(new Country("Qatar", R.drawable.qatar));
        countries.add(new Country("SouthAfrica", R.drawable.southafrica));
        countries.add(new Country("SouthKorea", R.drawable.southkorea));
        countries.add(new Country("Spain", R.drawable.spain));
        countries.add(new Country("United Kingdom", R.drawable.uk));
        countries.add(new Country("United States of America", R.drawable.usa));
    }
    private void spinnerData() {
        spinner = findViewById(R.id.spinner);
        CustomAdapter customAdapter = new CustomAdapter(this, R.layout.custom_spinner_row, countries);
        spinner.setAdapter(customAdapter);
    }
}