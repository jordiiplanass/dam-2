package com.example.edt72;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class InfoActivity extends AppCompatActivity {

    private TextView title_top;
    private TextView author_top;
    private TextView year_top;
    private ImageView image_top;
    private ListView listView1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        title_top = findViewById(R.id.title_top);
        author_top = findViewById(R.id.author_top);
        year_top = findViewById(R.id.year_top);
        image_top = findViewById(R.id.image_top);
        listView1 = findViewById(R.id.listView1);

        //ListView


        // Intent
        Intent i = getIntent();

        String disk = i.getStringExtra(MainActivity.PROD_DISK);
        String title = i.getStringExtra(MainActivity.PROD_TITLE);
        String author = i.getStringExtra(MainActivity.PROD_AUTHOR);
        String year = i.getStringExtra(MainActivity.PROD_YEAR);
        int image = i.getIntExtra(MainActivity.PROD_IMAGE, R.drawable.music_icon);

        title_top.setText(title);
        author_top.setText(author);
        year_top.setText(year);
        image_top.setImageResource(image);


    }

}