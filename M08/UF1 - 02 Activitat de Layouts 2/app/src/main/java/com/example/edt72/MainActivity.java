package com.example.edt72;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static String PROD_DISK = "com.example.PROD_DISK";
    public static String PROD_TITLE = "com.example.PROD_TITLE";
    public static String PROD_AUTHOR = "com.example.PROD_AUTHOR";
    public static String PROD_YEAR = "com.example.PROD_YEAR";
    public static String PROD_IMAGE = "com.example.PROD_IMAGE";
    private CardView card1;
    private CardView card2;
    private CardView card3;
    private CardView card4;
    private CardView card5;
    private CardView card6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        card1 = findViewById(R.id.card1);
        card2 = findViewById(R.id.card2);
        card3 = findViewById(R.id.card3);
        card4 = findViewById(R.id.card4);
        card5 = findViewById(R.id.card5);
        card6 = findViewById(R.id.card6);


        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String disk = getString(R.string.disk1);
                String title = getString(R.string.disk1_title);
                int image = R.drawable.disk1;
                String author = getString(R.string.disk1_author);
                String year = getString(R.string.disk1_year);

                ArrayList<String> songs = new ArrayList<>();
                songs.add(getString(R.string.disk1_song1));
                songs.add(getString(R.string.disk1_song2));
                songs.add(getString(R.string.disk1_song3));

                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(PROD_DISK, disk);
                intent.putExtra(PROD_TITLE, title);
                intent.putExtra(PROD_IMAGE, image);
                intent.putExtra(PROD_AUTHOR, author);
                intent.putExtra(PROD_YEAR, year);
                startActivity(intent);
            }
        });
        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.disk2_title);
                int image = R.drawable.disk2;
                String author = getString(R.string.disk2_author);
                String year = getString(R.string.disk2_year);

                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(PROD_TITLE, title);
                intent.putExtra(PROD_IMAGE, image);
                intent.putExtra(PROD_AUTHOR, author);
                intent.putExtra(PROD_YEAR, year);
                startActivity(intent);
            }
        });
        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.disk3_title);
                int image = R.drawable.disk3;
                String author = getString(R.string.disk3_author);
                String year = getString(R.string.disk3_year);

                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(PROD_TITLE, title);
                intent.putExtra(PROD_IMAGE, image);
                intent.putExtra(PROD_AUTHOR, author);
                intent.putExtra(PROD_YEAR, year);
                startActivity(intent);
            }
        });
        card4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.disk4_title);
                int image = R.drawable.disk4;
                String author = getString(R.string.disk4_author);
                String year = getString(R.string.disk4_year);

                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(PROD_TITLE, title);
                intent.putExtra(PROD_IMAGE, image);
                intent.putExtra(PROD_AUTHOR, author);
                intent.putExtra(PROD_YEAR, year);
                startActivity(intent);
            }
        });
        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.disk5_title);
                int image = R.drawable.disk5;
                String author = getString(R.string.disk4_author);
                String year = getString(R.string.disk5_year);

                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(PROD_TITLE, title);
                intent.putExtra(PROD_IMAGE, image);
                intent.putExtra(PROD_AUTHOR, author);
                intent.putExtra(PROD_YEAR, year);
                startActivity(intent);
            }
        });
        card6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.disk6_title);
                int image = R.drawable.disk6;
                String author = getString(R.string.disk6_author);
                String year = getString(R.string.disk6_year);

                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(PROD_TITLE, title);
                intent.putExtra(PROD_IMAGE, image);
                intent.putExtra(PROD_AUTHOR, author);
                intent.putExtra(PROD_YEAR, year);
                startActivity(intent);
            }
        });


    }
}