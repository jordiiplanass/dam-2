package com.example.calculadora;

public class Calculator {
    public double suma (double op1, double op2){
        return op1 + op2;
    }
    public double resta (double op1, double op2){
        return op1 - op2;
    }
    public double multi (double op1, double op2){
        return op1 * op2;
    }
    public double divi (double op1, double op2){
        return op1 / op2;
    }
    public double expo (double op1, double op2){
        return Math.pow(op1,op2);
    }
}
