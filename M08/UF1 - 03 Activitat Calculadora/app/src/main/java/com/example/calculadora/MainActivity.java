package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView resultat;
    private TextView textEntry1;
    private TextView textEntry2;
    private Button buttonSuma;
    private Button buttonResta;
    private Button buttonMulti;
    private Button buttonDivision;
    private Button buttonExponent;
    private Button buttonInfo;
    Calculator cal = new Calculator();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultat = findViewById(R.id.textResult);
        textEntry1 = findViewById(R.id.textEntry1);
        textEntry2 = findViewById(R.id.textEntry2);
        buttonSuma = findViewById(R.id.buttonPlus);
        buttonResta = findViewById(R.id.buttonMinus);
        buttonMulti = findViewById(R.id.buttonPower);
        buttonDivision = findViewById(R.id.buttonDivision);
        buttonExponent = findViewById(R.id.buttonExponent);

    }



    public void buttonSumClick (View view){
        if (textEntry1.getText().toString().trim().isEmpty() ||
                textEntry2.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Enter a number", Toast.LENGTH_SHORT).show();
        } else {
            double op1 = Double.parseDouble(textEntry1.getText().toString().trim());
            double op2 = Double.parseDouble(textEntry2.getText().toString().trim());

            resultat.setText(cal.suma(op1, op2) + "");
        }
    }
    public void buttonRestaClick (View view){
        if (textEntry1.getText().toString().trim().isEmpty() ||
                textEntry2.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Enter a number", Toast.LENGTH_SHORT).show();
        } else {
            double op1 = Double.parseDouble(textEntry1.getText().toString().trim());
            double op2 = Double.parseDouble(textEntry2.getText().toString().trim());

            resultat.setText(cal.resta(op1, op2) + "");
        }
    }
    public void buttonMultiClick (View view){
        if (textEntry1.getText().toString().trim().isEmpty() ||
                textEntry2.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Enter a number", Toast.LENGTH_SHORT).show();
        } else {
            double op1 = Double.parseDouble(textEntry1.getText().toString().trim());
            double op2 = Double.parseDouble(textEntry2.getText().toString().trim());

            resultat.setText((double) Math.round(cal.multi(op1, op2) * 10000) / 10000 + "");
        }
    }
    public void buttonDiviClick (View view){
        if (textEntry1.getText().toString().trim().isEmpty() ||
                textEntry2.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Enter a number", Toast.LENGTH_SHORT).show();
        } else {
            double op1 = Double.parseDouble(textEntry1.getText().toString().trim());
            double op2 = Double.parseDouble(textEntry2.getText().toString().trim());

            resultat.setText((double) Math.round(cal.divi(op1, op2) * 10000) / 10000 + "");
        }
    }
    public void buttonExpoClick (View view){
        if (textEntry1.getText().toString().trim().isEmpty() ||
                textEntry2.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Enter a number", Toast.LENGTH_SHORT).show();
        } else {
            double op1 = Double.parseDouble(textEntry1.getText().toString().trim());
            double op2 = Double.parseDouble(textEntry2.getText().toString().trim());

            resultat.setText((double) Math.round(cal.expo(op1, op2) * 10000) / 10000 + "");
        }
    }
    public void buttonClearClick (View view){
        resultat.setText("0.0");
        textEntry1.setText("");
        textEntry2.setText("");

    }
    public void buttonInfoClick(View view){
        Intent intent = new Intent(MainActivity.this, infoActivity.class);
        startActivity(intent);
    }
}