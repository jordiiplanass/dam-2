package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class infoActivity extends AppCompatActivity {

    private ImageView imageLogo;
    private TextView textHorse;
    private Switch switchTest;
    String url = "http://endless.horse/";
    String url2 = "https://www.youtube.com/watch?v=dQw4w9WgXcQ/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        textHorse = findViewById(R.id.textHorse);
        imageLogo = findViewById(R.id.imageLogo);
        switchTest = findViewById(R.id.switchTest);


        // clickable url
        textHorse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri link = Uri.parse(url);
                Intent i = new Intent(Intent.ACTION_VIEW, link);
                startActivity(i);
            }
        });

        // clickable image
        imageLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri link = Uri.parse(url2);
                Intent i = new Intent(Intent.ACTION_VIEW, link);
                startActivity(i);
            }
        });

        switchTest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                b + "", Toast.LENGTH_SHORT);
                toast1.show();
            }
        });
    }
}