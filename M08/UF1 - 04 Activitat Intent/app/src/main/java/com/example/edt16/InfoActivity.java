package com.example.edt16;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    private TextView title;
    private ImageView image1;
    private ImageView image2;
    private TextView text1;
    private TextView text2;
    private ImageView image3;
    private ImageView image4;
    private Button buttonHide;
    private LinearLayout linearLayout1;
    private int hideStatus = 0;


    private ObjectAnimator objectAnimator1;
    private ObjectAnimator objectAnimator2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        linearLayout1 = findViewById(R.id.linearLayout1);

        title = findViewById(R.id.title);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        buttonHide = findViewById(R.id.buttonHide);

        Intent intent = getIntent();

        String title_text = intent.getStringExtra(MainActivity.GAME_NAME);
        int image_main = intent.getIntExtra(MainActivity.GAME_IMAGE, R.drawable.main_logo);
        int image_back = intent.getIntExtra(MainActivity.GAME_BACKGROUND, R.drawable.main_background);
        String description = intent.getStringExtra(MainActivity.GAME_DESCRIPTION);
        String developer = intent.getStringExtra(MainActivity.GAME_DEVELOPER);
        int image_art = intent.getIntExtra(MainActivity.GAME_ART, R.drawable.main_background);
        int image_art2 = intent.getIntExtra(MainActivity.GAME_ART2, R.drawable.main_background);

        title.setText(title_text);
        image1.setImageResource(image_main);
        image2.setImageResource(image_back);
        text1.setText(description);
        text2.setText(developer);
        image3.setImageResource(image_art);
        image4.setImageResource(image_art2);

        buttonHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hideStatus == 0) {
                    objectAnimator1 = ObjectAnimator.ofFloat(linearLayout1, "alpha", 1f, 0f);
                    objectAnimator1.setDuration(1000);
                    hideStatus++;
                } else {
                    objectAnimator1 = ObjectAnimator.ofFloat(linearLayout1, "alpha", 0f, 1f);
                    objectAnimator1.setDuration(1000);

                    hideStatus = 0;
                }
                AnimatorSet set = new AnimatorSet();
                set.playTogether(objectAnimator1);
                set.start();
            }
        });

    }
}