package com.example.edt16;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static String GAME_IMAGE = "com.example.GAME_IMAGE";
    public static String GAME_NAME = "com.example.GAME_NAME";
    public static String GAME_BACKGROUND = "com.example.GAME_BACKGROUND";
    public static String GAME_DESCRIPTION = "com.example.GAME_DESCRIPTION";
    public static String GAME_DEVELOPER = "com.example.GAME_DEVELOPER";
    public static String GAME_ART = "com.example.GAME_ART";
    public static String GAME_ART2 = "com.example.GAME_ART2";

    private Button buttonAbout;
    private CardView cardView1;
    private CardView cardView2;
    private CardView cardView3;
    private CardView cardView4;
    private CardView cardView5;
    private CardView cardView6;
    private CardView cardView7;
    private CardView cardView8;
    private CardView cardView9;
    private CardView cardView10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardView1 = findViewById(R.id.cardView1);
        cardView2 = findViewById(R.id.cardView2);
        cardView3 = findViewById(R.id.cardView3);
        cardView4 = findViewById(R.id.cardView4);
        cardView5 = findViewById(R.id.cardView5);
        cardView6 = findViewById(R.id.cardView6);
        cardView7 = findViewById(R.id.cardView7);
        cardView8 = findViewById(R.id.cardView8);
        cardView9 = findViewById(R.id.cardView9);
        cardView10 = findViewById(R.id.cardView10);
        buttonAbout = findViewById(R.id.buttonAbout);

        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game1);
                int main = R.drawable.game1_main;
                int background = R.drawable.game1_background;
                String description = getString(R.string.game1_desc);
                String developer = getString(R.string.game1_developer);
                int art = R.drawable.game1_art;
                int art2 = R.drawable.game1_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game2);
                int main = R.drawable.game2_main;
                int background = R.drawable.game2_background;
                String description = getString(R.string.game2_desc);
                String developer = getString(R.string.game2_developer);
                int art = R.drawable.game2_art;
                int art2 = R.drawable.game2_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game3);
                int main = R.drawable.game3_main;
                int background = R.drawable.game3_background;
                String description = getString(R.string.game3_desc);
                String developer = getString(R.string.game3_developer);
                int art = R.drawable.game3_art;
                int art2 = R.drawable.game3_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game4);
                int main = R.drawable.game4_main;
                int background = R.drawable.game4_background;
                String description = getString(R.string.game4_desc);
                String developer = getString(R.string.game4_developer);
                int art = R.drawable.game4_art;
                int art2 = R.drawable.game4_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game5);
                int main = R.drawable.game5_main;
                int background = R.drawable.game5_background;
                String description = getString(R.string.game5_desc);
                String developer = getString(R.string.game5_developer);
                int art = R.drawable.game5_art;
                int art2 = R.drawable.game5_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game6);
                int main = R.drawable.game6_main;
                int background = R.drawable.game6_background;
                String description = getString(R.string.game6_desc);
                String developer = getString(R.string.game6_developer);
                int art = R.drawable.game6_art;
                int art2 = R.drawable.game6_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game7);
                int main = R.drawable.game7_main;
                int background = R.drawable.game7_background;
                String description = getString(R.string.game7_desc);
                String developer = getString(R.string.game7_developer);
                int art = R.drawable.game7_art;
                int art2 = R.drawable.game7_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game8);
                int main = R.drawable.game8_main;
                int background = R.drawable.game8_background;
                String description = getString(R.string.game8_desc);
                String developer = getString(R.string.game8_developer);
                int art = R.drawable.game8_art;
                int art2 = R.drawable.game8_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game9);
                int main = R.drawable.game9_main;
                int background = R.drawable.game9_background;
                String description = getString(R.string.game9_desc);
                String developer = getString(R.string.game9_developer);
                int art = R.drawable.game9_art;
                int art2 = R.drawable.game9_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        cardView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // carregar putExtra
                String title = getString(R.string.game10);
                int main = R.drawable.game10_main;
                int background = R.drawable.game10_background;
                String description = getString(R.string.game10_desc);
                String developer = getString(R.string.game10_developer);
                int art = R.drawable.game10_art;
                int art2 = R.drawable.game10_art2;


                // Intent
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(GAME_IMAGE, main);
                intent.putExtra(GAME_NAME, title);
                intent.putExtra(GAME_BACKGROUND, background);
                intent.putExtra(GAME_DESCRIPTION, description);
                intent.putExtra(GAME_DEVELOPER, developer);
                intent.putExtra(GAME_ART, art);
                intent.putExtra(GAME_ART2, art2);
                startActivity(intent);
            }
        });
        buttonAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, aboutActivity.class);
                startActivity(intent);
            }
        });
    }

}