package com.example.layout2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public static String HOTEL_TEXT = "com.example.HOTEL_TEXT";
    public static String HOTEL_IMAGE = "com.example.HOTEL_IMAGE";

    private ImageView image1;
    private ImageView image2;
    private ImageView image3;
    private ImageView image4;
    private ImageView image5;
    private CardView card1;
    private CardView card2;
    private CardView card3;
    private CardView card4;
    private CardView card5;
    private CardView card1_2;
    private CardView card2_2;
    private CardView card3_2;
    private CardView card4_2;
    private CardView card5_2;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;

    private int imageState = 0;
    private int imageState2 = 0;
    private int imageState3 = 0;
    private int imageState4 = 0;
    private int imageState5 = 0;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        card1 = findViewById(R.id.card1);
        card2 = findViewById(R.id.card2);
        card3 = findViewById(R.id.card3);
        card4 = findViewById(R.id.card4);
        card5 = findViewById(R.id.card5);
        card1_2 = findViewById(R.id.card1_2);
        card2_2 = findViewById(R.id.card2_2);
        card3_2 = findViewById(R.id.card3_2);
        card4_2 = findViewById(R.id.card4_2);
        card5_2 = findViewById(R.id.card5_2);

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageState == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(card1, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image1, "alpha", 1f, 0.3f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card1_2, "alpha", 0f, 1f);
                    objectAnimator3.setStartDelay(1000);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card1_2, "RotationY", 0f, 180f);
                    objectAnimator4.setDuration(50);
                    button1.setEnabled(true);
                    imageState++;
                } else {
                    imageState = 0;
                    button1.setEnabled(false);
                    objectAnimator1 = ObjectAnimator.ofFloat(card1, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image1, "alpha", 0.3f, 1f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card1_2, "alpha", 1f, 0f);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card1_2, "RotationY", 0f, 180f);
                    objectAnimator4.setStartDelay(1000);
                    objectAnimator4.setDuration(50);
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageState2 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(card2, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image2, "alpha", 1f, 0.3f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card2_2, "alpha", 0f, 1f);
                    objectAnimator3.setStartDelay(1000);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card2_2, "RotationY", 0f, 180f);
                    objectAnimator4.setDuration(50);
                    button2.setEnabled(true);
                    imageState2++;
                } else {
                    imageState2 = 0;
                    objectAnimator1 = ObjectAnimator.ofFloat(card2, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image2, "alpha", 0.3f, 1f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card2_2, "alpha", 1f, 0f);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card2_2, "RotationY", 0f, 180f);
                    objectAnimator4.setStartDelay(1000);
                    objectAnimator4.setDuration(50);
                    button2.setEnabled(false);
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageState3 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(card3, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image3, "alpha", 1f, 0.3f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card3_2, "alpha", 0f, 1f);
                    objectAnimator3.setStartDelay(1000);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card3_2, "RotationY", 0f, 180f);
                    objectAnimator4.setDuration(50);
                    button3.setEnabled(true);
                    imageState3++;
                } else {
                    imageState3 = 0;
                    objectAnimator1 = ObjectAnimator.ofFloat(card3, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image3, "alpha", 0.3f, 1f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card3_2, "alpha", 1f, 0f);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card3_2, "RotationY", 0f, 180f);
                    objectAnimator4.setStartDelay(1000);
                    objectAnimator4.setDuration(50);
                    button3.setEnabled(false);
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });
        image4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageState4 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(card4, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image4, "alpha", 1f, 0.3f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card4_2, "alpha", 0f, 1f);
                    objectAnimator3.setStartDelay(1000);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card4_2, "RotationY", 0f, 180f);
                    objectAnimator4.setDuration(50);
                    imageState4++;
                    button4.setEnabled(true);
                } else {
                    imageState4 = 0;
                    objectAnimator1 = ObjectAnimator.ofFloat(card4, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image4, "alpha", 0.3f, 1f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card4_2, "alpha", 1f, 0f);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card4_2, "RotationY", 0f, 180f);
                    objectAnimator4.setStartDelay(1000);
                    objectAnimator4.setDuration(50);
                    button4.setEnabled(false);
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });
        image5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageState5 == 0){
                    objectAnimator1 = ObjectAnimator.ofFloat(card5, "RotationY", 0f, 180f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image5, "alpha", 1f, 0.3f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card5_2, "alpha", 0f, 1f);
                    objectAnimator3.setStartDelay(1000);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card5_2, "RotationY", 0f, 180f);
                    objectAnimator4.setDuration(50);
                    button5.setEnabled(true);
                    imageState5++;
                } else {
                    imageState5 = 0;
                    objectAnimator1 = ObjectAnimator.ofFloat(card5, "RotationY", 180f, 0f);
                    objectAnimator1.setDuration(1000);
                    objectAnimator2 = ObjectAnimator.ofFloat(image5, "alpha", 0.3f, 1f);
                    objectAnimator2.setDuration(1000);
                    objectAnimator3 = ObjectAnimator.ofFloat(card5_2, "alpha", 1f, 0f);
                    objectAnimator3.setDuration(50);
                    objectAnimator4 = ObjectAnimator.ofFloat(card5_2, "RotationY", 0f, 180f);
                    objectAnimator4.setStartDelay(1000);
                    objectAnimator4.setDuration(50);
                    button5.setEnabled(true);
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                intent.putExtra(HOTEL_IMAGE, R.drawable.hotel1);
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                intent.putExtra(HOTEL_IMAGE, R.drawable.hotel2);
                startActivity(intent);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                intent.putExtra(HOTEL_IMAGE, R.drawable.hotel3);
                startActivity(intent);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                intent.putExtra(HOTEL_IMAGE, R.drawable.hotel4);
                startActivity(intent);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                intent.putExtra(HOTEL_IMAGE, R.drawable.hotel5);
                startActivity(intent);
            }
        });
    }
}