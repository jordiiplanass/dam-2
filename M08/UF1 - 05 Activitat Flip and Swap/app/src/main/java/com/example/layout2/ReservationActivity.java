package com.example.layout2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ReservationActivity extends AppCompatActivity {

    private ImageView image1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        image1 = findViewById(R.id.image1);

        Intent intent = getIntent();

        int image = intent.getIntExtra(MainActivity.HOTEL_IMAGE, R.drawable.hotel1);

        image1.setImageResource(image);

    }
}