package com.example.edt02;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DashBoard extends AppCompatActivity {

    public static String MOTO_IMAGE = "com.example.MOTO_IMAGE";

    private ImageView image2;
    private ImageView imageTitle;
    private ImageView moto1;
    private ImageView moto2;
    private ImageView moto3;
    private ImageView moto4;
    private ImageView moto5;
    private ImageView moto6;
    private ImageView ovni;
    private ImageView bomb;
    private ImageView explosion;

    private TextView text1;
    private TextView text4;

    private ObjectAnimator objectAnimator;
    private ObjectAnimator objectAnimator2;
    private ObjectAnimator objectAnimator3;
    private ObjectAnimator objectAnimator4;
    private ObjectAnimator objectAnimator5;
    private ObjectAnimator objectAnimator6;
    private ObjectAnimator objectAnimator7;
    private ObjectAnimator objectAnimator8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);



        //hooks
        image2 = findViewById(R.id.image2);
        imageTitle = findViewById(R.id.imageTitle);
        text1 = findViewById(R.id.text1);
        text4 = findViewById(R.id.text4);
        moto1 = findViewById(R.id.moto1);
        moto2 = findViewById(R.id.moto2);
        moto3 = findViewById(R.id.moto3);
        moto4 = findViewById(R.id.moto4);
        moto5 = findViewById(R.id.moto5);
        moto6 = findViewById(R.id.moto6);
        ovni = findViewById(R.id.ovni);
        bomb = findViewById(R.id.bomb);
        explosion = findViewById(R.id.explosion);


        // startup animations

        objectAnimator = ObjectAnimator.ofFloat(imageTitle, "x", -500f, 70f);
        objectAnimator.setDuration(1500);
        objectAnimator.setStartDelay(1100);

        objectAnimator2 = ObjectAnimator.ofFloat(text1, "y", -500f, 200f);
        objectAnimator2.setDuration(1500);
        objectAnimator2.setStartDelay(1100);

        objectAnimator3 = ObjectAnimator.ofFloat(text4, "alpha", 0f, 1f);
        objectAnimator3.setDuration(2500);
        objectAnimator3.setStartDelay(1700);

        objectAnimator4 = ObjectAnimator.ofFloat(text4, "y", -400f, 300f);
        objectAnimator4.setDuration(1500);
        objectAnimator4.setStartDelay(1100);

        objectAnimator5 = ObjectAnimator.ofFloat(image2, "y", 0f, -2100f);
        objectAnimator5.setDuration(1500);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3,
                objectAnimator4, objectAnimator5);
        animatorSet.start();


        elevacion(moto1);
        elevacion(moto2);
        elevacion(moto3);
        elevacion(moto4);
        elevacion(moto5);
        elevacion(moto6);


        moto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                objectAnimator = ObjectAnimator.ofFloat(moto1, "x", 100f, 140f);
                objectAnimator.setDuration(400);

                objectAnimator2 = ObjectAnimator.ofFloat(moto1, "Rotation", 0f, -40f);
                objectAnimator2.setStartDelay(600);
                objectAnimator2.setDuration(1200);

                objectAnimator3 = ObjectAnimator.ofFloat(moto1, "x", 140f, 70f);
                objectAnimator3.setStartDelay(800);
                objectAnimator3.setDuration(400);

                objectAnimator4 = ObjectAnimator.ofFloat(moto1, "x", 70f, 2000f);
                objectAnimator4.setDuration(300);
                objectAnimator4.setStartDelay(1200);

                objectAnimator5 = ObjectAnimator.ofFloat(moto1, "x", 2000f, 100f);
                objectAnimator5.setStartDelay(1800);

                objectAnimator6 = ObjectAnimator.ofFloat(moto1, "Rotation", -40f, 0f);
                objectAnimator6.setStartDelay(1800);



                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3,
                        objectAnimator4, objectAnimator5, objectAnimator6);
                animatorSet.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        int image = R.drawable.motorcycle;
                        intent.putExtra(MOTO_IMAGE, image);
                        startActivity(intent);
                    }
                }, 1500);



            }
        });
        moto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectAnimator = ObjectAnimator.ofFloat(moto2, "Rotation", 360f, 00f);
                objectAnimator.setDuration(1200);

                objectAnimator2 = ObjectAnimator.ofFloat(moto2, "x", 568f, 450f);
                objectAnimator2.setDuration(1200);

                objectAnimator3 = ObjectAnimator.ofFloat(moto2, "x", 450f, 3000f);
                objectAnimator3.setDuration(300);
                objectAnimator3.setStartDelay(1200);

                objectAnimator4 = ObjectAnimator.ofFloat(moto2, "x", 3000f, 568f);
                objectAnimator4.setStartDelay(1800);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        int image = R.drawable.motorcycle2;
                        intent.putExtra(MOTO_IMAGE, image);
                        startActivity(intent);
                    }
                }, 1500);
            }
        });
        moto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                objectAnimator = ObjectAnimator.ofFloat(moto3, "Rotation", 0f, 360f);
                objectAnimator.setDuration(1200);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator);
                animatorSet.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        int image = R.drawable.motorcycle3;
                        intent.putExtra(MOTO_IMAGE, image);
                        startActivity(intent);
                    }
                }, 1200);
            }
        });
        moto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                objectAnimator = ObjectAnimator.ofFloat(moto4, "Rotation", 0f, 360f);
                objectAnimator.setDuration(1200);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator);
                animatorSet.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        int image = R.drawable.motorcycle4;
                        intent.putExtra(MOTO_IMAGE, image);
                        startActivity(intent);
                    }
                }, 1200);
            }
        });
        moto5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                objectAnimator = ObjectAnimator.ofFloat(bomb, "y", -500f, 1500f);
                objectAnimator.setDuration(900);

                objectAnimator2 = ObjectAnimator.ofFloat(bomb, "x", 500f, 0f);
                objectAnimator2.setDuration(900);

                objectAnimator3 = ObjectAnimator.ofFloat(explosion, "alpha", 0f, 1f);
                objectAnimator3.setStartDelay(800);

                objectAnimator4 = ObjectAnimator.ofFloat(bomb, "alpha", 1f, 0f);
                objectAnimator4.setStartDelay(900);

                objectAnimator5 = ObjectAnimator.ofFloat(explosion, "alpha", 1f, 0f);
                objectAnimator5.setStartDelay(1200);

                objectAnimator6 = ObjectAnimator.ofFloat(bomb, "y", 1500f, -900f);
                objectAnimator6.setStartDelay(1200);

                objectAnimator7 = ObjectAnimator.ofFloat(bomb, "x", 0f, 900f);
                objectAnimator7.setStartDelay(1200);

                objectAnimator8 = ObjectAnimator.ofFloat(bomb, "alpha", 0f, 1f);
                objectAnimator8.setStartDelay(1200);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3,
                        objectAnimator4, objectAnimator5, objectAnimator6, objectAnimator7,
                        objectAnimator8);
                animatorSet.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        int image = R.drawable.motorcycle5;
                        intent.putExtra(MOTO_IMAGE, image);
                        startActivity(intent);
                    }
                }, 1100);
            }
        });
        moto6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                objectAnimator = ObjectAnimator.ofFloat(ovni, "x", -1500f, 200f);
                objectAnimator.setDuration(2000);

                objectAnimator2 = ObjectAnimator.ofFloat(moto6, "y", 1500f, 700f);
                objectAnimator2.setStartDelay(2000);
                objectAnimator2.setDuration(1500);

                objectAnimator3 = ObjectAnimator.ofFloat(ovni, "x", 200f, -1500f);
                objectAnimator3.setStartDelay(3600);
                objectAnimator3.setDuration(100);

                objectAnimator4 = ObjectAnimator.ofFloat(moto6, "y", 0f, 1500f);
                objectAnimator4.setStartDelay(3600);
                objectAnimator4.setDuration(100);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3,
                        objectAnimator4);
                animatorSet.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        int image = R.drawable.motorcycle6;
                        intent.putExtra(MOTO_IMAGE, image);
                        startActivity(intent);
                    }
                }, 3000);
            }
        });
    }
    public void elevacion (ImageView image1){

        image1.animate().alpha(1).translationY(-200).setStartDelay(1500).setDuration(1200);



    }
}