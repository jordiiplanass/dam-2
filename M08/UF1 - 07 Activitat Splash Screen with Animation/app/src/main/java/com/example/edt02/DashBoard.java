package com.example.edt02;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DashBoard extends AppCompatActivity {

    public static String MOTO_IMAGE = "com.example.MOTO_IMAGE";

    private ImageView image2;
    private ImageView imageTitle;

    private TextView text1;
    private TextView text4;
    private TextView signUpText;
    private TextView forgotText;
    private Button logButton;

    private View menuLayout;

    ObjectAnimator objectAnimator;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);


        //hooks
        image2 = findViewById(R.id.image2);
        imageTitle = findViewById(R.id.imageTitle);
        text1 = findViewById(R.id.text1);
        text4 = findViewById(R.id.text4);
        signUpText = findViewById(R.id.logInText);
        forgotText = findViewById(R.id.forgotText);
        menuLayout = findViewById(R.id.menuLayout);
        logButton = findViewById(R.id.logButton);

        // Start Animation
        objectAnimator = ObjectAnimator.ofFloat(text4, "alpha", 0f, 1f);
        objectAnimator.setDuration(2500);
        objectAnimator.setStartDelay(1700);

        objectAnimator2 = ObjectAnimator.ofFloat(text4, "y", -400f, 300f);
        objectAnimator2.setDuration(1500);
        objectAnimator2.setStartDelay(1100);

        objectAnimator3 = ObjectAnimator.ofFloat(image2, "y", 0f, -2100f);
        objectAnimator3.setDuration(1500);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3);
        animatorSet.start();

        // Layout animation
        layout_Start(menuLayout);

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard4.class);
                        startActivity(intent);
                    }
                }, 0);
            }
        });
        signUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click(signUpText);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard2.class);
                        startActivity(intent);
                    }
                }, 200);
            }
        });
        forgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click(forgotText);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard.this, DashBoard3.class);
                        startActivity(intent);
                    }
                }, 200);
            }
        });


    }
    public void layout_Start (View menuLayout){
        objectAnimator = ObjectAnimator.ofFloat(menuLayout, "scaleX", 0f, 1f);
        objectAnimator.setDuration(1200);
        objectAnimator.setStartDelay(600);

        objectAnimator2 = ObjectAnimator.ofFloat(menuLayout, "scaleY", 0f, 1f);
        objectAnimator2.setDuration(1200);
        objectAnimator2.setStartDelay(600);

        objectAnimator3 = ObjectAnimator.ofFloat(menuLayout, "alpha", 0f, 1f);
        objectAnimator3.setDuration(1200);
        objectAnimator3.setStartDelay(600);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3);
        animatorSet.start();

    }
    public void click (TextView text){

        objectAnimator = ObjectAnimator.ofFloat(text, "scaleX", 1f, 0.9f);
        objectAnimator.setDuration(100);
        objectAnimator2 = ObjectAnimator.ofFloat(text, "scaleX", 0.9f, 1f);
        objectAnimator2.setStartDelay(100);
        objectAnimator2.setDuration(100);

        objectAnimator3 = ObjectAnimator.ofFloat(text, "scaleY", 1f, 0.9f);
        objectAnimator3.setDuration(100);
        objectAnimator4 = ObjectAnimator.ofFloat(text, "scaleY", 0.9f, 1f);
        objectAnimator4.setStartDelay(100);
        objectAnimator4.setDuration(100);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }
}