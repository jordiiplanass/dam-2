package com.example.edt02;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class DashBoard2 extends AppCompatActivity {
    private DatePickerDialog picker;
    private TextView textDate;
    private TextView logInText;
    private Button logButton;
    private Spinner spinner;
    private Switch switchSlider;

    private ArrayList<Motorbike> motorbikeArrayList;
    private MotorbikeAdapter motorbikeAdapter;

    ObjectAnimator objectAnimator;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    private View menuLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board2);
        //hook
        textDate = findViewById(R.id.textDate);
        logInText = findViewById(R.id.logInText);
        menuLayout = findViewById(R.id.menuLayout);
        logButton = findViewById(R.id.logButton);
        spinner = findViewById(R.id.spinner);
        switchSlider = findViewById(R.id.switchSlider);
        // Animation
        layout_Start(menuLayout);

        // Clicks
        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashBoard2.this, "Mail sent\nConfirm your email", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard2.this, DashBoard.class);
                        startActivity(intent);
                    }
                }, 0);
            }
        });
        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Guardem el dia actual amb Calendar.getInstance()
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(DashBoard2.this,
                        android.R.style.Theme_Holo_Dialog,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                textDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        logInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                click(logInText);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard2.this, DashBoard.class);
                        startActivity(intent);
                    }
                }, 250);
            }
        });
        switchSlider.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Toast.makeText(DashBoard2.this, "SPAM 4 U", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Spinner
        motorbikeArrayList = new ArrayList<>();
        motorbikeArrayList.add(new Motorbike( "Moto 1", R.drawable.motorcycle));
        motorbikeArrayList.add(new Motorbike( "Moto 2", R.drawable.motorcycle2));
        motorbikeArrayList.add(new Motorbike( "Moto 3", R.drawable.motorcycle3));
        motorbikeArrayList.add(new Motorbike( "Moto 4", R.drawable.motorcycle4));
        motorbikeArrayList.add(new Motorbike( "Moto 5", R.drawable.motorcycle5));
        motorbikeArrayList.add(new Motorbike( "Moto 6", R.drawable.motorcycle6));

        motorbikeAdapter = new MotorbikeAdapter(this, motorbikeArrayList);
        spinner.setAdapter(motorbikeAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Motorbike selectedItem = (Motorbike) parent.getItemAtPosition(position);
                String selectedText = selectedItem.getModelName();
                Toast.makeText(DashBoard2.this, selectedText, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(DashBoard2.this, "Nothing Selected", Toast.LENGTH_SHORT);
            }
        });
    }
    public void layout_Start (View menuLayout){
        objectAnimator = ObjectAnimator.ofFloat(menuLayout, "scaleX", 0f, 1f);
        objectAnimator.setDuration(1200);

        objectAnimator2 = ObjectAnimator.ofFloat(menuLayout, "scaleY", 0f, 1f);
        objectAnimator2.setDuration(1200);

        objectAnimator3 = ObjectAnimator.ofFloat(menuLayout, "alpha", 0f, 1f);
        objectAnimator3.setDuration(1200);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3);
        animatorSet.start();

    }
    public void click (TextView text){

        objectAnimator = ObjectAnimator.ofFloat(text, "scaleX", 1f, 0.9f);
        objectAnimator.setDuration(100);
        objectAnimator2 = ObjectAnimator.ofFloat(text, "scaleX", 0.9f, 1f);
        objectAnimator2.setStartDelay(100);
        objectAnimator2.setDuration(100);

        objectAnimator3 = ObjectAnimator.ofFloat(text, "scaleY", 1f, 0.9f);
        objectAnimator3.setDuration(100);
        objectAnimator4 = ObjectAnimator.ofFloat(text, "scaleY", 0.9f, 1f);
        objectAnimator4.setStartDelay(100);
        objectAnimator4.setDuration(100);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3, objectAnimator4);
        animatorSet.start();

    }
}