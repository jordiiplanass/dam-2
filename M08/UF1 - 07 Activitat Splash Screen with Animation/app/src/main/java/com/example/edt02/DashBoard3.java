package com.example.edt02;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DashBoard3 extends AppCompatActivity {


    private View layout2;
    private TextView logInText;
    private Button logButton;

    ObjectAnimator objectAnimator;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board3);

        // Hook
        layout2 = findViewById(R.id.layout2);
        logInText = findViewById(R.id.logInText);
        logButton = findViewById(R.id.logButton);
        // Animations
        layout_Start(layout2);
        // Intent
        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashBoard3.this, "Mail sent\nCheck your email", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard3.this, DashBoard.class);
                        startActivity(intent);
                    }
                }, 350);
            }
        });
        logInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(logInText);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(DashBoard3.this, DashBoard.class);
                        startActivity(intent);
                    }
                }, 200);
            }
        });
    }
    public void layout_Start (View menuLayout){
        objectAnimator = ObjectAnimator.ofFloat(menuLayout, "scaleX", 0f, 1f);
        objectAnimator.setDuration(1200);

        objectAnimator2 = ObjectAnimator.ofFloat(menuLayout, "scaleY", 0f, 1f);
        objectAnimator2.setDuration(1200);

        objectAnimator3 = ObjectAnimator.ofFloat(menuLayout, "alpha", 0f, 1f);
        objectAnimator3.setDuration(1200);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2, objectAnimator3);
        animatorSet.start();

    }
    public void click (TextView text){

        objectAnimator = ObjectAnimator.ofFloat(text, "scaleX", 1f, 0.9f);
        objectAnimator.setDuration(100);
        objectAnimator2 = ObjectAnimator.ofFloat(text, "scaleX", 0.9f, 1f);
        objectAnimator2.setStartDelay(100);
        objectAnimator2.setDuration(100);

        objectAnimator3 = ObjectAnimator.ofFloat(text, "scaleY", 1f, 0.9f);
        objectAnimator3.setDuration(100);
        objectAnimator4 = ObjectAnimator.ofFloat(text, "scaleY", 0.9f, 1f);
        objectAnimator4.setStartDelay(100);
        objectAnimator4.setDuration(100);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }
}