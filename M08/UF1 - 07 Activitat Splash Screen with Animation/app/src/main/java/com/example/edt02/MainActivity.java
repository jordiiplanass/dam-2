package com.example.edt02;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final long SPLASH_SCREEN = 2000;
    private ImageView image1;
    private ImageView image2;
    private TextView text1;

    private ObjectAnimator objectAnimator;
    private ObjectAnimator objectAnimator2;
    private ObjectAnimator objectAnimator3;
    private ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        text1 = findViewById(R.id.text1);


        // Animacions



        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashBoard.class);
                Pair[] pairs = new Pair[3];
                pairs[0] = new Pair(image1, "image1transition");
                pairs[1] = new Pair(image2, "image2transition");
                pairs[2] = new Pair(text1, "texttransition");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);

                startActivity(intent, options.toBundle());
                finish();
            }
        };

        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, SPLASH_SCREEN);
/*
        objectAnimator = ObjectAnimator.ofFloat(image1, "x", 50f, 1500f);
        objectAnimator.setDuration(1500);
        objectAnimator.setStartDelay(300);

        objectAnimator2 = ObjectAnimator.ofFloat(text1, "y", 1155f, 3000f);
        objectAnimator2.setDuration(1500);
        objectAnimator2.setStartDelay(300);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

*/

    }
}