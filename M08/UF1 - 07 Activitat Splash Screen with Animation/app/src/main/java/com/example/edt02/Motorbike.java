package com.example.edt02;

public class Motorbike {
    private String modelName;
    private int img;

    public Motorbike(String modelName, int img) {
        this.modelName = modelName;
        this.img = img;
    }

    public String getModelName() {
        return modelName;
    }

    public int getImg() {
        return img;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
