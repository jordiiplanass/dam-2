package com.example.edt02;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MotorbikeAdapter extends ArrayAdapter<Motorbike>{
    public MotorbikeAdapter(Context context, ArrayList<Motorbike> motorbikeArrayList) {
        super(context, 0, motorbikeArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    public View init(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row, parent, false
            );
        }
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView textView = convertView.findViewById(R.id.textView2);
        Motorbike currentItem = getItem(position);
        if (currentItem != null) {
            imageView.setImageResource(currentItem.getImg());
            textView.setText(currentItem.getModelName());
        }
        return convertView;

    }
}
