package com.example.myapplication;

public interface Constants {
    double relativeFatMass(int altura, int circum, GENDER gender);
    double wanDerVael(int altura, GENDER gender);
    double lorentz(int altura, int edat, GENDER gender);
    double creff(int altura, int edat, MORPHOLOGY morphology);
}
