package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity implements Constants{

    private Button button2;

    private RadioGroup radioGroupStart;
    private RadioGroup radioGroupGender;
    private RadioGroup radioGroupMorphology;

    private RadioButton radioButton;
    private RadioButton radioButton2;
    private RadioButton radioButton3;
    private RadioButton radioButton4;
    private RadioButton radioButton5;
    private RadioButton radioButton6;
    private RadioButton radioButton7;
    private RadioButton radioButton8;
    private RadioButton radioButton9;

    private TextInputLayout input1;
    private TextInputLayout input2;

    private TextInputEditText input1text;
    private TextInputEditText input2text;
    private View tablePes;
    private TextView textResult;

    // Default
    public GENDER gender = GENDER.MALE;
    public MORPHOLOGY morphology = MORPHOLOGY.SMALL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // hook
        radioGroupStart = findViewById(R.id.radioGroupStart);
        radioGroupGender = findViewById(R.id.radioGroupGender);
        radioGroupMorphology = findViewById(R.id.radioGroupMorphology);

        radioButton = findViewById(R.id.radioButton);
        radioButton2 = findViewById(R.id.radioButton2);
        radioButton3 = findViewById(R.id.radioButton3);
        radioButton4 = findViewById(R.id.radioButton4);
        radioButton5 = findViewById(R.id.radioButton5);
        radioButton6 = findViewById(R.id.radioButton6);
        radioButton7 = findViewById(R.id.radioButton7);
        radioButton8 = findViewById(R.id.radioButton8);
        radioButton9 = findViewById(R.id.radioButton9);

        tablePes = findViewById(R.id.tablePes);

        input1 = findViewById(R.id.input1);
        input2 = findViewById(R.id.input2);
        input1text = findViewById(R.id.input1text);
        input2text = findViewById(R.id.input2text);

        button2 = findViewById(R.id.button2);
        tablePes = findViewById(R.id.tablePes);
        textResult = findViewById(R.id.textResult);

        radioGroupStart.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int radioSeleccted = radioGroupStart.getCheckedRadioButtonId();
                switch (radioSeleccted){
                    case R.id.radioButton:

                        // default picks
                        radioButton5.setChecked(true);
                        // Show & Hide
                        input1.setAlpha(1f);
                        button2.setAlpha(1f);
                        button2.setClickable(true);
                        tablePes.setAlpha(0);
                        hideRadioGroup(radioGroupMorphology);
                        showRadioGroup(radioGroupGender);
                        input2.setAlpha(1f);
                        input2.setHint(R.string.input3);
                        break;
                    case R.id.radioButton2:

                        // default picks
                        radioButton5.setChecked(true);
                        // Show & Hide
                        input1.setAlpha(1f);
                        button2.setAlpha(1f);
                        button2.setClickable(true);
                        tablePes.setAlpha(0);
                        hideRadioGroup(radioGroupMorphology);
                        showRadioGroup(radioGroupGender);
                        input2.setAlpha(0f);
                        break;
                    case R.id.radioButton3:

                        // default picks
                        radioButton5.setChecked(true);
                        // Show & Hide
                        input1.setAlpha(1f);
                        button2.setAlpha(1f);
                        button2.setClickable(true);
                        tablePes.setAlpha(0);
                        hideRadioGroup(radioGroupMorphology);
                        showRadioGroup(radioGroupGender);
                        input2.setAlpha(1f);
                        input2.setHint(R.string.input2);
                        break;
                    case R.id.radioButton4:

                        // default picks
                        radioButton7.setChecked(true);
                        // Show & Hide
                        input1.setAlpha(1f);
                        button2.setAlpha(1f);
                        button2.setClickable(true);
                        tablePes.setAlpha(0);
                        hideRadioGroup(radioGroupGender);
                        showRadioGroup(radioGroupMorphology);
                        input2.setAlpha(1f);
                        input2.setHint(R.string.input2);
                        break;

                }
            }
        });
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int radioSeleccted = radioGroupStart.getCheckedRadioButtonId();
                switch (radioSeleccted){
                    case R.id.radioButton5:
                        gender = GENDER.MALE;
                        break;
                    case R.id.radioButton6:
                        gender = GENDER.FEMALE;
                        break;
                }
            }
        });
        radioGroupMorphology.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int radioSeleccted = radioGroupStart.getCheckedRadioButtonId();
                switch (radioSeleccted){
                    case R.id.radioButton7:
                        morphology = MORPHOLOGY.SMALL;
                        break;
                    case R.id.radioButton8:
                        morphology = MORPHOLOGY.MEDIUM;
                        break;
                    case R.id.radioButton9:
                        morphology = MORPHOLOGY.BROAD;
                        break;
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioSeleccted = radioGroupStart.getCheckedRadioButtonId();
                switch (radioSeleccted) {
                    case R.id.radioButton2:
                        if (input1text.getText().length() > 0) {
                            if (TextUtils.isDigitsOnly(input1text.getText())) {
                                int altura = Integer.parseInt(String.valueOf(input1text.getText()));
                                double calcul = wanDerVael(altura, gender);
                                String sortida = insertUnit(String.valueOf(calcul), "Kg");
                                textResult.setText(sortida);
                            } else {
                                Toast.makeText(MainActivity.this, "Introdueix xifres", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Introdueix Informacio", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.radioButton:
                        if (input1text.getText().length() > 0 && input2text.getText().length() > 0){
                            if (TextUtils.isDigitsOnly(input1text.getText()) && TextUtils.isDigitsOnly(input2text.getText())
                            ) {
                                int altura = Integer.parseInt(String.valueOf(input1text.getText()));
                                int circum = Integer.parseInt(String.valueOf(input2text.getText()));
                                double calcul = relativeFatMass(altura, circum, gender);
                                String sortida = insertUnit(String.valueOf(calcul), "%");
                                tablePes.setAlpha(1);
                                textResult.setText(sortida);
                            } else {
                                Toast.makeText(MainActivity.this, "Introdueix xifres", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Introdueix Informacio", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.radioButton3:
                        if (input1text.getText().length() > 0 && input2text.getText().length() > 0) {
                            if (TextUtils.isDigitsOnly(input1text.getText()) && TextUtils.isDigitsOnly(input2text.getText())) {
                                int altura = Integer.parseInt(String.valueOf(input1text.getText()));
                                int edat = Integer.parseInt(String.valueOf(input2text.getText()));
                                double calcul = lorentz(altura, edat, gender);
                                String sortida = insertUnit(String.valueOf(calcul), "Kg");
                                textResult.setText(sortida);
                            } else {
                                Toast.makeText(MainActivity.this, "Introdueix xifres", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Introdueix Informacio", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.radioButton4:
                        if (input1text.getText().length() > 0 && input2text.getText().length() > 0) {
                            if (TextUtils.isDigitsOnly(input1text.getText()) && TextUtils.isDigitsOnly(input2text.getText())) {
                                int altura = Integer.parseInt(String.valueOf(input1text.getText()));
                                int edat = Integer.parseInt(String.valueOf(input2text.getText()));
                                double calcul = creff(altura, edat, morphology);
                                String sortida = insertUnit(String.valueOf(calcul), "Kg");
                                textResult.setText(sortida);
                            } else {
                                Toast.makeText(MainActivity.this, "Introdueix xifres", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Introdueix Informacio", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }


            }
        });
    }
    public String insertUnit(String a, String unit){
        return a + " " + unit;
    }

    public void hideRadioGroup(RadioGroup radioGroup){
        radioGroup.setAlpha(0f);
        for (int j = 0; j < radioGroup.getChildCount(); j++) {
            radioGroup.getChildAt(j).setClickable(false);
            radioGroup.getChildAt(j).setEnabled(false);
        }
    }
    public void showRadioGroup(RadioGroup radioGroup){
        radioGroup.setAlpha(1f);
        for (int j = 0; j < radioGroup.getChildCount(); j++) {
            radioGroup.getChildAt(j).setClickable(true);
            radioGroup.getChildAt(j).setEnabled(true);
        }
    }

    public double round(double a){
        double b = a*100;
        double c = Math.round(b);
        return c/100;
    }

    @Override
    public double relativeFatMass(int altura, int circum, GENDER gender) {
        int n = 0;
        switch(gender){
            case MALE:
                n = 64;
                break;
            case FEMALE:
                n = 76;
                break;
        }
        double result = n - (20*altura)/circum;
        return round(result);
    }

    @Override
    public double wanDerVael(int altura, GENDER gender) {
        double m = 0;
        switch(gender){
            case MALE:
                m = 0.75;
                break;
            case FEMALE:
                m = 0.6;
                break;
        }
        // (A−150)M+50
        double resultat = (altura-150)*m+50;
        return round(resultat);
    }

    @Override
    public double lorentz(int altura, int edat, GENDER gender) {
        double k = 0;
        switch(gender){
            case MALE:
                k = 0.75;
                break;
            case FEMALE:
                k = 0.6;
                break;
        }
        // (A−150)M+50
        double resultat = altura-100-(altura-150)/4+(edat-20)/k;
        return round(resultat);
    }

    @Override
    public double creff(int altura, int edat, MORPHOLOGY morphology) {
        double resultat = (altura - 100 + (edat / 10));
        switch(morphology){
            case SMALL:
                resultat *= Math.pow(0.9, 2);
                break;
            case MEDIUM:
                resultat *= 0.9;
                break;
            case BROAD:
                resultat *= 0.9 * 1.1;
                break;
        }
        return round(resultat);
    }
}