package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Grid_Class extends AppCompatActivity {

    ArrayList<Item> list = new ArrayList<>();
    private GridView gridView;
    Grid_Class.CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_class);
        //hook
        gridView = findViewById(R.id.gridView);

        Item item = new Item(R.drawable.drake, getString(R.string.drake1), getString(R.string.subtitle1), getString(R.string.race1));
        Item item2 = new Item(R.drawable.drake2, getString(R.string.drake2), getString(R.string.subtitle2), getString(R.string.race2));
        Item item3 = new Item(R.drawable.drake3, getString(R.string.drake3), getString(R.string.subtitle3), getString(R.string.race3));
        Item item4 = new Item(R.drawable.drake4, getString(R.string.drake4), getString(R.string.subtitle4), getString(R.string.race4));
        Item item5 = new Item(R.drawable.drake5, getString(R.string.drake5), getString(R.string.subtitle5), getString(R.string.race5));

        list.add(item);
        list.add(item2);
        list.add(item3);
        list.add(item4);
        list.add(item5);
        customAdapter = new Grid_Class.CustomAdapter(this, list);
        gridView.setAdapter(customAdapter);
    }
    private class CustomAdapter extends BaseAdapter {
        private Context context;
        private List<Item> items;

        public CustomAdapter(Context context, ArrayList<Item> items){
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data2, null);
            TextView textRow = view.findViewById(R.id.title);
            TextView textRow2 = view.findViewById(R.id.subtitle);
            TextView textRow3 = view.findViewById(R.id.race);
            ImageView imageRow = view.findViewById(R.id.imageRow);
            textRow.setText(items.get(position).getTitle());
            textRow2.setText(items.get(position).getSubtitle());
            textRow3.setText(items.get(position).getRace());
            imageRow.setImageResource(items.get(position).getImage());
            return view;
        }
    }
}