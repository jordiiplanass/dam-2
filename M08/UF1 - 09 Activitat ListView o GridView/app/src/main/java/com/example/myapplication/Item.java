package com.example.myapplication;

public class Item {
    private int image;
    private String title;
    private String subtitle;
    private String race;

    public void setRace(String race) {
        this.race = race;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getRace() {
        return race;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public Item(int image, String title, String subtitle, String race) {
        this.image = image;
        this.title = title;
        this.subtitle = subtitle;
        this.race = race;
    }
}
