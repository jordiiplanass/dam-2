package com.example.carrent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DashBoard extends AppCompatActivity {
    public static String EXTRA_CAR_NAME = "com.example.carrent.EXTRA_CAR_NAME";
    public static String EXTRA_CAR_IMAGE = "com.example.carrent.EXTRA_CAR_IMAGE";
    public static String EXTRA_CAR_PERSON = "com.example.carrent.EXTRA_CAR_PERSON";
    public static String EXTRA_CAR_STORAGE = "com.example.carrent.EXTRA_CAR_STORAGE";
    public static String EXTRA_CAR_DOOR = "com.example.carrent.EXTRA_CAR_DOOR";
    public static String EXTRA_CAR_ENGINE = "com.example.carrent.EXTRA_CAR_ENGINE";
    public static String EXTRA_CAR_FUEL = "com.example.carrent.EXTRA_CAR_FUEL";
    public static String EXTRA_CAR_PRICE = "com.example.carrent.EXTRA_CAR_PRICE";
    private ImageView image1;
    private ImageView image2;
    private TextView text1;
    ArrayList<Item> items = new ArrayList<>();

    private GridView gridView;
    Item car1 = new Item("Ford Ka", R.drawable.car1, "4", "2", "4", "Manual", "Gasolina", "4");
    Item car2 = new Item("Ford C-MAX", R.drawable.car2, "5", "4", "4", "Manual", "Diesel", "9");
    Item car3 = new Item("Nissan Qashqai", R.drawable.car3, "5", "2", "5", "Manual", "Gasolina", "17");
    Item car4 = new Item("Seat Ibiza", R.drawable.car4, "5", "1", "4", "Automatico", "Gasolina", "24");
    Item car5 = new Item("Ford Focus", R.drawable.car5, "5", "2", "4", "Automatico", "Gasolina", "26");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        items.add(car1);
        items.add(car2);
        items.add(car3);
        items.add(car4);
        items.add(car5);
        gridView = findViewById(R.id.gridView);

        CustomAdapter customAdapter = new CustomAdapter(this, items);
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
//     public Item(String name, int image, int person, int storage, int door, String engine, String fuel) {
    }
    private void displayInfo(int position) {


        Intent i = new Intent(getApplicationContext(), DetailActivity.class);
        i.putExtra(EXTRA_CAR_NAME,items.get(position).getName());
        i.putExtra(EXTRA_CAR_IMAGE,items.get(position).getImage());
        i.putExtra(EXTRA_CAR_STORAGE,items.get(position).getStorage());
        i.putExtra(EXTRA_CAR_DOOR,items.get(position).getDoor());
        i.putExtra(EXTRA_CAR_ENGINE, items.get(position).getEngine());
        i.putExtra(EXTRA_CAR_FUEL, items.get(position).getFuel());
        i.putExtra(EXTRA_CAR_PRICE, items.get(position).getPrice());
        startActivity(i);}
    public class CustomAdapter extends BaseAdapter {
        final Context context;
        private List<Item> items;
        public CustomAdapter(Context context, ArrayList<Item> items){
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.activity_row_cars, null);
            TextView textRow = view.findViewById(R.id.carName);
            TextView textRow2 = view.findViewById(R.id.carPrice);
            TextView textRow3 = view.findViewById(R.id.carPerson);
            TextView textRow4 = view.findViewById(R.id.carStorage);
            TextView textRow5 = view.findViewById(R.id.carDoor);
            TextView textRow6 = view.findViewById(R.id.carEngine);
            TextView textRow7 = view.findViewById(R.id.carFuel);
            ImageView imageRow = view.findViewById(R.id.imageView);
            textRow.setText(items.get(position).getName());
            textRow2.setText(items.get(position).getPrice());
            textRow3.setText(items.get(position).getPerson());
            textRow4.setText(items.get(position).getStorage());
            textRow5.setText(items.get(position).getDoor());
            textRow6.setText(items.get(position).getEngine());
            textRow7.setText(items.get(position).getFuel());
            imageRow.setImageResource(items.get(position).getImage());
            return view;
        }
    }
}