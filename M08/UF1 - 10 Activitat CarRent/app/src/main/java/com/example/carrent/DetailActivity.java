package com.example.carrent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private ImageView imageView6;
    private Spinner spinner;
    private TextInputEditText box1_text;
    private TextInputEditText box2_text;
    private TextInputEditText box3_text;
    private Switch switch1;
    private EditText editTextDate;
    private EditText editTextDate2;
    private Button buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hook
        imageView6 = findViewById(R.id.imageView6);
        spinner = findViewById(R.id.spinner);
        box1_text = findViewById(R.id.box1_text);
        box2_text = findViewById(R.id.box2_text);
        box3_text = findViewById(R.id.box3_text);
        switch1 = findViewById(R.id.switch1);
        editTextDate = findViewById(R.id.editTextDate);
        editTextDate2 = findViewById(R.id.editTextDate2);
        buttonSave = findViewById(R.id.buttonSave);

        List<String> cotxes = new ArrayList<>();

        readData();



    }

    private void readData() {

    }

}