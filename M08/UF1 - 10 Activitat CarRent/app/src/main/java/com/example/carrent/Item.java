package com.example.carrent;

public class Item {
    private String name;
    private int image;
    private String person;
    private String storage;
    private String door;
    private String engine;
    private String fuel;
    private String price;

    public Item(String name, int image, String person, String storage, String door, String engine, String fuel, String price) {
        this.name = name;
        this.image = image;
        this.person = person;
        this.storage = storage;
        this.door = door;
        this.engine = engine;
        this.fuel = fuel;
        this.price = price;
    }


    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    public String getPerson() {
        return person;
    }

    public String getStorage() {
        return storage;
    }

    public String getDoor() {
        return door;
    }

    public String getEngine() {
        return engine;
    }

    public String getFuel() {
        return fuel;
    }
}
