package com.example.carrent;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView image1;
    private ImageView image2;
    private ImageView image3;
    private TextView text1;
    private static final long SPLASH_SCREEN = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image1 = findViewById(R.id.imageCar1);
        image2 = findViewById(R.id.imageCar2);
        image3 = findViewById(R.id.imageCircle);
        text1 = findViewById(R.id.textView);

        image1.setAlpha(0f);
        image2.setX(-1000f);
        image3.setAlpha(0f);
        text1.setAlpha(0f);

        image1.animate().alpha(1f).setDuration(1000).setStartDelay(1000);
        image2.animate().translationX(0f).setDuration(900).setStartDelay(100);
        image3.animate().alpha(1f).setDuration(800).setStartDelay(2000);
        text1.animate().alpha(1f).setDuration(800).setStartDelay(2000);


        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashBoard.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair(image1, "imagetransition");
                pairs[1] = new Pair(text1, "texttransition");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);

                startActivity(intent, options.toBundle());
                finish();
            }
        };

        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, SPLASH_SCREEN);
    }
}