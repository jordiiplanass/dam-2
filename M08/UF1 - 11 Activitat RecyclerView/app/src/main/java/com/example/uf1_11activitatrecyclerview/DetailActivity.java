package com.example.uf1_11activitatrecyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView textDetailed;
    private TextView textDetailed2;
    private ImageView imageDetailed;

    String title, description, imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textDetailed = findViewById(R.id.textDetailed);
        textDetailed2 = findViewById(R.id.textDetailed2);
        imageDetailed = findViewById(R.id.imageDetailed);
        getData();
        setData();
    }
    private void getData(){
        if (getIntent().hasExtra("text") &&
                getIntent().hasExtra("url")){
            title = getIntent().getStringExtra("text");
            description = getIntent().getStringExtra("desc");
            imageUrl = getIntent().getStringExtra("url");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }
    private void setData(){
        textDetailed.setText(title);
        textDetailed2.setText(description);
        Picasso.get().load(imageUrl)
                .fit()
                .centerCrop()
                .into(imageDetailed);


    }
}