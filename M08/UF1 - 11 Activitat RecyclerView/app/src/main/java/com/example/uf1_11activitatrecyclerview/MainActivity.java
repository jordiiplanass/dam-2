package com.example.uf1_11activitatrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    ArrayList<Picture> pictures = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        initData();
        MyAdapter myAdapter = new MyAdapter(this, pictures);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2,
                RecyclerView.VERTICAL, false));

    }
    private void initData(){
        Picture picture1 = new Picture(
                "Ciclista",
                "https://images.unsplash.com/photo-1638469834848-e4b859f23918?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "Jan Kopřiva"
        );
        Picture picture2 = new Picture(
                "Gos Unicornio",
                "https://images.unsplash.com/photo-1638464830368-9e8938150ecc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1112&q=80",
                "BRUNO EMMANUELLE"
        );
        Picture picture3 = new Picture(
                "Gatet llums",
                "https://images.unsplash.com/photo-1638497812440-a012db923800?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "Max Ovcharenko"
        );
        Picture picture4 = new Picture(
                "Pizza",
                "https://images.unsplash.com/photo-1638425793674-32119fffb3d5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "amirhosein darbzanjiri"
        );
        Picture picture5 = new Picture(
                "Cotxe",
                "https://images.unsplash.com/photo-1638398414730-909f9eddd76d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "Tyrel Johnson"
        );
        Picture picture6 = new Picture(
                "Zebra",
                "https://images.unsplash.com/photo-1638292596998-8b1f2f28c386?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "Tomáš Malík"
        );
        Picture picture7 = new Picture(
                "Pedra",
                "https://images.unsplash.com/photo-1638365448598-8fb987e93be3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1113&q=80",
                "Mads Eneqvist"
        );
        Picture picture8 = new Picture(
                "Skater",
                "https://images.unsplash.com/photo-1630483264555-a6d025ed6a0f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "Eugene Chystiakov"
        );
        Picture picture9 = new Picture(
                "Pomes",
                "https://images.unsplash.com/photo-1634376740156-eb1ebaf79d58?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
                "Ethan Medrano"
        );
        Picture picture10 = new Picture(
                "Gimnasta Fort",
                "https://images.unsplash.com/photo-1627197843575-00cc3965c2d5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                "mob alizadeh"
        );
        pictures.add(picture1);
        pictures.add(picture2);
        pictures.add(picture3);
        pictures.add(picture4);
        pictures.add(picture5);
        pictures.add(picture6);
        pictures.add(picture7);
        pictures.add(picture8);
        pictures.add(picture9);
        pictures.add(picture10);


    }
}