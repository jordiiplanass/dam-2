package com.example.floatingmenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ActivityGridView2 extends AppCompatActivity {

    public static String EXTRA_TEXT = "com.example.startup.EXTRA_TEXT";

    private GridView gridView;
    private ArrayList<String> llistat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view2);
        hook();
        llistat = new ArrayList<>();
        initdata();
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                llistat
        );
        gridView.setAdapter(arrayAdapter);
        registerForContextMenu(gridView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.my_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.delete:
                ArrayAdapter adapter = (ArrayAdapter) gridView.getAdapter();
                adapter.remove(adapter.getItem(info.position));
                gridView.setAdapter(adapter);
                return true;
            case R.id.share:
                Toast.makeText(this, "~~~~~~~", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    private void initdata() {
        llistat.add("Abella");
        llistat.add("Arbre");
        llistat.add("Gla");
        llistat.add("Lluna");
    }

    private void hook() {
        gridView = findViewById(R.id.gridView1);
    }


}