package com.example.floatingmenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityGridView4 extends AppCompatActivity {

    private GridView gridView;
    private ArrayList<Objecte> llistat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view4);

        hook();
        llistat = new ArrayList<>();
        initdata();
        ActivityGridView4.CustomAdapter customAdapter = new ActivityGridView4.CustomAdapter(this, llistat);
        gridView.setAdapter(customAdapter);
        registerForContextMenu(gridView);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.my_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete:
                Toast.makeText(this, "No se com esborrar en un customAdapter :(", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.share:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void hook() {
        //hook
        gridView = findViewById(R.id.gridView);
    }

    private void initdata() {
        llistat.add(new Objecte(R.drawable.abeja, R.drawable.real_bee, "Abella", "Abeja", "a"));
        llistat.add(new Objecte(R.drawable.arbol, R.drawable.real_tree, "Arbre", "Arbol", "a"));
        llistat.add(new Objecte(R.drawable.bellota, R.drawable.real_bellota ,"Gla", "Bellota", "a"));
        llistat.add(new Objecte(R.drawable.luna, R.drawable.real_moon, "Lluna", "Luna", "a"));
    }

    private class CustomAdapter extends BaseAdapter {

        private Context context;
        private List<Objecte> llistatObjectes;

        public CustomAdapter(Context context, List<Objecte> llistatObjectes) {
            this.context = context;
            this.llistatObjectes = llistatObjectes;
        }

        @Override
        public int getCount() {
            return llistatObjectes.size();
        }

        @Override
        public Object getItem(int i) {
            return llistatObjectes.get(i);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.row_data2, null);

            TextView textView = view.findViewById(R.id.textView);
            TextView textView2 = view.findViewById(R.id.textView2);
            ImageView imageView = view.findViewById(R.id.imageView);

            textView.setText(llistatObjectes.get(position).getText1());
            textView2.setText(llistatObjectes.get(position).getText2());
            imageView.setImageResource(llistatObjectes.get(position).getImage1());

            return view;
        }
    }
}