package com.example.floatingmenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityListView5 extends AppCompatActivity {

    public static String EXTRA_TEXT1 = "com.example.startup.EXTRA_TEXT1";
    public static String EXTRA_TEXT2 = "com.example.startup.EXTRA_TEXT2";
    public static String EXTRA_IMAGE = "com.example.startup.EXTRA_IMAGE";
    public static String EXTRA_IMAGE2 = "com.example.startup.EXTRA_IMAGE2";
    public static String EXTRA_DESC = "com.example.startup.EXTRA_DESC";

    private ListView listView;
    private ArrayList<Objecte> llistat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view5);

        hook();
        llistat = new ArrayList<>();
        initdata();
        CustomAdapter customAdapter = new CustomAdapter(this, llistat);
        listView.setAdapter(customAdapter);
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.my_menu2, menu);
    }

    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.delete:
                Toast.makeText(this, "No se com esborrar en un customAdapter :(", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.share:
                Toast.makeText(this, "~~~~~~~", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.info:
                Intent intent = new Intent(this, detailActivity.class);
                intent.putExtra(EXTRA_TEXT1, llistat.get(info.position).getText1());
                intent.putExtra(EXTRA_TEXT2, llistat.get(info.position).getText2());
                intent.putExtra(EXTRA_IMAGE, llistat.get(info.position).getImage1());
                intent.putExtra(EXTRA_IMAGE2, llistat.get(info.position).getImage2());
                intent.putExtra(EXTRA_DESC, llistat.get(info.position).getDescription());
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    private void initdata() {
        llistat.add(new Objecte(R.drawable.abeja, R.drawable.real_bee, "Abella", "Abeja", "L'abella de la mel (també borinot o abellot), abella europea o abella occidental (Apis mellifera) és una espècie d'himenòpter apòcrit de la família dels àpids. És l'abella que s'utilitza preferentment (però no exclusivament) en apicultura en tot el món."));
        llistat.add(new Objecte(R.drawable.arbol, R.drawable.real_tree, "Arbre", "Arbol", "Un arbre (del llatí arbor) és una planta capaç de créixer a més de cinc metres d'alçària gràcies a una estructura que forma un tronc, i eventualment ramificacions anomenades branques."));
        llistat.add(new Objecte(R.drawable.bellota, R.drawable.real_bellota, "Gla", "Bellota", "La gla (f.), aglà (m. o f.) (del llatí glans id.) o bellota (f., de l'àrab ballūta, alzina) és un fruit característic de les plantes del gènere Quercus (família Fagaceae)."));
        llistat.add(new Objecte(R.drawable.luna, R.drawable.real_moon, "Lluna", "Luna", "La Lluna és l'únic satèl·lit natural de la Terra. Encara que no és el satèl·lit més gros del sistema solar, és el més gros en proporció a la mida del cos que orbita (el seu primari) i, després del satèl·lit jovià Io, és el segon satèl·lit més dens dels satèl·lits dels quals se'n coneix la densitat."));
    }

    private void hook() {
        //hook
        listView = findViewById(R.id.listView);
    }
    private class CustomAdapter extends BaseAdapter {

        private Context context;
        private List<Objecte> llistatObjectes;

        public CustomAdapter(Context context, List<Objecte> llistatObjectes) {
            this.context = context;
            this.llistatObjectes = llistatObjectes;
        }

        @Override
        public int getCount() {
            return llistatObjectes.size();
        }

        @Override
        public Object getItem(int i) {
            return llistatObjectes.get(i);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.row_data, null);

            TextView textView = view.findViewById(R.id.textView);
            TextView textView2 = view.findViewById(R.id.textView2);
            ImageView imageView = view.findViewById(R.id.imageView);

            textView.setText(llistatObjectes.get(position).getText1());
            textView2.setText(llistatObjectes.get(position).getText2());
            imageView.setImageResource(llistatObjectes.get(position).getImage1());

            return view;
        }


    }
}