package com.example.floatingmenu;

public class Objecte {
    private int image1;
    private int image2;
    private String text1;
    private String text2;
    private String description;

    public Objecte(int imageName, int image2, String text1, String text2, String description) {
        this.image1 = imageName;
        this.image2 = image2;
        this.text1 = text1;
        this.text2 = text2;
        this.description = description;
    }

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage2() {
        return image2;
    }

    public void setImage2(int image2) {
        this.image2 = image2;
    }
}
