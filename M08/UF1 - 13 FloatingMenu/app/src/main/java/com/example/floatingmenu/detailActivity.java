package com.example.floatingmenu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class detailActivity extends AppCompatActivity {


    private TextView detailText1;
    private TextView detailText2;
    private ImageView detailImage1;
    private ImageView detailImage2;
    private TextView detailDesc;
    private String text1;
    private String text2;
    private String descripcio;
    private int image1;
    private int image2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        hook();
        getInfo();
        setInfo();


    }

    private void hook() {
        detailText1 = findViewById(R.id.detailText1);
        detailText2 = findViewById(R.id.detailText2);
        detailImage1 = findViewById(R.id.detailImage1);
        detailImage2 = findViewById(R.id.detailImage2);
        detailDesc = findViewById(R.id.detailDesc);
    }

    private void setInfo() {
        detailText1.setText(text1);
        detailText2.setText(text2);
        detailImage1.setImageResource(image1);
        detailImage2.setImageResource(image2);
        detailDesc.setText(descripcio);
    }

    private void getInfo() {
        Intent intent = getIntent();
        text1 = intent.getStringExtra(ActivityListView5.EXTRA_TEXT1);
        text2 = intent.getStringExtra(ActivityListView5.EXTRA_TEXT2);
        descripcio = intent.getStringExtra(ActivityListView5.EXTRA_DESC);
        image1 = intent.getIntExtra(ActivityListView5.EXTRA_IMAGE, 1);
        image2 = intent.getIntExtra(ActivityListView5.EXTRA_IMAGE2, 1);
    }
}