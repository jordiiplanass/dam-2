package com.example.uf1_bottomnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private FrameLayout frameLayout;
    private BottomNavigationView bottomView;
    private fragment_home fragment_home;
    private fragment_user fragment_user;
    private fragment_diving fragment_diving;
    private ArrayList<Fotito> fotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hook();
        fragment_home = fragment_home.newInstance("a", "b");
        fragment_user = fragment_user.newInstance("a", "b");
        fragment_diving = fragment_diving.newInstance("a", "b");

        setFragment(fragment_home);

        bottomView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.main:
                        setFragment(fragment_home);
                        return true;
                    case R.id.user:
                        setFragment(fragment_user);
                        return true;
                    case R.id.diving:
                        setFragment(fragment_diving);
                        return true;
                    default:
                        setFragment(fragment_home);
                        return false;
                }
            }
        });

    }


    private void hook() {
        bottomView = findViewById(R.id.bottomView);
        frameLayout = findViewById(R.id.frameLayout);
    }


    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }
}