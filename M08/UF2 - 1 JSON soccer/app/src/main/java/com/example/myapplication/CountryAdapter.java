package com.example.myapplication;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CountryAdapter extends ArrayAdapter<Country> {
    public CountryAdapter(@NonNull Context context, ArrayList<Country> countryArrayLIst) {
        super(context, 0, countryArrayLIst);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row, parent, false
            );
        }
        ImageView countryFlag = convertView.findViewById(R.id.countryFlag);
        TextView countryName = convertView.findViewById(R.id.countryName);
        Country currentItem = getItem(position);
        if (currentItem != null) {
            Picasso.get().load(currentItem.getImage())
                    .centerCrop()
                    .fit()
                    .into(countryFlag);
            countryName.setText(currentItem.getName());
        }
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup
            parent) {
        return initView(position, convertView, parent);
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull
            ViewGroup parent) {
        return initView(position, convertView, parent);
    }
}
