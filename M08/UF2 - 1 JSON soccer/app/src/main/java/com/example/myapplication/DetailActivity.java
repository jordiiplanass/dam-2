package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class DetailActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<Peak> peaks = new ArrayList<>();
    private PeakAdapter peakAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        hook();
        getData();
    }

    private void getData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                getCountryURL(),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("countrys");
                            for (int i = 0 ; i < jsonArray.length() ; i++ ){
                                try{
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Peak peak = new Peak();
                                    peak.strBadge = jsonObject.getString("strBadge");
                                    peak.strDescriptionEN = jsonObject.getString("strDescriptionEN");
                                    peak.strLeague = jsonObject.getString("strLeague");
                                    peak.strWebsite = jsonObject.getString("strWebsite");
                                    peak.strFanart1 = jsonObject.getString("strFanart1");
                                    peak.strFanart2 = jsonObject.getString("strFanart2");
                                    peak.strFanart3 = jsonObject.getString("strFanart3");
                                    peak.strFanart4 = jsonObject.getString("strFanart4");
                                    peaks.add(peak);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        peakAdapter = new PeakAdapter(getApplicationContext(), peaks);
                        recyclerView.setAdapter(peakAdapter);
                    }
                },
            new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error",error.toString());

            }
        }
        );
        queue.add(jsonObjectRequest);
    }

    private String getCountryURL() {
        String countryName = getIntent().getStringExtra(MainActivity.COUNTRY_NAME);
        return "https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=" + countryName + "&s=Soccer";
    }

    private void hook() {
        recyclerView = findViewById(R.id.recyclerView);
    }
}