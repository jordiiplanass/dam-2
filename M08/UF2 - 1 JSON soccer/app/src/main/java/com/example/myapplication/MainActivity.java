package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static String COUNTRY_NAME = "com.example.myapplication.COUNTRY_NAME";

    private Spinner mySpinner;
    private CountryAdapter adapter;
    ArrayList<Country> countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hooks();
        initData();

        adapter = new CountryAdapter(this, countries);
        mySpinner.setAdapter(adapter);

        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Country itemSelected = (Country) adapterView.getItemAtPosition(i);
                String pais = itemSelected.getName();
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(COUNTRY_NAME, pais);
                startActivity(intent);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d("Error","No flag selected");
            }
        });
    }

    private void initData() {
        countries = new ArrayList<>();
        countries.add(new Country("Denmark", "https://cdn.countryflags.com/thumbs/denmark/flag-800.png"));
        countries.add(new Country("Italy", "https://cdn.countryflags.com/thumbs/italy/flag-800.png"));
        countries.add(new Country("Spain", "https://cdn.countryflags.com/thumbs/spain/flag-800.png"));
    }

    private void hooks() {
        mySpinner = findViewById(R.id.mySpinner);
    }
}