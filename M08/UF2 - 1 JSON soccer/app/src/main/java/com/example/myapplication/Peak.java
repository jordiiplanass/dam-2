package com.example.myapplication;

public class Peak {
    public String strBadge;
    public String strLeague;
    public String strDescriptionEN;
    public String strWebsite;
    public String strFanart1;
    public String strFanart2;
    public String strFanart3;
    public String strFanart4;

    public Peak( ) {
    }

    public Peak(String strBadge, String strLeague, String strDescriptionEN, String strWebsite, String strFanart1, String strFanart2, String strFanart3, String strFanart4) {
        this.strBadge = strBadge;
        this.strLeague = strLeague;
        this.strDescriptionEN = strDescriptionEN;
        this.strWebsite = strWebsite;
        this.strFanart1 = strFanart1;
        this.strFanart2 = strFanart2;
        this.strFanart3 = strFanart3;
        this.strFanart4 = strFanart4;
    }
}
