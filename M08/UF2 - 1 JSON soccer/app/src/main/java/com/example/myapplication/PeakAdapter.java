package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class PeakAdapter extends RecyclerView.Adapter<PeakAdapter.MyViewHolder> {
    Context context;
    List<Peak> peaks;
    public PeakAdapter(Context context, List<Peak> peaks) {
        this.context = context;
        this.peaks = peaks;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.peak_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(peaks.get(position).strBadge)
            .fit()
            .centerCrop()
            .into(holder.flag);
        holder.title.setText(peaks.get(position).strLeague);
        holder.description.setText(peaks.get(position).strDescriptionEN);
    }

    @Override
    public int getItemCount() {
        return peaks.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView flag;
        private TextView title;
        private TextView description;
        private Button buttonWeb;
        private Button buttonImages;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            flag = itemView.findViewById(R.id.flag);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            buttonWeb = itemView.findViewById(R.id.buttonWeb);
            buttonImages = itemView.findViewById(R.id.buttonImages);
        }
    }
}
