package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class LoginActivity extends AppCompatActivity {
    private Button logInButton;
    private Button registerButton;
    private LinearLayout linearLayout;
    private LogInFragment logInFragment;
    private RegisterFragment registerFragment;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hook();
        logInFragment = new LogInFragment();
        registerFragment = new RegisterFragment();
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linearLayout.setAlpha(0f);
                logInButton.setEnabled(false);
                logInButton.setClickable(false);
                registerButton.setEnabled(false);
                registerButton.setClickable(false);
                setFragment(registerFragment, "register");
            }
        });
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.setAlpha(0f);
                logInButton.setEnabled(false);
                logInButton.setClickable(false);
                registerButton.setEnabled(false);
                registerButton.setClickable(false);
                setFragment(logInFragment, "login");
            }
        });
    }

    private void hook() {
        linearLayout = findViewById(R.id.linearLayout);
        logInButton = findViewById(R.id.logInButton);
        registerButton = findViewById(R.id.registerButton);
    }

    private void setFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }





}