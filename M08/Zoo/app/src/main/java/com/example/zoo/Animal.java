package com.example.zoo;

public class Animal {
    private String urlImage;
    private String type;
    private String nickname;
    private int age;
    private int birthYear;
    private String[] meals;
    private String desc;

    public Animal(String urlImage, String type, String nickname, int age, int birthYear, String[] meals, String desc) {
        this.urlImage = urlImage;
        this.type = type;
        this.nickname = nickname;
        this.age = age;
        this.birthYear = birthYear;
        this.meals = meals;
        this.desc = desc;
    }
    // getter i setter
    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String[] getMeals() {
        return meals;
    }

    public void setMeals(String[] meals) {
        this.meals = meals;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getMealsAnimal() {
        String value = "";
        for (int i = 0; i < meals.length; i++) {
            if (i == 0) {
                value = meals[i];
            } else {
                value = value + ", " + meals[i];
            }
        }
        return value;
    }


}
