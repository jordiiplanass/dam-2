package com.example.zoo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private ImageView imageView2;
    private TextView typeText;
    private TextView nickText;
    private TextView ageText;
    private TextView birthText;
    private TextView mealsText;
    private TextView descripText;
    String type, nickname, meals, desc, urlImage;
    int age, birthYear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        imageView2 = findViewById(R.id.imageView2);
        typeText = findViewById(R.id.typeText);
        nickText = findViewById(R.id.nickText);
        ageText = findViewById(R.id.ageText);
        birthText = findViewById(R.id.birthText);
        mealsText = findViewById(R.id.mealsText);
        descripText = findViewById(R.id.descripText);

        getData();
        setData();
    }
    private void getData() {
        if (getIntent().hasExtra("type") &&
                getIntent().hasExtra("nickname")) {
            type = getIntent().getStringExtra("type");
            nickname = getIntent().getStringExtra("nickname");
            age = getIntent().getIntExtra("age", 1);
            birthYear = getIntent().getIntExtra("birthyear", 1);
            meals = getIntent().getStringExtra("meals");
            desc = getIntent().getStringExtra("desc");
            urlImage = getIntent().getStringExtra("urlImage");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }
    private void setData() {
        typeText.setText(type);
        nickText.setText(nickname);
        ageText.setText(age + "");
        birthText.setText(birthYear + "");
        mealsText.setText(meals);
        descripText.setText(desc);
        Picasso.get().load(urlImage)
                .fit()
                .centerCrop()
                .into(imageView2);
    }
}