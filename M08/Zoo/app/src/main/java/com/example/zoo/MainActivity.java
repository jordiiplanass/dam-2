package com.example.zoo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    ArrayList<Animal> animals = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        initData();
        MyAdapter myAdapter = new MyAdapter(this, animals);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }
    private void initData(){
        String[] animalMeals = {"M01", "M02"};
        Animal animal = new Animal (
                "https://picsum.photos/200/299.jpg",
                "Deer",
                "Peter Pan",
                7,
                2013,
                animalMeals,
                "Deer or true deer are hoofed ruminant mammals forming the family Cervidae. The two main groups of deer are the Cervinae, including the muntjac, the elk, the red deer, the fallow deer, and the chital; and the Capreolinae, including the reindeer, the roe deer, the mule deer, and the moose."
            );
        Animal animal2 = new Animal (
                "https://picsum.photos/200/300.jpg",
                "Algo",
                "Peperoni",
                2,
                1313,
                animalMeals,
                "Guacamole"
            );
        Animal animal3 = new Animal (
                "https://picsum.photos/200/301.jpg",
                "Algo2",
                "Peperon2i",
                3,
                1314,
                animalMeals,
                "Macarrones"
            );
        Animal animal4 = new Animal (
                "https://picsum.photos/200/302.jpg",
                "Origami",
                "WaxiWaoo",
                7,
                1909,
                animalMeals,
                "ASDASD"
            );
        Animal animal5 = new Animal (
                "https://picsum.photos/200/303.jpg",
                "asdfDF",
                "bgggbgbgb",
                7,
                98999,
                animalMeals,
                "LOASDLOASLDasdasd"
            );
        Animal animal6 = new Animal (
                "https://picsum.photos/200/304.jpg",
                "JEJEJE",
                "LOLASOOO",
                4,
                1200,
                animalMeals,
                "ASDASD"
            );
        animals.add(animal);
        animals.add(animal2);
        animals.add(animal3);
        animals.add(animal4);
        animals.add(animal5);
        animals.add(animal6);
    }
    private void getData(){

    }
}