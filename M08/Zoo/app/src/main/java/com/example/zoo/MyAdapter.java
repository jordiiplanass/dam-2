package com.example.zoo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<Animal> animals = new ArrayList<>();

    public MyAdapter(Context context, ArrayList<Animal> animals) {
        this.context = context;
        this.animals = animals;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_row, parent, false );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Picasso.get().load(animals.get(position)
                .getUrlImage())
                .centerCrop()
                .fit()
                .into(holder.imageView);
        holder.textView.setText(animals.get(position).getType());
        holder.textView2.setText(animals.get(position).getNickname());
        holder.textView3.setText(String.valueOf(animals.get(position).getAge()));
        holder.textView4.setText(String.valueOf(animals.get(position).getBirthYear()));
        holder.textView5.setText(String.valueOf(animals.get(position).getMealsAnimal()));
        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("type", animals.get(holder.getAdapterPosition()).getType());
                intent.putExtra("nickname", animals.get(holder.getAdapterPosition()).getNickname());
                intent.putExtra("age", animals.get(holder.getAdapterPosition()).getAge());
                intent.putExtra("birthyear", animals.get(holder.getAdapterPosition()).getBirthYear());
                intent.putExtra("meals", animals.get(holder.getAdapterPosition()).getMealsAnimal());
                intent.putExtra("desc", animals.get(holder.getAdapterPosition()).getDesc());
                intent.putExtra("urlImage", animals.get(holder.getAdapterPosition()).getUrlImage());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        private TextView textView2;
        private TextView textView3;
        private TextView textView4;
        private TextView textView5;
        private ConstraintLayout rowLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            textView2 = itemView.findViewById(R.id.textView2);
            textView3 = itemView.findViewById(R.id.textView3);
            textView4 = itemView.findViewById(R.id.textView4);
            textView5 = itemView.findViewById(R.id.textView5);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
