package com.example.examenFinal;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment3 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private int mParam4;
    private ArrayList<Integer> mParam5;
    private String mParam6;

    public Fragment3() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Fragment3.
     */
    // TODO: Rename and change types and number of parameters
    /*
    guides.get(holder.getAdapterPosition()).getGuideCity(),
                        guides.get(holder.getAdapterPosition()).getGuideName(),
                        guides.get(holder.getAdapterPosition()).getGuidePrice(),
                        guides.get(holder.getAdapterPosition()).getGuideDesc(),
                        guides.get(holder.getAdapterPosition()).getGuideImage(),
                        guides.get(holder.getAdapterPosition()).getGuideImages()
     */
    public static Fragment3 newInstance(String cityName, String guideName, String guidePrice, String guideDesc, int image, ArrayList<Integer> guideImages) {
        Fragment3 fragment = new Fragment3();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, guideName);
        args.putString(ARG_PARAM2, guidePrice);
        args.putString(ARG_PARAM3, guideDesc);
        args.putInt(ARG_PARAM4, image);
        args.putIntegerArrayList(ARG_PARAM5, guideImages);
        args.putString(ARG_PARAM6, cityName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getInt(ARG_PARAM4);
            mParam5 = getArguments().getIntegerArrayList(ARG_PARAM5);
            mParam6 = getArguments().getString(ARG_PARAM6);
        }
    }
    private FrameLayout main_frame;
    private ImageView imageDetail;
    private TextView nameDetail;
    private TextView cityDetail;
    private TextView priceDetail;
    private TextView descriptionDetail;
    private RecyclerView recyclerImages;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment3, container, false);

        //hook
        main_frame = view.findViewById(R.id.main_frame);
        imageDetail = view.findViewById(R.id.imageDetail);
        nameDetail = view.findViewById(R.id.nameDetail);
        cityDetail = view.findViewById(R.id.cityDetail);
        priceDetail = view.findViewById(R.id.priceDetail);
        descriptionDetail = view.findViewById(R.id.descriptionDetail);
        recyclerImages = view.findViewById(R.id.recyclerImages);

        nameDetail.setText(mParam1);
        cityDetail.setText(mParam6);
        priceDetail.setText(mParam2);
        descriptionDetail.setText(mParam3);
        imageDetail.setImageResource(mParam4);
        ArrayList<Integer> images = mParam5;

        recyclerImages = view.findViewById(R.id.recyclerImages);
        com.example.myapplication2.MyAdapter2 myAdapter = new com.example.myapplication2.MyAdapter2(images, view.getContext());
        recyclerImages.setAdapter(myAdapter);
        recyclerImages.setLayoutManager(new LinearLayoutManager(view.getContext()));
        return view;
    }

}