package com.example.examenFinal;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;

public class LandingActivity extends AppCompatActivity {

    private ImageView world;
    private ImageView gps;

    private ObjectAnimator objectAnimator1;
    private ObjectAnimator objectAnimator2;
    private ObjectAnimator objectAnimator3;
    private ObjectAnimator objectAnimator4;
    private ObjectAnimator objectAnimator5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        hook();
        objectAnimator1 = ObjectAnimator.ofFloat(world, "translationY", 2000f, world.getY());
        objectAnimator1.setDuration(2000);
        objectAnimator2 = ObjectAnimator.ofFloat(gps, "translationX", 700f, gps.getX());
        objectAnimator2.setDuration(2000);
        objectAnimator3 = ObjectAnimator.ofFloat(world, "rotation", 0f, 360f);
        objectAnimator3.setStartDelay(1800);
        objectAnimator3.setDuration(1000);
        objectAnimator4 = ObjectAnimator.ofFloat(world, "scaleX", 50f);
        objectAnimator5 = ObjectAnimator.ofFloat(world, "scaleY", 50f);
        objectAnimator4.setStartDelay(2800);
        objectAnimator4.setDuration(1200);
        objectAnimator5.setStartDelay(2800);
        objectAnimator5.setDuration(1200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1,
                objectAnimator2, objectAnimator3, objectAnimator4, objectAnimator5);
        animatorSet.start();

        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LandingActivity.this, MainActivity.class);

                startActivity(intent);
                finish();
            }
        };

        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, 4000);
    }

    private void hook() {
        world = findViewById(R.id.world);
        gps = findViewById(R.id.gps);
    }
}