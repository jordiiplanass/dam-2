package com.example.examenFinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView bottomNavigationView;
    private FrameLayout frameLayout;
    private Fragment1 fragment1;
    private Fragment2 fragment2;
    private Fragment3 fragment3;

    private Toolbar toolbar1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hook();
        setSupportActionBar(toolbar1);
        ArrayList<Integer> pics1 = new ArrayList<>();
        pics1.add(R.drawable.m1a1);
        pics1.add(R.drawable.m1a2);
        pics1.add(R.drawable.m1a3);
        pics1.add(R.drawable.m1a4);
        pics1.add(R.drawable.m1a5);
        Guide guide1 = new Guide("MACBA", "Barcelona", "12",
                R.drawable.m1,
                "Welcome to MACBA, a space for discovery and shared knowledge in Barcelona's Raval neighbourhood. Come and visit a collection of iconic pieces that represent key moments in the past century of art, culture and society.",
                pics1);
        fragment1 = fragment1.newInstance("a", "b");
        fragment2 = fragment2.newInstance("a", "b");
        fragment3 = fragment3.newInstance(guide1.guideName, guide1.guideName, guide1.guidePrice, guide1.guideDesc, guide1.guideImage, guide1.guideImages);

        setFragment(fragment1, "fragment1");
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            // he afegit canviar el color del toolbar com un extra
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.screen1:
                        bottomNavigationView.setItemBackgroundResource(R.color.color1);
                        toolbar1.setBackgroundColor(Color.parseColor("#3D60A8"));
                        setFragment(fragment1, "fragment1");
                        return true;
                    case R.id.screen2:
                        bottomNavigationView.setItemBackgroundResource(R.color.color2);
                        toolbar1.setBackgroundColor(Color.parseColor("#A6C36F"));
                        setFragment(fragment2, "fragment2");
                        return true;
                    case R.id.screen3:
                        bottomNavigationView.setItemBackgroundResource(R.color.color3);
                        toolbar1.setBackgroundColor(Color.parseColor("#CA4569"));
                        setFragment(fragment3, "fragment3");
                        return true;
                    default:
                        bottomNavigationView.setItemBackgroundResource(R.color.color1);
                        toolbar1.setBackgroundColor(Color.parseColor("#3D60A8"));
                        setFragment(fragment1, "fragment1");
                        return false;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.mail:
                Toast.makeText(MainActivity.this, "Mail enviat", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.language:
                Toast.makeText(MainActivity.this, "Idioma actual: Catanyol", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.settings:
                Toast.makeText(MainActivity.this, "W.I.P. Settings", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    private void hook() {
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        frameLayout = findViewById(R.id.frameLayout);
        toolbar1 = findViewById(R.id.toolbar1);
    }

    private void setFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment, tag);
        fragmentTransaction.commit();
    }
    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("fragment2");
        if (fragment != null && fragment.isVisible()) {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        } else {
            if (count == 0) {
                bottomNavigationView.setItemBackgroundResource(R.color.color2);
                toolbar1.setBackgroundColor(Color.parseColor("#A6C36F"));
                setFragment(fragment2, "fragment2");
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }



    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.item1:
                break;
            case R.id.item2:
                break;
            case R.id.item3:
                break;
            case R.id.item4:
                break;
        }
        return true;
    }
}