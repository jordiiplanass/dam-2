package com.example.examenFinal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    public static final String PREF_FILE_NAME = "MySharedFile";

    private ArrayList<Guide> guides;
    private Context context;

    public MyAdapter(ArrayList<Guide> guides, Context context) {
        this.guides = guides;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.guide.setText(guides.get(position).guideName);
        holder.city.setText(guides.get(position).guideCity);
        holder.price.setText(guides.get(position).guidePrice);
        holder.imageView.setImageResource(guides.get(position).guideImage);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment3 fragment3 = Fragment3.newInstance(
                        guides.get(holder.getAdapterPosition()).getGuideCity(),
                        guides.get(holder.getAdapterPosition()).getGuideName(),
                        guides.get(holder.getAdapterPosition()).getGuidePrice(),
                        guides.get(holder.getAdapterPosition()).getGuideDesc(),
                        guides.get(holder.getAdapterPosition()).getGuideImage(),
                        guides.get(holder.getAdapterPosition()).getGuideImages()
                );
                FragmentManager fragmentManager=((FragmentActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_frame2, fragment3);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
    @Override
    public int getItemCount() {
        return guides.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView guide;
        private TextView city;
        private TextView price;
        private ImageView imageView;
        private RelativeLayout relativeLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            guide = itemView.findViewById(R.id.guide);
            city = itemView.findViewById(R.id.city);
            price = itemView.findViewById(R.id.price);
            imageView = itemView.findViewById(R.id.imageView);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }
}
