package com.example.myapplication2;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.example.examenFinal.R;

import java.util.ArrayList;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder>{
    public static final String PREF_FILE_NAME = "MySharedFile";

    private ArrayList<Integer> imatges;
    private Context context;

    public MyAdapter2(ArrayList<Integer> imatges, Context context) {
        this.imatges = imatges;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row2, parent, false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imageView2.setImageResource(imatges.get(position).intValue());

    }
    @Override
    public int getItemCount() {
        return imatges.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView2 = itemView.findViewById(R.id.imageView2);
        }
    }
}

