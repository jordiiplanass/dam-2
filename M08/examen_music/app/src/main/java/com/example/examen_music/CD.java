package com.example.examen_music;

import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CD {
    private String nameCD;
    private String bandCD;
    private String imageCD;
    private String infoCD;
    private List<Song> songs;

    public CD(String nameCD, String imageCD, String bandCD, String infoCD, List<Song> songs) {
        this.nameCD = nameCD;
        this.bandCD = bandCD;
        this.imageCD = imageCD;
        this.infoCD = infoCD;
        this.songs = songs;
    }

    public String getNameCD() {
        return nameCD;
    }

    public void setNameCD(String nameCD) {
        this.nameCD = nameCD;
    }

    public String getBandCD() {
        return bandCD;
    }

    public void setBandCD(String bandCD) {
        this.bandCD = bandCD;
    }

    public String getImageCD() {
        return imageCD;
    }

    public void setImageCD(String imageCD) {
        this.imageCD = imageCD;
    }

    public String getInfoCD() {
        return infoCD;
    }

    public void setInfoCD(String infoCD) {
        this.infoCD = infoCD;
    }

    public List getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
