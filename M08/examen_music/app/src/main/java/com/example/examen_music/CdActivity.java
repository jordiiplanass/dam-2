package com.example.examen_music;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CdActivity extends AppCompatActivity {
    private ImageView cdImage;
    private TextView cdName;
    private TextView cdBand;
    private TextView cdInfo;
    private RecyclerView recyclerView;

    String cdImageIntented, cdNameIntented, cdBandIntented, cdInfoIntented;
    ArrayList<Song> songList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cd);

        //hook
        cdImage = findViewById(R.id.cdImage);
        cdName = findViewById(R.id.songName);
        cdBand = findViewById(R.id.songBand);
        cdInfo = findViewById(R.id.cdInfo);
        recyclerView = findViewById(R.id.recyclerView);

        getData();
        setData();
        // recyclerView
        MyAdapter2 myAdapter = new MyAdapter2(this, songList);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1,
                RecyclerView.VERTICAL, false));
    }

    private void getData(){
        if (getIntent().hasExtra("nameCD") &&
                getIntent().hasExtra("bandCD")){
            cdImageIntented = getIntent().getStringExtra("imageCD");
            cdNameIntented = getIntent().getStringExtra("nameCD");
            cdBandIntented = getIntent().getStringExtra("bandCD");
            cdInfoIntented = getIntent().getStringExtra("infoCD");
            songList = (ArrayList<Song>) getIntent().getSerializableExtra("songsCD");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }
    private void setData(){
        Picasso.get().load(cdImageIntented)
                .fit()
                .centerCrop()
                .into(cdImage);
        cdName.setText(cdNameIntented);
        cdBand.setText(cdBandIntented);
        cdInfo.setText(cdInfoIntented);
    }

}