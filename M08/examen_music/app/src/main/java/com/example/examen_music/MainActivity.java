package com.example.examen_music;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView recyclerViewHoritzontal;
    private RecyclerView recyclerViewVertical;
    private List<CD> cds = new ArrayList<>();
    List<Song> songs = new ArrayList<>();
    private Toolbar myToolbar;
    private DrawerLayout drawerLayout;
    private NavigationView nav_menu;

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "7 1/2 IS THE BEST APP EVER");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.rate:
                Toast.makeText(MainActivity.this, "Not available", Toast.LENGTH_SHORT).show();
                break;
            case R.id.contact:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:" + "jordiplanas86@gmail.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Seven Half Opinion");
                startActivity(intent);
                break;
            case R.id.about:
                String url = "https://github.com/jplanasmartinez/jplanasmartinez";
                Uri uri = Uri.parse(url);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
                break;
            case R.id.website:
                String url2 = "https://gitlab.com/jordiiplanass/dam-2/-/tree/main/M08/";
                Uri uri2 = Uri.parse(url2);
                startActivity(new Intent(Intent.ACTION_VIEW, uri2));
                break;
            case R.id.profile:
                Intent intent2 = new Intent(MainActivity.this, my_profile.class);
                startActivity(intent2);
                break;
            case R.id.donate:
                String url3 = "https://paypal.me/jordiplanas54";
                Uri uri3 = Uri.parse(url3);
                startActivity(new Intent(Intent.ACTION_VIEW, uri3));
        }
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return true;
    }
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // toolbar
        myToolbar = findViewById(R.id.myToolbar);
        drawerLayout = findViewById(R.id.drawerLayout);
        nav_menu = findViewById(R.id.nav_menu);
        setSupportActionBar(myToolbar);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(MainActivity.this,
                drawerLayout,
                myToolbar,
                R.string.menuopen,
                R.string.menuclose);
        nav_menu.setNavigationItemSelectedListener(this);

        //hook
        recyclerViewHoritzontal = findViewById(R.id.recyclerViewHoritzontal);
        recyclerViewVertical = findViewById(R.id.recyclerViewVertical);


        // data
        initData();
        MyAdapter myAdapter = new MyAdapter(this, cds);
        recyclerViewHoritzontal.setAdapter(myAdapter);
        recyclerViewHoritzontal.setLayoutManager(new GridLayoutManager(this, 1,
                RecyclerView.HORIZONTAL, false));

        MyAdapter2 myAdapter2 = new MyAdapter2(this, songs);
        recyclerViewVertical.setAdapter(myAdapter2);
        recyclerViewVertical.setLayoutManager(new GridLayoutManager(this, 1,
                RecyclerView.VERTICAL, false));
    }
    private void initData(){
        String lyrics = "Load up on guns, bring your friends\n" +
                "It's fun to lose and to pretend\n" +
                "She's over-bored and self-assured\n" +
                "Oh, no, I know a dirty word\n" +
                "\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello\n" +
                "\n" +
                "With the lights out\n" +
                "It's less dangerous\n" +
                "Here we are now, entertain us\n" +
                "I feel stupid and contagious\n" +
                "Here we are now, entertain us\n" +
                "A mulatto, an albino\n" +
                "A mosquito, my libido\n" +
                "Yeah, hey, yay\n" +
                "\n" +
                "I'm worse at what I do best\n" +
                "And for this gift I feel blessed\n" +
                "Our little group has always been\n" +
                "And always will until the end\n" +
                "\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello\n" +
                "\n" +
                "With the lights out\n" +
                "It's less dangerous\n" +
                "Here we are now, entertain us\n" +
                "I feel stupid and contagious\n" +
                "Here we are now, entertain us\n" +
                "A mulatto, an albino\n" +
                "A mosquito, my libido\n" +
                "Yeah, hey, yay\n" +
                "\n" +
                "And I forget just why I taste\n" +
                "Oh, yeah, I guess it makes me smile\n" +
                "I found it hard, it's hard to find\n" +
                "Well, whatever, nevermind\n" +
                "\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello, how low\n" +
                "Hello, hello, hello\n" +
                "\n" +
                "With the lights out\n" +
                "It's less dangerous\n" +
                "Here we are now, entertain us\n" +
                "I feel stupid and contagious\n" +
                "Here we are now, entertain us\n" +
                "A mulatto, an albino\n" +
                "A mosquito, my libido\n" +
                "\n" +
                "A denial, a denial, a denial\n" +
                "A denial, a denial, a denial\n" +
                "A denial, a denial, a denial";
        Song song11 = new Song("Title Song1 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song12 = new Song("Title Song2 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song13 = new Song("Title Song3 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song14 = new Song("Title Song4 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song15 = new Song("Title Song5 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        Song song16 = new Song("Title Song6 CD 1", "Name Band 1","https://joanseculi.com/images/cds/cd01.jpg", (short)2010, lyrics);
        List<Song> songs1 = new ArrayList<>();
        songs1.add(song11);
        songs1.add(song12);
        songs1.add(song13);
        songs1.add(song14);
        songs1.add(song15);
        songs1.add(song16);
        CD cd1 = new CD("Name CD 1", "https://joanseculi.com/images/cds/cd01.jpg", "Name Band 1",
                "This is a description of the band 1, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs1);
        cds.add(cd1);
        Song song21 = new Song("Title Song1 CD 2", "Name Band 2", "https://joanseculi.com/images/cds/cd02.jpg",(short)2010,
                lyrics);
        Song song22 = new Song("Title Song2 CD 2", "Name Band 2", "https://joanseculi.com/images/cds/cd02.jpg",(short)2010,
                lyrics);
        Song song23 = new Song("Title Song3 CD 2", "Name Band 2", "https://joanseculi.com/images/cds/cd02.jpg",(short)2010,
                lyrics);
        Song song24 = new Song("Title Song4 CD 2", "Name Band 2", "https://joanseculi.com/images/cds/cd02.jpg",(short)2010,
                lyrics);
        Song song25 = new Song("Title Song5 CD 2", "Name Band 2", "https://joanseculi.com/images/cds/cd02.jpg",(short)2010,
                lyrics);
        Song song26 = new Song("Title Song6 CD 2", "Name Band 2", "https://joanseculi.com/images/cds/cd02.jpg",(short)2010,
                lyrics);
        List<Song> songs2 = new ArrayList<>();
        songs2.add(song21);
        songs2.add(song22);
        songs2.add(song23);
        songs2.add(song24);
        songs2.add(song25);
        songs2.add(song26);
        CD cd2 = new CD("Name CD 2", "https://joanseculi.com/images/cds/cd02.jpg", "Name Band 2",
                "This is a description of the band 2, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs2);
        cds.add(cd2);
        Song song31 = new Song("Title Song1 CD 3", "Name Band 3", "https://joanseculi.com/images/cds/cd03.jpg",(short)2010,
                lyrics);
        Song song32 = new Song("Title Song2 CD 3", "Name Band 3", "https://joanseculi.com/images/cds/cd03.jpg",(short)2010,
                lyrics);
        Song song33 = new Song("Title Song3 CD 3", "Name Band 3", "https://joanseculi.com/images/cds/cd03.jpg",(short)2010,
                lyrics);
        Song song34 = new Song("Title Song4 CD 3", "Name Band 3", "https://joanseculi.com/images/cds/cd03.jpg",(short)2010,
                lyrics);
        Song song35 = new Song("Title Song5 CD 3", "Name Band 3", "https://joanseculi.com/images/cds/cd03.jpg",(short)2010,
                lyrics);
        Song song36 = new Song("Title Song6 CD 3", "Name Band 3", "https://joanseculi.com/images/cds/cd03.jpg",(short)2010,
                lyrics);
        List<Song> songs3 = new ArrayList<>();
        songs3.add(song31);
        songs3.add(song32);
        songs3.add(song33);
        songs3.add(song34);
        songs3.add(song35);
        songs3.add(song36);
        CD cd3 = new CD("Name CD 3", "https://joanseculi.com/images/cds/cd03.jpg", "Name Band 3",
                "This is a description of the band 3, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs3);
        cds.add(cd3);
        List<Song> songs4 = new ArrayList<>();
        Song song41 = new Song("Title Song1 CD 4", "Name Band 4", "https://joanseculi.com/images/cds/cd04.jpg",(short)2010,
                lyrics);
        Song song42 = new Song("Title Song2 CD 4", "Name Band 4", "https://joanseculi.com/images/cds/cd04.jpg",(short)2010,
                lyrics);
        Song song43 = new Song("Title Song3 CD 4", "Name Band 4", "https://joanseculi.com/images/cds/cd04.jpg",(short)2010,
                lyrics);
        Song song44 = new Song("Title Song4 CD 4", "Name Band 4", "https://joanseculi.com/images/cds/cd04.jpg",(short)2010,
                lyrics);
        Song song45 = new Song("Title Song5 CD 4", "Name Band 4", "https://joanseculi.com/images/cds/cd04.jpg",(short)2010,
                lyrics);
        Song song46 = new Song("Title Song6 CD 4", "Name Band 4", "https://joanseculi.com/images/cds/cd04.jpg",(short)2010,
                lyrics);
        songs4.add(song41);
        songs4.add(song42);
        songs4.add(song43);
        songs4.add(song44);
        songs4.add(song45);
        songs4.add(song46);
        CD cd4 = new CD("Name CD 4", "https://joanseculi.com/images/cds/cd04.jpg", "Name Band 4",
                "This is a description of the band 4, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs4);
        cds.add(cd4);
        List<Song> songs5 = new ArrayList<>();
        Song song51 = new Song("Title Song1 CD 5", "Name Band 5", "https://joanseculi.com/images/cds/cd05.jpg",(short)2010,
                lyrics);
        Song song52 = new Song("Title Song2 CD 5", "Name Band 5", "https://joanseculi.com/images/cds/cd05.jpg",(short)2010,
                lyrics);
        Song song53 = new Song("Title Song3 CD 5", "Name Band 5", "https://joanseculi.com/images/cds/cd05.jpg",(short)2010,
                lyrics);
        Song song54 = new Song("Title Song4 CD 5", "Name Band 5", "https://joanseculi.com/images/cds/cd05.jpg",(short)2010,
                lyrics);
        Song song55 = new Song("Title Song5 CD 5", "Name Band 5", "https://joanseculi.com/images/cds/cd05.jpg",(short)2010,
                lyrics);
        Song song56 = new Song("Title Song6 CD 5", "Name Band 5", "https://joanseculi.com/images/cds/cd05.jpg",(short)2010,
                lyrics);
        songs5.add(song51);
        songs5.add(song52);
        songs5.add(song53);
        songs5.add(song54);
        songs5.add(song55);
        songs5.add(song56);
        CD cd5 = new CD("Name CD 5", "https://joanseculi.com/images/cds/cd05.jpg", "Name Band 5",
                "This is a description of the band 5, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs5);
        cds.add(cd5);
        List<Song> songs6 = new ArrayList<>();
        Song song61 = new Song("Title Song1 CD 6", "Name Band 6", "https://joanseculi.com/images/cds/cd06.jpg",(short)2010,
                lyrics);
        Song song62 = new Song("Title Song2 CD 6", "Name Band 6", "https://joanseculi.com/images/cds/cd06.jpg",(short)2010,
                lyrics);
        Song song63 = new Song("Title Song3 CD 6", "Name Band 6", "https://joanseculi.com/images/cds/cd06.jpg",(short)2010,
                lyrics);
        Song song64 = new Song("Title Song4 CD 6", "Name Band 6", "https://joanseculi.com/images/cds/cd06.jpg",(short)2010,
                lyrics);
        Song song65 = new Song("Title Song5 CD 6", "Name Band 6", "https://joanseculi.com/images/cds/cd06.jpg",(short)2010,
                lyrics);
        Song song66 = new Song("Title Song6 CD 6", "Name Band 6", "https://joanseculi.com/images/cds/cd06.jpg",(short)2010,
                lyrics);
        songs6.add(song61);
        songs6.add(song62);
        songs6.add(song63);
        songs6.add(song64);
        songs6.add(song65);
        songs6.add(song66);
        CD cd6 = new CD("Name CD 6", "https://joanseculi.com/images/cds/cd06.jpg", "Name Band 6",
                "This is a description of the band 6, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs6);
        cds.add(cd6);
        List<Song> songs7 = new ArrayList<>();
        Song song71 = new Song("Title Song1 CD 7", "Name Band 7", "https://joanseculi.com/images/cds/cd07.jpg",(short)2010,
                lyrics);
        Song song72 = new Song("Title Song2 CD 7", "Name Band 7", "https://joanseculi.com/images/cds/cd07.jpg",(short)2010,
                lyrics);
        Song song73 = new Song("Title Song3 CD 7", "Name Band 7", "https://joanseculi.com/images/cds/cd07.jpg",(short)2010,
                lyrics);
        Song song74 = new Song("Title Song4 CD 7", "Name Band 7", "https://joanseculi.com/images/cds/cd07.jpg",(short)2010,
                lyrics);
        Song song75 = new Song("Title Song5 CD 7", "Name Band 7", "https://joanseculi.com/images/cds/cd07.jpg",(short)2010,
                lyrics);
        Song song76 = new Song("Title Song6 CD 7", "Name Band 7", "https://joanseculi.com/images/cds/cd07.jpg",(short)2010,
                lyrics);
        songs7.add(song71);
        songs7.add(song72);
        songs7.add(song73);
        songs7.add(song74);
        songs7.add(song75);
        songs7.add(song76);
        CD cd7 = new CD("Name CD 7", "https://joanseculi.com/images/cds/cd07.jpg", "Name Band 7",
                "This is a description of the band 7, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs7);
        cds.add(cd7);
        List<Song> songs8 = new ArrayList<>();
        Song song81 = new Song("Title Song1 CD 8", "Name Band 8", "https://joanseculi.com/images/cds/cd08.jpg",(short)2010,
                lyrics);
        Song song82 = new Song("Title Song2 CD 8", "Name Band 8", "https://joanseculi.com/images/cds/cd08.jpg",(short)2010,
                lyrics);
        Song song83 = new Song("Title Song3 CD 8", "Name Band 8", "https://joanseculi.com/images/cds/cd08.jpg",(short)2010,
                lyrics);
        Song song84 = new Song("Title Song4 CD 8", "Name Band 8", "https://joanseculi.com/images/cds/cd08.jpg",(short)2010,
                lyrics);
        Song song85 = new Song("Title Song5 CD 8", "Name Band 8", "https://joanseculi.com/images/cds/cd08.jpg",(short)2010,
                lyrics);
        Song song86 = new Song("Title Song6 CD 8", "Name Band 8", "https://joanseculi.com/images/cds/cd08.jpg",(short)2010,
                lyrics);
        songs8.add(song81);
        songs8.add(song82);
        songs8.add(song83);
        songs8.add(song84);
        songs8.add(song85);
        songs8.add(song86);
        CD cd8 = new CD("Name CD 8", "https://joanseculi.com/images/cds/cd08.jpg", "Name Band 8",
                "This is a description of the band 8, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs8);
        cds.add(cd8);
        List<Song> songs9 = new ArrayList<>();
        Song song91 = new Song("Title Song1 CD 9", "Name Band 9", "https://joanseculi.com/images/cds/cd09.jpg",(short)2010,
                lyrics);
        Song song92 = new Song("Title Song2 CD 9", "Name Band 9", "https://joanseculi.com/images/cds/cd09.jpg",(short)2010,
                lyrics);
        Song song93 = new Song("Title Song3 CD 9", "Name Band 9", "https://joanseculi.com/images/cds/cd09.jpg",(short)2010,
                lyrics);
        Song song94 = new Song("Title Song4 CD 9", "Name Band 9", "https://joanseculi.com/images/cds/cd09.jpg",(short)2010,
                lyrics);
        Song song95 = new Song("Title Song5 CD 9", "Name Band 9", "https://joanseculi.com/images/cds/cd09.jpg",(short)2010,
                lyrics);
        Song song96 = new Song("Title Song6 CD 9", "Name Band 9", "https://joanseculi.com/images/cds/cd09.jpg",(short)2010,
                lyrics);
        songs9.add(song91);
        songs9.add(song92);
        songs9.add(song93);
        songs9.add(song94);
        songs9.add(song95);
        songs9.add(song96);
        CD cd9 = new CD("Name CD 9", "https://joanseculi.com/images/cds/cd09.jpg", "Name Band 9",
                "This is a description of the band 9, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs9);
        cds.add(cd9);
        List<Song> songs10 = new ArrayList<>();
        Song song101 = new Song("Title Song1 CD 10", "Name Band 10", "https://joanseculi.com/images/cds/cd10.jpg",(short)2010,
                lyrics);
        Song song102 = new Song("Title Song2 CD 10", "Name Band 10", "https://joanseculi.com/images/cds/cd10.jpg",(short)2010,
                lyrics);
        Song song103 = new Song("Title Song3 CD 10", "Name Band 10", "https://joanseculi.com/images/cds/cd10.jpg",(short)2010,
                lyrics);
        Song song104 = new Song("Title Song4 CD 10", "Name Band 10", "https://joanseculi.com/images/cds/cd10.jpg",(short)2010,
                lyrics);
        Song song105 = new Song("Title Song5 CD 10", "Name Band 10", "https://joanseculi.com/images/cds/cd10.jpg",(short)2010,
                lyrics);
        Song song106 = new Song("Title Song6 CD 10", "Name Band 10", "https://joanseculi.com/images/cds/cd10.jpg",(short)2010,
                lyrics);
        songs10.add(song101);
        songs10.add(song102);
        songs10.add(song103);
        songs10.add(song104);
        songs10.add(song105);
        songs10.add(song106);
        CD cd10 = new CD("Name CD 10", "https://joanseculi.com/images/cds/cd10.jpg", "Name Band 10",
                "This is a description of the band 10, this constains information about the genre, the members of the band and also it contains the history of the band",
                songs10);
        cds.add(cd10);

        songs.add(song11);
        songs.add(song21);


        songs.add(song12);
        songs.add(song22);

        songs.add(song13);
        songs.add(song23);

        songs.add(song14);
        songs.add(song24);

        songs.add(song15);
        songs.add(song25);

        songs.add(song16);
        songs.add(song26);
    }


}
