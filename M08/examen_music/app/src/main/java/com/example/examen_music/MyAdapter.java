package com.example.examen_music;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private Context context;
    private List<CD> cdsList;

    public MyAdapter(Context context, List<CD> cdsList) {
        this.context = context;
        this.cdsList = cdsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.horizontalrecycler, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        Picasso.get().load(cdsList.get(position).getImageCD())
                .centerCrop()
                .fit()
                .into(holder.imageView);
        holder.textView.setText(cdsList.get(position).getNameCD());

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CdActivity.class);
                intent.putExtra("bandCD", cdsList.get(holder.getAdapterPosition()).getBandCD());
                intent.putExtra("infoCD", cdsList.get(holder.getAdapterPosition()).getInfoCD());
                intent.putExtra("imageCD", cdsList.get(holder.getAdapterPosition()).getImageCD());
                intent.putExtra("nameCD", cdsList.get(holder.getAdapterPosition()).getNameCD());
                ArrayList<Song> songs = new ArrayList<Song>(cdsList.get(holder.getAdapterPosition()).getSongs());
                intent.putExtra("songsCD", songs);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cdsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        private ConstraintLayout rowLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cdCover);
            textView = itemView.findViewById(R.id.cdTitle);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
