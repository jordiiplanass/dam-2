package com.example.examen_music;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder2> {
    private Context context;
    private List<Song> songList = new ArrayList<>();
    private ObjectAnimator objectAnimator;
    public MyAdapter2(Context context, List<Song> songList) {
        this.context = context;
        this.songList = songList;
    }

    @NonNull
    @Override
    public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater2 = LayoutInflater.from(context);
        View view = inflater2.inflate(R.layout.verticalrecycler, parent, false);
        return new MyViewHolder2(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder2 holder, int position) {
        Picasso.get().load(songList.get(position).getImageSong())
                .centerCrop()
                .fit()
                .into(holder.imageView);
        holder.textView.setText(songList.get(position).getNameSong());
        holder.textView2.setText(songList.get(position).getBandSong());
        // boto rotatori
        holder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // default image
                int imageOfButton = R.drawable.play;
                // depenent del estat, una imatge o una altre
                switch (holder.estado){
                    case STOP:
                        imageOfButton = R.drawable.play;
                        holder.estado = Estado.PLAY;
                        break;
                    case PLAY:
                        imageOfButton = R.drawable.play2;
                        holder.estado = Estado.STOP;
                        break;
                    default:
                        imageOfButton = R.drawable.play;
                        holder.estado = Estado.PLAY;
                        break;
                }
                objectAnimator = ObjectAnimator.ofFloat(holder.imageButton, "rotation", 0f, 720f);
                objectAnimator.setDuration(1000);
                // no acabo d'entendre perque, pero em demana crear una nova variable per a la imatge
                int finalImageOfButton = imageOfButton;
                objectAnimator.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.imageButton.setImageResource(finalImageOfButton);
                    }
                }, 1000);
            }
        });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SongActivity.class);
                intent.putExtra("nameSong", songList.get(holder.getAdapterPosition()).getNameSong());
                intent.putExtra("imageSong", songList.get(holder.getAdapterPosition()).getImageSong());
                intent.putExtra("bandSong", songList.get(holder.getAdapterPosition()).getBandSong());
                intent.putExtra("yearSong", songList.get(holder.getAdapterPosition()).getYearSong());
                intent.putExtra("lyrics", songList.get(holder.getAdapterPosition()).getLyrics());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        private TextView textView2;
        private ImageButton imageButton;
        private Estado estado;
        private ConstraintLayout rowLayout;
        public MyViewHolder2(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageViewSong);
            textView = itemView.findViewById(R.id.textView3);
            textView2 = itemView.findViewById(R.id.textView4);
            rowLayout = itemView.findViewById(R.id.rowLayout);
            imageButton = itemView.findViewById(R.id.imageButton);
            estado = Estado.PLAY;
        }
    }
}
