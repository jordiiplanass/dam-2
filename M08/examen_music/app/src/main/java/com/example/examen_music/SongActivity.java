package com.example.examen_music;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class SongActivity extends AppCompatActivity {
    private ImageView songImage;
    private TextView songName;
    private TextView songBand;
    private TextView songYear;
    private TextView songLyrics;
    private ImageButton imageButton;
    String imageIntented;
    String nameIntented;
    String bandIntented;
    String yearIntented;
    String lyricsIntented;
    private Estado estado = Estado.PLAY;
    ObjectAnimator objectAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        //hook
        songImage = findViewById(R.id.songImage);
        songName = findViewById(R.id.songName);
        songBand = findViewById(R.id.songBand);
        songYear = findViewById(R.id.songYear);
        songLyrics = findViewById(R.id.songLyrics);
        imageButton = findViewById(R.id.imageButton);

        getData();
        setData();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int imageResource = R.drawable.play;
                switch (estado){
                    case STOP:
                        imageResource = R.drawable.play;
                        estado = Estado.PLAY;
                        break;
                    case PLAY:
                        imageResource = R.drawable.play2;
                        estado = Estado.STOP;
                        break;
                    default:
                        imageResource = R.drawable.play;
                        estado = Estado.PLAY;
                        break;
                }
                objectAnimator = ObjectAnimator.ofFloat(imageButton, "rotation", 0f, 720f);
                objectAnimator.setDuration(1000);
                objectAnimator.start();
                int finalImageOfButton = imageResource;
                // no acabo d'entendre perque, pero em demana crear una nova variable per a la imatge
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageButton.setImageResource(finalImageOfButton);
                    }
                }, 1000);

            }
        });
    }

    private void getData() {
        if (getIntent().hasExtra("imageSong") &&
                getIntent().hasExtra("nameSong")) {
            imageIntented = getIntent().getStringExtra("imageSong");
            nameIntented = getIntent().getStringExtra("nameSong");
            bandIntented = getIntent().getStringExtra("bandSong");
            yearIntented = Short.toString(getIntent().getShortExtra("yearSong", Short.MIN_VALUE));
            lyricsIntented = getIntent().getStringExtra("lyrics");
        } else{
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        Picasso.get().load(imageIntented)
                .fit()
                .centerCrop()
                .into(songImage);
        songName.setText(nameIntented);
        songBand.setText(bandIntented);
        songYear.setText(yearIntented);
        songLyrics.setText(lyricsIntented);
    }
}