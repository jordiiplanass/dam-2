package com.example.examen_music;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class my_profile extends AppCompatActivity {
    private ImageView imageIcon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        //hook
        imageIcon = findViewById(R.id.imageIcon);

        Picasso.get().load("https://gitlab.com/uploads/-/system/user/avatar/8683499/avatar.png?width=400")
                .centerCrop()
                .fit()
                .into(imageIcon);
    }
}