package com.example.kotlintest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    val CONSTANT = "com.example.edt74.CONSTANT"
    private lateinit var editText1: TextView
    private lateinit var button1: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //hook
        editText1 = findViewById(R.id.editText1)
        button1 = findViewById(R.id.button1)

        button1.setOnClickListener {
            val intent = Intent(this, newActivity::class.java).apply {
                putExtra(CONSTANT, editText1.text)
            }
            startActivity(intent)
        }
        valVar2()
        ifSentence()
        whenFunction()
        arrayFunction()
        objetoPersona()
    }

    private fun objetoPersona() {
        var persona11 = Person("Jordi", "Pedrosa", 100, 50.0, true)
        var persona12 = Person("Pedrito", "Pedrosa", 99, 50.0, true)
        var persona13 = Person("Josefa", "Pedrosa", 100, 150.0, true)
        var arrayPerson = ArrayList<Person>()
        arrayPerson.add(persona11)
        arrayPerson.add(persona12)
        arrayPerson.add(persona13)

        arrayPerson.forEach{
            println(it.name)
        }
        for (element in arrayPerson){
            println(element.age)
        }
    }

    private fun arrayFunction() {
        val country1 = "Spain"
        val country2 = "France"
        val country3 = "Italy"
        val country4 = "UK"
        val country5 = "US"

        val myArray = arrayListOf<String>()

        myArray.add(country1)
        myArray.add(country2)
        myArray.add(country3)
        myArray.add(country4)
        myArray.add(country5)

        myArray.removeAt(1)
        myArray.forEach{
            println(it)
        }

    }

    private fun whenFunction() {
        var text = "Barcelona"
        when (text){
          "Madrid" -> {
              println("$text tiene el mejor awa")
          } "Barcelona" -> {
              println ("$text tiene poder")
          } else -> {
              println ("$text es una mierda")
          }
        }
    }
    private fun whenFunction2() {
        var number = 4
        when (number){
          0 -> {
              println("useless")
          } 1,2,3,4 -> {
              println ("I wanna feel the heat")
          } else -> {
              println ("Potato")
          }
        }
    }

    private fun ifSentence() {
        var num = 5
        if (num > 4) {
            println("$num")
        }else if (num == 5){
            println("$num es 5")
        }
    }

    private fun valVar2() {
        var numero: Int = 5
        var numero2 = 3_000_000_000
        var valid = true
        var number3 = 3.0
        var number4 = 7f
        var text = "Hey!"

        var number5: Byte = 7
        var vacio: Unit = Unit

        val NUM: Int = 3
        val NUM2: Int = 7
        val TEXT: String = "7"
        val NUM3: Long = NUM.toLong()
        val NUM4: Double = NUM3.toDouble()

        var txt = "email@mail.com"
        val email = "Email: $txt"                // Email: email@mail.com
        val email2 = "Email: ${txt.uppercase()}" // Email: EMAIL@MAIL.COM
    }

}