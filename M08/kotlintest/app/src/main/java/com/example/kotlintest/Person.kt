package com.example.kotlintest;

class Person(
    var name: String,
    var surname: String,
    var age: Int,
    var balance: Double,
    var isEmployee: Boolean
)
