package com.example.marvel;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AdapterArticles extends RecyclerView.Adapter<AdapterArticles.MyViewHolder> {
    private Context context;
    private ArrayList<Entry> entries;

    public AdapterArticles(Context context, ArrayList<Entry> entries) {
        this.context = context;
        this.entries = entries;
    }

    @NonNull
    @Override
    public AdapterArticles.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_detail, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterArticles.MyViewHolder holder, int position) {
        Picasso.get().load(entries.get(position).urlToImage)
                .fit()
                .centerCrop()
                .into(holder.urlToImage);
        holder.title.setText(entries.get(position).title);
        if (entries.get(position).description.length() > 100){
            holder.description.setText(entries.get(position).description.substring(0,100));
        } else {
            holder.description.setText(entries.get(position).description);
        }
        holder.publishedAt.setText(entries.get(position).publishedAt);
        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra(Variables.INTENT_DETAIL, entries.get(position));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView urlToImage;
        TextView title;
        TextView description;
        TextView publishedAt;
        CardView cardView;
        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            urlToImage = itemView.findViewById(R.id.urlToImage);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            publishedAt = itemView.findViewById(R.id.publishedAt);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
