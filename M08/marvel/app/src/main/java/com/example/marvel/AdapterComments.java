package com.example.marvel;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvel.R;

import java.util.ArrayList;

public class AdapterComments extends RecyclerView.Adapter<AdapterComments.MyViewHolder> {

    Context context;
    ArrayList<String> comm;

    public AdapterComments(Context context, ArrayList<String> comm) {
        this.context = context;
        this.comm = comm;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.comment.setText(comm.get(position));
    }

    @Override
    public int getItemCount() {
        return comm.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView comment;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            comment = itemView.findViewById(R.id.comment);
        }
    }
}