package com.example.marvel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {
    YouTubePlayerView urlVideo;
    Entry entry;
    ViewPager media2;
    ImageView urlToImage2;
    TextView title2;
    TextView publishedAt2;
    TextView description2;
    TextView content2;
    TextView authorname2;
    Button url;
    Button authorlink;
    Button contact;
    RecyclerView comments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        hook();
        Intent intent = getIntent();
        if (intent != null){
            entry = getIntent().getExtras().getParcelable(Variables.INTENT_DETAIL);
            setData();

        }
    }



    private void hook() {
        urlToImage2 = findViewById(R.id.urlToImage2);
        title2 = findViewById(R.id.title2);
        publishedAt2 = findViewById(R.id.publishedAt2);
        description2 = findViewById(R.id.description2);
        content2 = findViewById(R.id.content2);
        authorname2 = findViewById(R.id.authorname2);
        url = findViewById(R.id.url);
        authorlink = findViewById(R.id.authorlink);
        contact = findViewById(R.id.contact);
        urlVideo = findViewById(R.id.urlVideo);
        media2 = findViewById(R.id.media2);
        comments = findViewById(R.id.comments);

    }
    private void setData() {
        // image
        Picasso.get()
                .load(Uri.parse(entry.urlToImage))
                .centerCrop()
                .fit()
                .into(urlToImage2);
        // all text views
        title2.setText(entry.title);
        publishedAt2.setText(entry.publishedAt);
        description2.setText(entry.description);
        content2.setText(entry.content);
        authorname2.setText(entry.authorname);
        // buttons
        url.setOnClickListener(v -> {
            Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse(entry.url));
            startActivity(intent2);
        });
        authorlink.setOnClickListener(v -> {
            Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse(entry.authorlink));
            startActivity(intent2);
        });
        contact.setOnClickListener(v -> {
            Intent intent3 = new Intent(Intent.ACTION_SENDTO);
            intent3.setData(Uri.parse("mailto:" + entry.contact)); // only email apps should handle this
            startActivity(Intent.createChooser(intent3, "Send Email"));
        });
        // youtube
        getLifecycle().addObserver(urlVideo);
        urlVideo.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId = entry.urlvideo.substring(30);
                youTubePlayer.cueVideo(videoId, 0);
            }
        });
        // images
        ArrayList<String> mediaEntry = new ArrayList<>();
        mediaEntry.add(entry.media.urlToImage1);
        mediaEntry.add(entry.media.urlToImage2);
        mediaEntry.add(entry.media.urlToImage3);
        SliderAdapter sliderAdapter = new SliderAdapter(this, mediaEntry);
        media2.setAdapter(sliderAdapter);
        // comments
        ArrayList<String> comentarios = new ArrayList<>();
        for(int i = 0 ; i < entry.comments.length ; i++){
            comentarios.add(entry.comments[i]);
        }
        AdapterComments adapterComments = new AdapterComments(this, comentarios);
        comments.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        comments.setAdapter(adapterComments);

    }
}