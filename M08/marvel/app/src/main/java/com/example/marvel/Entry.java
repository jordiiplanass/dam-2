package com.example.marvel;

import android.os.Parcel;
import android.os.Parcelable;

public class Entry implements Parcelable {
    public String authorlink;
    public String authorname;
    public String contact;
    public String title;
    public String description;
    public String url;
    public String urlToImage;
    public String publishedAt;
    public String content;
    public Media media;
    public String urlvideo;
    public String[] comments;

    public Entry() {
    }

    protected Entry(Parcel in) {
        authorlink = in.readString();
        authorname = in.readString();
        contact = in.readString();
        title = in.readString();
        description = in.readString();
        url = in.readString();
        urlToImage = in.readString();
        publishedAt = in.readString();
        content = in.readString();
        media = in.readParcelable(Media.class.getClassLoader());
        urlvideo = in.readString();
        comments = in.createStringArray();
    }

    public static final Creator<Entry> CREATOR = new Creator<Entry>() {
        @Override
        public Entry createFromParcel(Parcel in) {
            return new Entry(in);
        }

        @Override
        public Entry[] newArray(int size) {
            return new Entry[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(authorlink);
        parcel.writeString(authorname);
        parcel.writeString(contact);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(url);
        parcel.writeString(urlToImage);
        parcel.writeString(publishedAt);
        parcel.writeString(content);
        parcel.writeParcelable(media, i);
        parcel.writeString(urlvideo);
        parcel.writeStringArray(comments);
    }
}
