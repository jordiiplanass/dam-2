package com.example.marvel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static String JSON = "https://run.mocky.io/v3/39fcc41e-9d03-486c-8fb2-235e3e831a1b";
    private RecyclerView recyclerView;
    private ImageView imageView2;
    AdapterArticles adapterArticles;
    ArrayList entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // https://run.mocky.io/v3/39fcc41e-9d03-486c-8fb2-235e3e831a1b
        hook();
        entries = new ArrayList<>();
        getMovies();
    }

    private void hook() {
        recyclerView = findViewById(R.id.recyclerView);
        imageView2 = findViewById(R.id.imageView2);
        Picasso.get().load(Uri.parse("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Marvel_Logo.svg/2560px-Marvel_Logo.svg.png"))
                .fit()
                .centerCrop()
                .into(imageView2);
        // no se perque en el layout no em sortia la imatge de normal posant-la de drawable, per
        // tant he utilitzat picasso
    }

    private void getMovies() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON,
                null,
                new Response.Listener<JSONObject>() { //Response listener
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.optString("status");
                    String totalResults = response.optString("totalResults");
                    JSONArray jsonArray = response.optJSONArray("articles");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject peakObject = jsonArray.getJSONObject(i);
                            Entry entry = new Entry(); //empty constructor
                            entry.authorlink = peakObject.getString("authorlink");
                            entry.authorname = peakObject.getString("authorname");
                            entry.contact = peakObject.getString("contact");
                            entry.title = peakObject.getString("title");
                            entry.description = peakObject.getString("description");
                            entry.url = peakObject.getString("url");
                            entry.urlToImage = peakObject.getString("urlToImage");
                            entry.publishedAt = peakObject.getString("publishedAt");
                            entry.content = peakObject.getString("content");
                            JSONObject medias = peakObject.getJSONObject("media");
                            String urlToImage1 = medias.getString("urlToImage1");
                            String urlToImage2 = medias.getString("urlToImage2");
                            String urlToImage3 = medias.getString("urlToImage3");
                            entry.media = new Media(urlToImage1, urlToImage2, urlToImage3);
                            entry.urlvideo = peakObject.getString("urlvideo");
                            JSONArray jsonArray1 = peakObject.getJSONArray("comments");
                            String[] comm = new String[jsonArray1.length()];
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                comm[j] = (jsonArray1.getString(j));
                            }
                            entry.comments = comm;
                            entries.add(entry);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    adapterArticles = new AdapterArticles(getApplicationContext(), entries);
                    recyclerView.setAdapter(adapterArticles);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                        int a = 3;
                    }
                }

        );
        queue.add(jsonObjectRequest);
    }
}