package com.example.marvel;

import android.os.Parcel;
import android.os.Parcelable;

public class Media implements Parcelable {
    public String urlToImage1;
    public String urlToImage2;
    public String urlToImage3;

    public Media() {
    }

    public Media(String urlToImage1, String urlToImage2, String urlToImage3) {
        this.urlToImage1 = urlToImage1;
        this.urlToImage2 = urlToImage2;
        this.urlToImage3 = urlToImage3;
    }

    protected Media(Parcel in) {
        urlToImage1 = in.readString();
        urlToImage2 = in.readString();
        urlToImage3 = in.readString();
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(urlToImage1);
        parcel.writeString(urlToImage2);
        parcel.writeString(urlToImage3);
    }
}
