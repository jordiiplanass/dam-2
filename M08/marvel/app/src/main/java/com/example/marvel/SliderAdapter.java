package com.example.marvel;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SliderAdapter extends PagerAdapter {
    private Context mContext;
    private ArrayList<String> mImagesIds;

    public SliderAdapter(Context mContext, ArrayList<String> mImagesIds) {
        this.mContext = mContext;
        this.mImagesIds = mImagesIds;
    }


    @Override
    public int getCount() {
        return mImagesIds.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        Picasso.get().load(Uri.parse(mImagesIds.get(position)))
                .fit()
                .centerCrop()
                .into(imageView);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object
            object) {
        //super.destroyItem(container, position, object);
        container.removeView((ImageView) object);
    }
}
