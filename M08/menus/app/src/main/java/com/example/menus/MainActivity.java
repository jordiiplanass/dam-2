package com.example.menus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.anchor:
                Toast.makeText(MainActivity.this, "Anchor", Toast.LENGTH_SHORT).show();
                break;
            case R.id.beach:
                Toast.makeText(MainActivity.this, "beach", Toast.LENGTH_SHORT).show();
                break;
            case R.id.poke:
                //Toast.makeText(MainActivity.this, "poke", Toast.LENGTH_SHORT).show();
                break;
            // submenu
                case R.id.poke_lets:
                    Toast.makeText(MainActivity.this, "POKEMON1", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.poke_catch:
                    Toast.makeText(MainActivity.this, "POKEMON2", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.poke_em:
                    Toast.makeText(MainActivity.this, "POKEMON3", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.poke_all:
                    Toast.makeText(MainActivity.this, "POKEMON4", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.sofa:
                Toast.makeText(MainActivity.this, "sofa", Toast.LENGTH_SHORT).show();
                break;
            case R.id.color_pick:
                Toast.makeText(MainActivity.this, "color_pick", Toast.LENGTH_SHORT).show();
                break;

        }
        return true;
    }
}