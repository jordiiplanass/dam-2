package com.example.myapplication2;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    public static final String PREF_FILE_NAME = "MySharedFile";

    private ArrayList<Fotito> fotitos;
    private Context context;

    public MyAdapter(ArrayList<Fotito> fotitos, Context context) {
        this.fotitos = fotitos;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recycler_row1, parent, false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(fotitos.get(position).foto)
                .centerCrop()
                .fit()
                .into(holder.image1);
        String imagen = fotitos.get(position).foto;
        holder.text1.setText(fotitos.get(position).titulo);
        holder.text2.setText(fotitos.get(position).descripcion);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Character",holder.text1.getText().toString());
                editor.putString("Anime",holder.text2.getText().toString());
                editor.putString("Image",imagen);
                editor.commit();
                Toast.makeText(context, "You selected " + holder.text1.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public int getItemCount() {
        return fotitos.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView text1;
        private TextView text2;
        private ImageView image1;
        private CardView cardView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            text1 = itemView.findViewById(R.id.text1);
            text2 = itemView.findViewById(R.id.text2);
            image1 = itemView.findViewById(R.id.image1);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
