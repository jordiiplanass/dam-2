package com.example.myapplication2;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_home extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ArrayList<Fotito> fotos = new ArrayList<>();
    private RecyclerView recyclerView;
    private String mParam1;
    private String mParam2;

    public fragment_home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragment_home.
     */
    // TODO: Rename and change types and number of parameters
    public static fragment_home newInstance(String param1, String param2) {
        fragment_home fragment = new fragment_home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initData();
        recyclerView = view.findViewById(R.id.recyclerView);

        MyAdapter myAdapter = new MyAdapter(fotos, view.getContext());

        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 3, RecyclerView.VERTICAL, false));
        return view;
    }
    private void initData() {
        Resources res = getResources();
        String[] arrayName = res.getStringArray(R.array.items);
        String[] arrayAnime = res.getStringArray(R.array.items2);
        fotos.clear();
        fotos.add(new Fotito(arrayName[0], arrayAnime[0], "https://static.wikia.nocookie.net/berserk/images/9/94/Guts_para_chakram_de_Silat_%28manga%29.jpg/revision/latest/scale-to-width-down/1000?cb=20180323043123&path-prefix=es"));
        fotos.add(new Fotito(arrayName[1], arrayAnime[1], "https://image.winudf.com/v2/image/Y29tLmJhbGVmb290Lk1vbmtleURMdWZmeVdhbGxwYXBlcl9zY3JlZW5fMF8xNTI0NTE5MTEwXzAyOA/screen-0.jpg?fakeurl=1&type=.jpg"));
        fotos.add(new Fotito(arrayName[2], arrayAnime[2], "https://img.wattpad.com/e29a2c1fe0f67685beab4be1aa11b086e315286b/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f6a74395f46524c746748476c35673d3d2d313032383530373236362e313636356539656539303238336332653837373336353733363937382e6a7067?s=fit&w=720&h=720"));
        fotos.add(new Fotito(arrayName[3], arrayAnime[3], "https://pbs.twimg.com/profile_images/1292630859531591681/-_YsvraA_400x400.jpg"));
        fotos.add(new Fotito(arrayName[4], arrayAnime[4], "https://laverdadnoticias.com/__export/1624291527694/sites/laverdad/img/2021/06/21/deku_villano_my_hero_academia_317_manga.jpg_2065693783.jpg"));
        fotos.add(new Fotito(arrayName[5], arrayAnime[5], "https://esports.as.com/2021/12/29/bonus/influencers/Willyrex_1533156686_878765_1024x576.png"));
        fotos.add(new Fotito(arrayName[6], arrayAnime[6], "https://static.wikia.nocookie.net/shingeki-no-kyojin/images/e/ea/Eren_Jaeger_-_Anime.png/revision/latest?cb=20190429004402&path-prefix=es"));
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}