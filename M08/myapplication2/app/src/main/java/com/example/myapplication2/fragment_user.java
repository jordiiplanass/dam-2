package com.example.myapplication2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_user#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_user extends Fragment {

    public static final String PREF_FILE_NAME = "MySharedFile";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageView imageView;
    private TextView textView;
    private String mParam1;
    private String mParam2;

    public fragment_user() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragment_user.
     */
    // TODO: Rename and change types and number of parameters
    public static fragment_user newInstance(String param1, String param2) {
        fragment_user fragment = new fragment_user();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        String character = sharedPreferences.getString("Character", "No data found");
        String anime = sharedPreferences.getString("Anime", "No data found");
        String imageString = sharedPreferences.getString("Image", "No data found");
        textView = view.findViewById(R.id.textView);
        imageView = view.findViewById(R.id.imageView);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        textView.setText(character);
        Picasso.get().load(imageString)
                .fit().
                centerCrop().into(imageView);
        return view;
    }
}