package com.example.toolbarproves;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar my_Toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView nav_view;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // toolbar
        my_Toolbar = findViewById(R.id.my_toolbar);
        drawerLayout = findViewById(R.id.drawerLayout);
        nav_view = findViewById(R.id.nav_view);
        setSupportActionBar(my_Toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(MainActivity.this,
                drawerLayout,
                my_Toolbar,
                R.string.menuopen,
                R.string.menuclose);
        nav_view.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.pokemon:
                Toast.makeText(MainActivity.this, "LET'S CATCH THEM ALL", Toast.LENGTH_SHORT).show();
                break;
            case R.id.burger:
                Toast.makeText(MainActivity.this, "BURGIR", Toast.LENGTH_SHORT).show();
                break;
            case R.id.contacts:
                Toast.makeText(MainActivity.this, "u have no friends...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bitcoin:
                Toast.makeText(MainActivity.this, "DAME TU DINERO", Toast.LENGTH_SHORT).show();
                break;
            case R.id.chair:
                Toast.makeText(MainActivity.this, "Levantate, que seguro estas en una silla, vago", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(MainActivity.this, "a", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}