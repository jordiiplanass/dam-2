package test1;

public class MainProgram extends Thread {
    String string;

    public MainProgram(String str){
        string = str;
    }

    public void run(){
        for (int x = 0 ; x < 5 ; x++){
            System.out.println(string + " " + x);
        }
    }

    public static void main(String[] args){
        // Cadascun dels thread es un fil.
        Thread thread1 = new MainProgram("Fil 1");
        Thread thread2 = new MainProgram("Fil 2");
        // Per a executar un fil, hem d'utilitzar el metode start()
        // El metode start crida al metode run automaticament
        thread1.start();
        thread2.start();
        System.out.println("End");
    }
}
