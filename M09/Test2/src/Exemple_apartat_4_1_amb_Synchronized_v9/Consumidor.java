package Exemple_apartat_4_1_amb_Synchronized_v9;



public class Consumidor implements Runnable{
	private Magatzem magatzem;
	private int id;
	
	

	public Consumidor(Magatzem magatzem, int id) {
		this.magatzem = magatzem;
		this.id = id;
	}



	@Override
	public void run() {
		if (id == 1) {
			magatzem.impressioAmbSyncronized_1();
		} else {
			magatzem.impressioAmbSyncronized_2();
		}
	}

}
