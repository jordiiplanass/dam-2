package Exemple_apartat_4_1_amb_Synchronized_v9;



public class Inici {

	public static void main(String[] args) {
		Magatzem magatzemCompartit = new Magatzem();
		
		Consumidor objRunnable1 = new Consumidor(magatzemCompartit, 1);
		Consumidor objRunnable2 = new Consumidor(magatzemCompartit, 2);
		Productor objRunnable3 = new Productor(magatzemCompartit, 1);
		Productor objRunnable4 = new Productor(magatzemCompartit, 2);
		
		Thread threadConsumidor_1 = new Thread(objRunnable1);
		Thread threadConsumidor_2 = new Thread(objRunnable2);
		Thread threadProductor_1 = new Thread(objRunnable3);
		Thread threadProductor_2 = new Thread(objRunnable4);
		
		threadConsumidor_1.setName("threadConsumidor_1");
		threadConsumidor_2.setName("threadConsumidor_2");
		threadProductor_1.setName("threadProductor_1");
		threadProductor_2.setName("threadProductor_2");
		
		System.out.println("main: INICI");
		
		threadConsumidor_1.start();
		threadConsumidor_2.start();
		threadProductor_1.start();
		threadProductor_2.start();
		
		try {
			threadConsumidor_1.join();
			threadConsumidor_2.join();
			threadProductor_1.join();
			threadProductor_2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("main: FI");
	}

}
