package Exemple_apartat_4_1_amb_Synchronized_v9;



public class Magatzem {
	
	public synchronized void impressioAmbSyncronized_1() {
		System.out.println(Thread.currentThread().getName() + ": INICI impressioAmbSyncronized_1() amb sleep(10000)" );
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName() + ": FI impressioAmbSyncronized_1() amb sleep(10000)" );
	}

	
	public synchronized void impressioAmbSyncronized_2() {
		System.out.println(Thread.currentThread().getName() + ": INICI impressioAmbSyncronized_2() amb sleep(5000)" );
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName() + ": FI impressioAmbSyncronized_2() amb sleep(5000)" );
	}
	

	public void impressioSenseSyncronized_1() {
		System.out.println(Thread.currentThread().getName() + ": impressioSenseSyncronized_1()" );
	}
	
	
	public void impressioSenseSyncronized_2() {
		System.out.println(Thread.currentThread().getName() + ": impressioSenseSyncronized_2()" );
	}	

}
