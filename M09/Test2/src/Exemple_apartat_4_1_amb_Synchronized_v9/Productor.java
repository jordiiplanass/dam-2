package Exemple_apartat_4_1_amb_Synchronized_v9;



public class Productor implements Runnable{
	private Magatzem magatzem;
	private int id;
	
	

	public Productor(Magatzem magatzem, int id) {
		this.magatzem = magatzem;
		this.id = id;
	}



	@Override
	public void run() {
		if (id == 1) {
			magatzem.impressioSenseSyncronized_1();
		} else {
			magatzem.impressioSenseSyncronized_2();
		}
	}

}
