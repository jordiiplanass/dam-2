package examen_2021_2022;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;



public class Drassana_Dades implements Runnable{
	public static final String nomDrassana = "MCRN Calisto "; 
	
	private LinkedList<Nau_Dades> llistaNausEnDrassana = new LinkedList<Nau_Dades>();		// Cua de tipus FIFO que suporti null's
	
	// La clau serà peça_ID. El valor serà la quantitat de peces que hi ha del propi model.
	private LinkedHashMap<String, Integer> mapaStockPeces = new LinkedHashMap<String, Integer>();
	
	// La clau serà peça_ID. El valor seran els objecte de tipus Peça_electronica_Dades i es diferenciaran pel peça_num_serie.
	private LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica = new LinkedHashMap<String, LinkedList<Peça_electronica_Dades>>();

	public Drassana_Dades(LinkedList<Nau_Dades> llistaNausEnDrassana, LinkedHashMap<String, Integer> mapaStockPeces, LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica) {
		this.llistaNausEnDrassana = llistaNausEnDrassana;
		this.mapaStockPeces = mapaStockPeces;
		this.mapaPecesElectronica = mapaPecesElectronica;
	}
	public Drassana_Dades() {
	}

	public LinkedList<Nau_Dades> getLlistaNausEnDrassana() {
		return llistaNausEnDrassana;
	}

	public void setLlistaNausEnDrassana(LinkedList<Nau_Dades> llistaNausEnDrassana) {
		this.llistaNausEnDrassana = llistaNausEnDrassana;
	}

	public LinkedHashMap<String, Integer> getMapaStockPeces() {
		return mapaStockPeces;
	}

	public void setMapaStockPeces(LinkedHashMap<String, Integer> mapaStockPeces) {
		this.mapaStockPeces = mapaStockPeces;
	}

	public LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronica() {
		return mapaPecesElectronica;
	}

	public void setMapaPecesElectronica(LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica) {
		this.mapaPecesElectronica = mapaPecesElectronica;
	}


	
	public void veureContingutMapaPecesElectronica() {
		LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> contingut = getMapaPecesElectronica();
		for (String key : contingut.keySet()) {
			System.out.println("\n\t" + key + ": " + contingut.get(key).size());
		}
	}


	@Override
	public void run() {
		//1
		System.out.println("Naus acabades: " + "magatzem.nausAcabades");
		//2
		veureContingutMapaPecesElectronica();
		//3
		for (String key : mapaPecesElectronica.keySet()){
			//magatzem.processarPecesElectronica(Peça_electronica_Dades mapaPecesElectronica.get(key));
		}
		//4
		/*
		he escrit un pseudocodi orientatiu, funcionaria si sapigues com obtenir el magatzem.
		boolean acabades = false;
		while (!acabades){
			try{
				sleep(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
			acabades = true;
			magatzem.nausAcabades.forEach(nau ->{
				if (!nau) acabades = false;
			});
			System.out.println(magatzem.nausAcabades);
		}

		 */
		//5
		veureContingutMapaPecesElectronica();
	}
}
