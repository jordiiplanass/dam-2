package examen_2021_2022;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;



public class Magatzem_peces_Dades extends Thread {
	// Aquí s'emmagatzemaran totes les peces electròniques que hi ha en les naus i en la drassana. 
	// Totes les naus i la drassana han d'enviar les seves peces a aquest magatzem.
	
	// És una llista inicialitzada per Drassana que contindrà tants false's com naus hi hagi en la drassana.
	private ArrayList <Boolean> nausAcabades = new ArrayList<Boolean>();
	
	// La clau serà peça_ID. El valor seran els objecte de tipus Peça_electronica_Dades i es diferenciaran pel peça_num_serie.
	private LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronicaOperatives = new LinkedHashMap<String, LinkedList<Peça_electronica_Dades>>();
	
	// La clau serà peça_ID. El valor seran els objecte de tipus Peça_electronica_Dades i es diferenciaran pel peça_num_serie.
	private LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronicaTrencades = new LinkedHashMap<String, LinkedList<Peça_electronica_Dades>>();
	
	private Semaphore treballadorsIntergalactics = new Semaphore(4);
	

	public ArrayList<Boolean> getNausAcabades() {
		return nausAcabades;
	}

	public LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronicaOperatives() {
		return mapaPecesElectronicaOperatives;
	}

	public void setMapaPecesElectronicaOperatives(LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica) {
		this.mapaPecesElectronicaOperatives = mapaPecesElectronica;
	}

	public LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronicaTrencades() {
		return mapaPecesElectronicaTrencades;
	}

	public void setMapaPecesElectronicaTrencades(
			LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronicaTrencades) {
		this.mapaPecesElectronicaTrencades = mapaPecesElectronicaTrencades;
	}


	
	
	
	public void notificaNauAcabada() {
		for (int i = 0 ; i < nausAcabades.size() ; i++){
			if (!nausAcabades.get(i)) {
				nausAcabades.set(i, true);
				break;
			}
		}
	}

	public void processarPecesElectronica(Peça_electronica_Dades peçaRebuda) {
		try{
			treballadorsIntergalactics.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (peçaRebuda.isPeça_trencada()){
			try{
				sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			mapaPecesElectronicaTrencades.put(peçaRebuda.getPeça_ID(), new LinkedList<Peça_electronica_Dades>(Arrays.asList(peçaRebuda)));
		} else {
			mapaPecesElectronicaOperatives.put(peçaRebuda.getPeça_ID(), new LinkedList<Peça_electronica_Dades>(Arrays.asList(peçaRebuda)));
		}

	}
	
	
	public void veureContingutDelMagatzem() {
		System.out.print("Contingut del magatzem:");
		for (String key : mapaPecesElectronicaOperatives.keySet()) {
			System.out.println("\n\t" + key + ": " + mapaPecesElectronicaOperatives.get(key).size());
		}
		for (String key : mapaPecesElectronicaTrencades.keySet()) {
			System.out.println("\n\t" + key + ": " + mapaPecesElectronicaTrencades.get(key).size());
		}
	}
	
}
