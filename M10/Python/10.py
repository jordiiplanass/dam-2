# 10. Escriu un programa que pregunti primer si es vol calcular 
# l'àrea d'un triangle o la d'un cercle. 
# Si es contesta que es vol calcular l'àrea d'un triangle, 
# el programa ha de demanar aleshores la base i l'alçada i 
# escriure l'àrea. Si es contesta que es vol calcular l'àrea del cercle, 
# el programa aleshores ha de demanar el radi i escriure l'àrea.

import math

text = "Vols calcular l'àrea un triangle o un cercle? \n\tTriangle --> 0\n\tCercle --> 1\n"
entrada = int(input(text))

if entrada != 1 and entrada != 0:
    print("Entra un valor valid")
    exit(1)

if entrada == 0:
    base = float(input("Base del triangle: "))
    alsada = float(input("Alçada del triangle: "))
    print("Area del triangle:", base * alsada / 2)

elif entrada == 1:
    radi = float(input("Radi del cercle:"))
    print("Area del cercle:", radi * 2 * math.pi)
