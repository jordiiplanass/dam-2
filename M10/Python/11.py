# Escriu un programa per repartir una quantitat d'euros en el nombre
# de bitllets i monedes corresponents, tenint en compte un algoritme
# que ens asseguri el menor nombre de bitllets/monedes (és a
# dir, sempre que puguem utilitzar un bitllet de 10€, no
# utilitzar-ne dos de 5€ ni cinc monedes de 2€)
# a. Suposa que no existeixen els bitllets majors de 50 € i que les quantitats
# introduïdes són enteres

entrada = int(input("Quants diners vols repartir? "))
bitllets50 = 0
bitllets20 = 0
bitllets10 = 0
bitllets5 = 0
moneda2 = 0
moneda1 = 0
while entrada > 0:
    if entrada >= 50:
        bitllets50 += 1
        entrada-=50 
    elif entrada >= 20:
        bitllets20 += 1
        entrada -= 20
    elif entrada >= 10:
        bitllets10 += 1
        entrada -= 10
    elif entrada >= 5:
        bitllets5 += 1
        entrada -= 5
    elif entrada >= 2:
        moneda2 += 1
        entrada -= 2
    else:
        moneda1 += 1
        entrada -= 1
print("Bitllets50: ", bitllets50, "\nBitllets20: ", bitllets20, "\nBitllets10: ",
    bitllets10, "\nBitllets5: ", bitllets5, "\nMonedes2: ", moneda2, "\nMoneda1: ", moneda1)
