# 12. Escriu un programa que llegeixi una data (dia, mes i any), determini si la data correspon
# a un valor vàlid i ho escrigui. Les dates seran tres dades de tipus enter que
# correspondran a un dia, un mes i un any (dd, mm, aa).

# a. Els mesos 1, 3, 5, 7, 8, 10 i 12 tenen 31 dies.

# b. Els mesos 4, 6, 9 i 11 tenen 30 dies.

# c. El mes 2 té 28 dies, excepte quan l'any és de traspàs, que té 29 dies.

# d. Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples de
# 4 però no de 100.

dia = int(input("Introdueix el dia: "))
mes = int(input("Introdueix el mes: "))
year = int(input("Introdueix l'any: "))
mesos31 = [1,3,5,7,8,10,12]
mesos30 = [4,6,9,11]
if mes in mesos31:
    if dia > 31 or dia <= 0:
        print("Data no valida")
        exit(1)
elif mes in mesos30:
    if dia > 30 or dia <= 0:
        print("Data no valida")
        exit(2)
elif mes == 2:
    if year % 4 and (year % 100 or not year % 400):
        if dia > 28 or dia <= 0 :
                print("Data no valida")
                exit(3)
    else:
        if dia > 29 or dia <= 0 :
                print("Data no valida")
                exit(4)
print("Data valida")
    
