# 13. Escriu un programa que demani dos nombres enters i escrigui la suma de tots els enters
# des del primer nombre fins al segon. Comprova que el segon nombre és més gran que
# el primer
num1 = int(input("Entra una xifra entera: "))
num2 = int(input("Entra una altre xifra entera: "))
sortida = 0
while num1 < num2:
    sortida += num1
    num1 += 1
print(sortida)