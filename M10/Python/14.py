# 14. Escriu un programa que demani un nombre enter major que 
# 0 i que calculi el seu factorial

num = int(input('Introdueix una xifra'))
if num > 0:
    factorial = 1
    while num > 0:
        factorial *= num
        num -= 1
print(factorial)
