# 15. Escriu un programa que mostri els múltiples de 7 
#  que hi ha entre els números del 0 al 100. 
# Fes-ho amb una estructura for.
for n in range(1,100):
    if n % 7 == 0:
        print(n)