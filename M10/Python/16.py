# 16. Escriu un programa que permeti crear una llista de paraules. El programa haurà de
# demanar un nombre i a continuació sol·licitar tantes paraules com hagi indicat l'usuari.
# Finalment mostrarà la llista creada
list = []
quantitatParaules = int(input("Quantes paraules vols emmagatzemar? "))
while quantitatParaules > 0:
    list.append(input("Introdueix una paraula: "))
    quantitatParaules -= 1
print(list)