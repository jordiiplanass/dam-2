# 17. Escriu un programa que permeti crear dues llistes de paraules i que, a continuació,
# elimini de la primera llista les paraules de la segona llista.

# En aquest cas he escrit el llistat a ma per a comoditat en la utilitat, si 
# volem introduir manualment les dades només hem de utilitzar el codi de l'exercici 16 
# 2 cops i guardar-ho en dues llistes diferents:

llista1 = ['paraula1','paraula2','paraula3','paraula4','paraula5','paraula6','paraula7']
llista2 = ['paraula8', 'paraula9','paraula10','paraula11','paraula12', 'paraula1']

print ( "Abans de l'execució", llista1)

for paraula in llista1:
    if paraula in llista2:
        llista1.remove(paraula)

print ("Després de l'execució", llista1)

