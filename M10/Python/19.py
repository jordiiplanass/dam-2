# 19. Escriu els nombres enters positius narcisistes de tres xifres.
for i in range(100, 999):
    numText = str(i)
    narcisista = 0;
    for num in numText:
        narcisista += pow(int(num), len(numText))
    if narcisista == i:
        print("El nombre",i,"es narcissista")
    