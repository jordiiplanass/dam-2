# 2. Escriu un programa que demani una distància en 
# peus i polzades i que escrigui aquesta distància 
# en centímetres. Recorda que un peu son 12 polsades i 
# una polzada són 2.54 cm

peus = float(input("Distancia en Peus: "))
polzades = float(input("Distancia en Polzades: "))
polzades += (peus*12) 
centimetres = polzades * 2.54
print(centimetres, " cm")