# 20. Donats dos nombres enters n1 i n2 amb n1 < n2, 
# escriu per pantalla tots els nombres
# enters dins l’interval [n1, n2] en ordre creixent.
n1 = int(input("n1: "))
n2 = int(input("n2: "))
if n1 < n2:
    for i in range(n1, n2+1):
        print(i)