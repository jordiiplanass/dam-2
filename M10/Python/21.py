# 21. Donats dos nombre enters n1 i n2 amb n1 < n2 , escriu tots els nombres enters dins
# l’interval [n1, n2] en ordre decreixent.
n1 = int(input("n1: "))
n2 = int(input("n2: "))
if n1 < n2:
    for i in range(n2, n1-1, -1):
        print(i)