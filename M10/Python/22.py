# 22. Donats dos nombre enters n1 i n2 amb n1 < n2, escriu 
# les arrels quadrades dels nombres enters dins l'interval 
# [n1, n2] en ordre creixent.
from cmath import sqrt

n1 = int(input("n1: "))
n2 = int(input("n2: "))
if n1 < n2:
    for i in range(n1, n2+1):
        print(i,": ", sqrt(i))
