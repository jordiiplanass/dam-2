# 23. Donats dos nombre enters n1 i n2 amb n1 < n2, 
# escriu els nombres enters parells que hi ha dins 
# l’interval [n1, n2] en ordre creixent. 
# El nombre zero es considera parell.
n1 = int(input("n1: "))
n2 = int(input("n2: "))
if n1 < n2:
    for i in range(n1, n2+1):
        if i % 2 == 0:
            print(i)