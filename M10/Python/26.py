# 26. Escriu un programa que donada una paraula ens digui si és “alfabètica”. Una paraula
# és “alfabètica” si totes les seves lletres estan ordenades alfabèticament.
# Per exemple, “amor“, “ceps“ e “himno“ són paraules “alfabètiques”.
paraulaEntrada = input("Introdueix una paraula: ")
paraulaOrdenada = sorted(paraulaEntrada)
paraulaEntrada = list(paraulaEntrada)
if paraulaOrdenada == paraulaEntrada:
    print("Es una paraula alfabètica")
else:
    print("No es una paraula alfabètica")
