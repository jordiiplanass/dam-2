# 27. Escriu un programa que donada una cadena ens digui si és un palíndrom o no. Una
# frase o cadena és palíndroma si es pot llegir en ordre o a l'inrevés obtenint la mateixa
# frase, per exemple: "dabale arroz a la zorra el abad", és palíndroma.

paraulaEntrada = input("Introdueix una paraula: ")
paraulaEntrada = list(paraulaEntrada)
paraulaEntrada.remove(' ')
auxiliar = paraulaEntrada
paraulaEntrada.reverse()
if paraulaEntrada == auxiliar:
    print("Es palíndroma")
else:
    print("No es palíndroma")
