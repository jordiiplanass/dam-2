# 28. Escriu un programa que donat un DNI, el verifiqui comprovant que la lletra és correcta.
# L’última lletra del DNI es pot calcular a partir dels seus números. Per a això només has
# de dividir el nombre per 23 i quedar-te amb la resta. La resta és un nombre entre 0 i 22.
# La lletra que correspon a cada nombre la tens en aquesta taula:
# -------------------------------------------------- ------------
# 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
# T R W A G M I F P D X B N J Z S Q V H L C K E
# -------------------------------------------------- ------------
# (Nota: una implementació basada en prendre una decisions amb if -elif condueix a un
# programa “molt” llarg. Si fas servir l'operador d'indexació de cadenes, el programa tot
# just ocupa 3 línies)
numDNI = int(input("Introdueix els numeros del teu DNI: "))
letrasDNI = ['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E']
print("Letra: ", letrasDNI[numDNI%23]) # 3 lineas 😎