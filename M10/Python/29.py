# 29. Escriu un programa que donat un fitxer de text d'entrada, el llegeix i mostra la llista de
# paraules ordenades alfabèticament. Les paraules no poden estar repetides, i no han de
# ser keysensitive. És a dir, a la llista només hauria d’aparèixer un cop la paraula “hola” si
# en el fitxer posés “Hola, hola, hoLA ...”
paraules = []
aaa = ""
with open ("paraules.txt") as fitxer:
    for line in fitxer:
        words = line.strip().split(' ')
        for word in words:
            if word.upper() not in map(str.upper, paraules):
                paraules.append(word)
print (sorted(paraules))