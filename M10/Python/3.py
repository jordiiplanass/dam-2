# 3. Escriu un programa que demani una temperatura 
# en graus Celsius i que escrigui la temperatura en 
# graus Fahrenheit. Recorda que la relació entre 
# graus Celsius (C) i Fahrenheit (F) és la següent 
# F – 32 = 1,8 * C

celsius = float(input("Temperatura en Celsius(C): "))
fahrenheit = (celsius * 1.8) + 32
print (fahrenheit, " F")