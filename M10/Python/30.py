# 30. Escriu un programa que donada una paraula escrigui per pantalla tots els seus
# anagrames. Un anagrama és el resultat de construir una nova paraula a partir de la
# reordenació de les lletres d’una paraula.
# paraula = input("Escriu una paraula: ")
import itertools
paraula = input("Introdueix una paraula: ")
perms = sorted(set(["".join(perm) for perm in itertools.permutations(paraula)]))
print(perms)