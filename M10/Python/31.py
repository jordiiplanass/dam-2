# 31. La criba d’Eratóstenes. És un mètode per trobar els nombres primers menors que un
# número donat.
xifra = int(input("Introdueix una xifra: "))
llista = [x+1 for x in range(1, xifra)]
cosins = []

for i in range(len(llista)):
    if not llista[i] == 0:
        cosins.append(llista[i])
        aux = llista[i]
        for j in range(len(llista)):
            if llista[j] % aux == 0:
                llista[j] = 0
print(cosins)