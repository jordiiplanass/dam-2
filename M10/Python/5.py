#5. Escriu un programa que creï una llista amb les vocals.

vocals = ['a', 'e', 'i', 'o', 'u']

#   a. Que demani a l'usuari quin número 
#       d’element de la llista vol modificar i amb quin 
#       valor, i mostri per pantalla la llista modificada.

numElement = int(input("Quin valor vols modificar de la llista? "))
if numElement > len(vocals) or numElement < 0:
    print("Entra un valor valid")
    exit(1)
valorElement = input("Quin valor li vols donar? ")

vocals[numElement] = valorElement
print(vocals)

#   b. Després que demani quina lletra vol modificar i 
#       amb quin valor, i mostri per pantalla la 
#       llista modificada

lletraElement = input("Quina lletra vols modificar? ")
if not lletraElement in vocals:
    print("Entra un valor valid")
    exit(2)

valorElement = input("Quin valor li vols donar? ")
for valor in vocals:
    if valor == lletraElement:
        vocals[vocals.index(valor)] = valorElement
print (vocals)
