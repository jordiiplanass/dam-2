# 6. Escriu un programa que mostri la taula de 
# multiplicar del 7 a partir d'una llista. 
# Cal que definiu una llista en la que es 
# guardin els valors de la taula de multiplicar

taulaDeMultiplicar = [7*x for x in range(11)]
# es 11 perque el primer valor es 0 i hem de saber els 10 proxims
print(taulaDeMultiplicar)