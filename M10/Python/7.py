# 7. Escriu un programa amb un diccionari 
# que relacioni el nom de 4 alumnes i la 
# nota que han obtingut en un exercici. 

alumne1 = {"Nom" : "Eva", "Nota" : 5}
alumne2 = {"Nom" : "Pere", "Nota" : 7}
alumne3 = {"Nom" : "Joan", "Nota" : 10}
alumne4 = {"Nom" : "Manel", "Nota" : 6}

classe = [alumne1, alumne2, alumne3, alumne4]
#   a. Que demani a l’usuari de quin alumne 
#   vol saber la nota, i que mostri 
#   el resultat per pantalla.

alumneCerca = input("De quin alumne vols modificar la nota? ")
for alumne in classe:
    if alumne["Nom"] == alumneCerca:
        print("La nota del alumne", alumne["Nom"], "es:", alumne["Nota"])
