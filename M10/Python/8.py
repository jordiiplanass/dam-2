# 8. Escriu un programa en el que donades dues llistes 
# de paraules (definides internament en el programa i 
# sense repeticions) escrigui les següents llistes 
# (sense repeticions)

llistaParaules1 = ["avena", "boca", "cera", "dit"]
llistaParaules2 = ["elefant", "formiga", "gegant", "helicopter"]

#  - Llista de paraules que apareixen tant en la primera llista com en la segona
#    (intersecció)

llistaInterseccio = []

for paraula1 in llistaParaules1:
    for paraula2 in llistaParaules2:
        if paraula1 == paraula2:
            llistaInterseccio.append(paraula1)

print (llistaInterseccio)

#  - Llista de paraules que apareixen en la primera llista, però no en la segona
#    (diferència)

llistaDiferencia1 = []

for paraula1 in llistaParaules1:
    if not paraula1 in llistaParaules2:
            llistaDiferencia1.append(paraula1)

print(llistaDiferencia1)

#  - Llista de paraules que apareixen en la segona llista, però no en la primera

llistaDiferencia2 = []
for paraula2 in llistaParaules2:
    if not paraula2 in llistaParaules1:
            llistaDiferencia2.append(paraula2)

print(llistaDiferencia2)

#  - Llista de paraules que apareixen en alguna de las dues llistes (unió)

llistaUnio = []
for paraula1 in llistaParaules1:
    llistaUnio.append(paraula1)
for paraula2 in llistaParaules2:
    llistaUnio.append(paraula2)

print(llistaUnio)