# 9. Escriu un programa que demani dos nombres 
# enters i que calculi la seva divisió, escrivint 
# si la divisió és exacta o no. Has de controlar 
# el cas que indiqui que no es pot
# dividir per 0

xifra1 = int(input("Introdueix una xifra "))
xifra2 = int(input("Introdueix una altre xifra "))

if xifra1 == 0 or xifra2 == 0:
    print("Introdueix valors valids")
    exit(1)
if xifra1 % xifra2 == 0:
    print ("La divisio dels dos nombres es exacta")
else:
    print ("La divisio dels dos nombres no es exacta")