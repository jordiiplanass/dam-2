'''
    Jordi Planas Martinez
    Escola del treball
    Mòdul: M10 Sistemes de Gestió Empresarial
    UF2 PR1. Entrega exercicis python
'''
import math
import itertools
import requests
from bs4 import BeautifulSoup


menuText = """Quin exercici vols probar? 
        1...31 ó 33
        """
menuOption = int(input(menuText))
if menuOption == 32 or menuOption > 33 or menuOption < 1:
    print("Introdueix un valor vàlid en el menu")
    exit(1)
textSortida = ""
if menuOption == 1:
    textSortida = """
    1. Escriu un programa que demani dos nombres 
        i que retorni la seva mitjana aritmètica.
    """
    print(textSortida)

    num1 = int(input("Entra una xifra: "))
    num2 = int(input("Entra una altre xifra: "))
    average = ((num1 + num2) / 2)
    print("Mitjana: ",average)

elif menuOption == 2:
    textSortida = """
    2. Escriu un programa que demani una distància en 
        peus i polzades i que escrigui aquesta distància 
        en centímetres. Recorda que un peu son 12 polsades i 
        una polzada són 2.54 cm
    """
    print(textSortida)

    peus = float(input("Distancia en Peus: "))
    polzades = float(input("Distancia en Polzades: "))
    polzades += (peus*12) 
    centimetres = polzades * 2.54
    print(centimetres, " cm")

elif menuOption == 3:
    textSortida = """
    3. Escriu un programa que demani una temperatura 
        en graus Celsius i que escrigui la temperatura en 
        graus Fahrenheit. Recorda que la relació entre 
        graus Celsius (C) i Fahrenheit (F) és la següent 
        F – 32 = 1,8 * C
    """
    print(textSortida)

    celsius = float(input("Temperatura en Celsius(C): "))
    fahrenheit = (celsius * 1.8) + 32
    print (fahrenheit, " Fº")

elif menuOption == 4:
    textSortida = """
    4. Escriu un programa que demani una quantitat 
        de segons i que escrigui quants minuts i segons són.
    """
    print(textSortida)
    
    segons = int(input("Segons per passar a minuts:segons : "))
    minuts = segons//60
    segons %= 60
    print(minuts, ":", segons)

elif menuOption == 5:
    textSortida = """
    5. Escriu un programa que creï una llista amb les vocals.
        a. Que demani a l'usuari quin número 
            d’element de la llista vol modificar i amb quin 
            valor, i mostri per pantalla la llista modificada.
        b. Després que demani quina lletra vol modificar i 
            amb quin valor, i mostri per pantalla la 
            llista modificada
    """
    print(textSortida)

    vocals = ['a', 'e', 'i', 'o', 'u']

    numElement = int(input("Quin valor vols modificar de la llista? "))
    if numElement > len(vocals) or numElement < 0:
        print("Entra un valor valid")
        exit(1)
    valorElement = input("Quin valor li vols donar? ")

    vocals[numElement] = valorElement
    print(vocals)

    lletraElement = input("Quina lletra vols modificar? ")
    if not lletraElement in vocals:
        print("Entra un valor valid")
        exit(2)

    valorElement = input("Quin valor li vols donar? ")
    for valor in vocals:
        if valor == lletraElement:
            vocals[vocals.index(valor)] = valorElement
    print (vocals)

elif menuOption == 6:
    textSortida = """
    6. Escriu un programa que mostri la taula de 
        multiplicar del 7 a partir d'una llista. 
        Cal que definiu una llista en la que es 
        guardin els valors de la taula de multiplicar
    """
    print(textSortida)

    taulaDeMultiplicar = [7*x for x in range(11)]
    # es 11 perque el primer valor es 0 i hem de saber els 10 proxims
    print(taulaDeMultiplicar)

elif menuOption == 7:
    textSortida = """
    7. Escriu un programa amb un diccionari 
        que relacioni el nom de 4 alumnes i la 
        nota que han obtingut en un exercici. 
            
        a. Que demani a l’usuari de quin alumne 
            vol saber la nota, i que mostri 
            el resultat per pantalla.
    """
    print(textSortida)
    alumne1 = {"Nom" : "Eva", "Nota" : 5}
    alumne2 = {"Nom" : "Pere", "Nota" : 7}
    alumne3 = {"Nom" : "Joan", "Nota" : 10}
    alumne4 = {"Nom" : "Manel", "Nota" : 6}

    classe = [alumne1, alumne2, alumne3, alumne4]
    alumneCerca = input("De quin alumne vols modificar la nota? ")
    for alumne in classe:
        if alumne["Nom"] == alumneCerca:
            print("La nota del alumne", alumne["Nom"], "es:", alumne["Nota"])

elif menuOption == 8:
    textSortida = """
    8. Escriu un programa en el que donades dues llistes 
        de paraules (definides internament en el programa i 
        sense repeticions) escrigui les següents llistes 
        (sense repeticions)
    """
    print(textSortida)

    llistaParaules1 = ["avena", "boca", "cera", "dit"]
    llistaParaules2 = ["elefant", "formiga", "gegant", "helicopter"]

    llistaInterseccio = []

    for paraula1 in llistaParaules1:
        for paraula2 in llistaParaules2:
            if paraula1 == paraula2:
                llistaInterseccio.append(paraula1)

    print ("Llista Interseccio: ",llistaInterseccio)

    llistaDiferencia1 = []

    for paraula1 in llistaParaules1:
        if not paraula1 in llistaParaules2:
                llistaDiferencia1.append(paraula1)

    print("Llista diferencia: ",llistaDiferencia1)

    llistaDiferencia2 = []
    for paraula2 in llistaParaules2:
        if not paraula2 in llistaParaules1:
                llistaDiferencia2.append(paraula2)

    print("Llista Diferencia2: ", llistaDiferencia2)

    llistaUnio = []
    for paraula1 in llistaParaules1:
        llistaUnio.append(paraula1)
    for paraula2 in llistaParaules2:
        llistaUnio.append(paraula2)

    print("Llista unio: ",llistaUnio)

elif menuOption == 9:
    textSortida = """
    9. Escriu un programa que demani dos nombres 
        enters i que calculi la seva divisió, escrivint 
        si la divisió és exacta o no. Has de controlar 
        el cas que indiqui que no es pot
        dividir per 0
    """
    print(textSortida)

    xifra1 = int(input("Introdueix una xifra "))
    xifra2 = int(input("Introdueix una altre xifra "))

    if xifra1 == 0 or xifra2 == 0:
        print("Introdueix valors valids")
        exit(1)
    if xifra1 % xifra2 == 0:
        print ("La divisio dels dos nombres es exacta")
    else:
        print ("La divisio dels dos nombres no es exacta")

elif menuOption == 10:
    textSortida = """
    10. Escriu un programa que pregunti primer si es vol calcular 
        l'àrea d'un triangle o la d'un cercle. 
        Si es contesta que es vol calcular l'àrea d'un triangle, 
        el programa ha de demanar aleshores la base i l'alçada i 
        escriure l'àrea. Si es contesta que es vol calcular l'àrea del cercle, 
        el programa aleshores ha de demanar el radi i escriure l'àrea.
    """
    print(textSortida)

    text = "Vols calcular l'àrea un triangle o un cercle? \n\tTriangle --> 0\n\tCercle --> 1\n"
    entrada = int(input(text))

    if entrada != 1 and entrada != 0:
        print("Entra un valor valid")
        exit(1)

    if entrada == 0:
        base = float(input("Base del triangle: "))
        alsada = float(input("Alçada del triangle: "))
        print("Area del triangle:", base * alsada / 2)

    elif entrada == 1:
        radi = float(input("Radi del cercle:"))
        print("Area del cercle:", radi * 2 * math.pi)

elif menuOption == 11:
    textSortida = """
    11 Escriu un programa per repartir una quantitat d'euros en el nombre
        de bitllets i monedes corresponents, tenint en compte un algoritme
        que ens asseguri el menor nombre de bitllets/monedes (és a
        dir, sempre que puguem utilitzar un bitllet de 10€, no
        utilitzar-ne dos de 5€ ni cinc monedes de 2€)
            a. Suposa que no existeixen els bitllets majors de 50 € i que les quantitats
                introduïdes són enteres
    """
    print(textSortida)

    entrada = int(input("Quants diners vols repartir? "))
    bitllets50 = 0
    bitllets20 = 0
    bitllets10 = 0
    bitllets5 = 0
    moneda2 = 0
    moneda1 = 0
    while entrada > 0:
        if entrada >= 50:
            bitllets50 += 1
            entrada-=50 
        elif entrada >= 20:
            bitllets20 += 1
            entrada -= 20
        elif entrada >= 10:
            bitllets10 += 1
            entrada -= 10
        elif entrada >= 5:
            bitllets5 += 1
            entrada -= 5
        elif entrada >= 2:
            moneda2 += 1
            entrada -= 2
        else:
            moneda1 += 1
            entrada -= 1
    print("Bitllets50: ", bitllets50, "\nBitllets20: ", bitllets20, "\nBitllets10: ",
        bitllets10, "\nBitllets5: ", bitllets5, "\nMonedes2: ", moneda2, "\nMoneda1: ", moneda1)

elif menuOption == 12:
    textSortida = """
    12. Escriu un programa que llegeixi una data (dia, mes i any), determini si la data correspon
        a un valor vàlid i ho escrigui. Les dates seran tres dades de tipus enter que
        correspondran a un dia, un mes i un any (dd, mm, aa).
            a. Els mesos 1, 3, 5, 7, 8, 10 i 12 tenen 31 dies.

            b. Els mesos 4, 6, 9 i 11 tenen 30 dies.

            c. El mes 2 té 28 dies, excepte quan l'any és de traspàs, que té 29 dies.

            d. Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples de
                4 però no de 100.
    """
    print(textSortida)
    
    dia = int(input("Introdueix el dia: "))
    mes = int(input("Introdueix el mes: "))
    year = int(input("Introdueix l'any: "))
    mesos31 = [1,3,5,7,8,10,12]
    mesos30 = [4,6,9,11]
    if mes in mesos31:
        if dia > 31 or dia <= 0:
            print("Data no valida")
            exit(1)
    elif mes in mesos30:
        if dia > 30 or dia <= 0:
            print("Data no valida")
            exit(2)
    elif mes == 2:
        if year % 4 and (year % 100 or not year % 400):
            if dia > 28 or dia <= 0 :
                    print("Data no valida")
                    exit(3)
        else:
            if dia > 29 or dia <= 0 :
                    print("Data no valida")
                    exit(4)
    print("Data valida")

elif menuOption == 13:
    textSortida = """
    13. Escriu un programa que demani dos nombres enters i escrigui la suma de tots els enters
        des del primer nombre fins al segon. Comprova que el segon nombre és més gran que
        el primer
    """
    print(textSortida)
    num1 = int(input("Entra una xifra entera: "))
    num2 = int(input("Entra una altre xifra entera: "))
    sortida = 0
    while num1 < num2:
        sortida += num1
        num1 += 1
    print(sortida)
elif menuOption == 14:
    textSortida = """
    14. Escriu un programa que demani un nombre enter major que 
        0 i que calculi el seu factorial
    """
    print(textSortida)
    num = int(input('Introdueix una xifra'))
    if num > 0:
        factorial = 1
        while num > 0:
            factorial *= num
            num -= 1
    print(factorial)

elif menuOption == 15:
    textSortida = """
    15. Escriu un programa que mostri els múltiples de 7 
        que hi ha entre els números del 0 al 100. 
        Fes-ho amb una estructura for.
    """
    print(textSortida)

    for n in range(1,100):
        if n % 7 == 0:
            print(n)
    
elif menuOption == 16:
    textSortida = """
    16. Escriu un programa que permeti crear una llista de paraules. El programa haurà de
        demanar un nombre i a continuació sol·licitar tantes paraules com hagi indicat l'usuari.
        Finalment mostrarà la llista creada
    """
    print(textSortida)

    list = []
    quantitatParaules = int(input("Quantes paraules vols emmagatzemar? "))
    while quantitatParaules > 0:
        list.append(input("Introdueix una paraula: "))
        quantitatParaules -= 1
    print(list)

elif menuOption == 17:
    textSortida = """
    17. Escriu un programa que permeti crear dues llistes de paraules i que, a continuació,
        elimini de la primera llista les paraules de la segona llista.

    En aquest cas he escrit el llistat a ma per a comoditat en la utilitat, si 
        volem introduir manualment les dades només hem de utilitzar el codi de l'exercici 16 
        2 cops i guardar-ho en dues llistes diferents:
    """
    print(textSortida)

    llista1 = ['paraula1','paraula2','paraula3','paraula4','paraula5','paraula6','paraula7']
    llista2 = ['paraula8', 'paraula9','paraula10','paraula11','paraula12', 'paraula1']

    print ( "Abans de l'execució", llista1)

    for paraula in llista1:
        if paraula in llista2:
            llista1.remove(paraula)

    print ("Després de l'execució", llista1)


elif menuOption == 18:
    textSortida = """
    18. Escriu un programa que donat un número comprovi si és primer. (for/else)
    """
    print(textSortida)

    numEntrada = int(input("Entra una xifra per conprovar si es primera: "))
    for i in range(2, numEntrada):
        if numEntrada % i == 0:
            print("No es una xifra entera")
            exit(1)
    print("Es una xifra entera")
    
elif menuOption == 19:
    textSortida = """
    19. Escriu els nombres enters positius narcisistes de tres xifres.
    """
    print(textSortida)

    for i in range(100, 999):
        numText = str(i)
        narcisista = 0;
        for num in numText:
            narcisista += pow(int(num), len(numText))
        if narcisista == i:
            print("El nombre",i,"es narcissista")

elif menuOption == 20:
    textSortida = """
    20. Donats dos nombres enters n1 i n2 amb n1 < n2, 
        escriu per pantalla tots els nombres
        enters dins l’interval [n1, n2] en ordre creixent.
    """
    print(textSortida)
    n1 = int(input("n1: "))
    n2 = int(input("n2: "))
    if n1 < n2:
        for i in range(n1, n2+1):
            print(i)

elif menuOption == 21:
    textSortida = """
    21. Donats dos nombre enters n1 i n2 amb n1 < n2 , escriu tots els nombres enters dins
        l’interval [n1, n2] en ordre decreixent.
    """
    n1 = int(input("n1: "))
    n2 = int(input("n2: "))
    if n1 < n2:
        for i in range(n2, n1-1, -1):
            print(i)

elif menuOption == 22:
    textSortida = """
    22. Donats dos nombre enters n1 i n2 amb n1 < n2, escriu 
        les arrels quadrades dels nombres enters dins l'interval 
        [n1, n2] en ordre creixent.
    """
    print(textSortida)
    n1 = int(input("n1: "))
    n2 = int(input("n2: "))
    if n1 < n2:
        for i in range(n1, n2+1):
            print(i,": ", math.sqrt(i))

elif menuOption == 23:
    textSortida = """
    23. Donats dos nombre enters n1 i n2 amb n1 < n2, 
        escriu els nombres enters parells que hi ha dins 
        l’interval [n1, n2] en ordre creixent. 
    El nombre zero es considera parell.
    """
    print(textSortida)
    n1 = int(input("n1: "))
    n2 = int(input("n2: "))
    if n1 < n2:
        for i in range(n1, n2+1):
            if i % 2 == 0:
                print(i)

elif menuOption == 24:
    textSortida = """
    24. Escriure els 10 primers múltiples de 2
    """
    print(textSortida)
    multiplesDeDos = []
    for i in range(1,11):
        multiplesDeDos.append(2*i)
    print(multiplesDeDos)

elif menuOption == 25:
    textSortida = """
    25. Escriure els n primers múltiples de m
    """
    print(textSortida)
    multiplesDeDos = []
    n = int(input("n: "))
    m = int(input("m: "))

    for i in range(1,n + 1):
        multiplesDeDos.append(m*i)
    print(multiplesDeDos)

elif menuOption == 26:
    textSortida = """
    26. Escriu un programa que donada una paraula ens digui si és “alfabètica”. Una paraula
        és “alfabètica” si totes les seves lletres estan ordenades alfabèticament.
        Per exemple, “amor“, “ceps“ e “himno“ són paraules “alfabètiques”.
    """
    print(textSortida)
    paraulaEntrada = input("Introdueix una paraula: ")
    paraulaOrdenada = sorted(paraulaEntrada)
    paraulaEntrada = list(paraulaEntrada)
    if paraulaOrdenada == paraulaEntrada:
        print("Es una paraula alfabètica")
    else:
        print("No es una paraula alfabètica")

elif menuOption == 27:
    textSortida = """
    27. Escriu un programa que donada una cadena ens digui si és un palíndrom o no. Una
        frase o cadena és palíndroma si es pot llegir en ordre o a l'inrevés obtenint la mateixa
        frase, per exemple: "dabale arroz a la zorra el abad", és palíndroma.
    """
    print(textSortida)
    paraulaEntrada = input("Introdueix una paraula: ")
    paraulaEntrada = list(paraulaEntrada)
    paraulaEntrada.remove(' ')
    auxiliar = paraulaEntrada
    paraulaEntrada.reverse()
    if paraulaEntrada == auxiliar:
        print("Es palíndroma")
    else:
        print("No es palíndroma")

elif menuOption == 28:
    textSortida = """
    28. Escriu un programa que donat un DNI, el verifiqui comprovant que la lletra és correcta.
        L’última lletra del DNI es pot calcular a partir dels seus números. Per a això només has
        de dividir el nombre per 23 i quedar-te amb la resta. La resta és un nombre entre 0 i 22.
    La lletra que correspon a cada nombre la tens en aquesta taula:
    -------------------------------------------------- ------------
    0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
    T R W A G M I F P D X B N J Z S Q V H L C K E
    -------------------------------------------------- ------------
    (Nota: una implementació basada en prendre una decisions amb if -elif condueix a un
    programa “molt” llarg. Si fas servir l'operador d'indexació de cadenes, el programa tot
    just ocupa 3 línies)
    """
    print(textSortida)
    numDNI = int(input("Introdueix els numeros del teu DNI: "))
    letrasDNI = ['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E']
    print("Letra: ", letrasDNI[numDNI%23]) # 3 lineas 😎

elif menuOption == 29:
    textSortida = """
    29. Escriu un programa que donat un fitxer de text d'entrada, el llegeix i mostra la llista de
        paraules ordenades alfabèticament. Les paraules no poden estar repetides, i no han de
        ser keysensitive. És a dir, a la llista només hauria d’aparèixer un cop la paraula “hola” si
        en el fitxer posés “Hola, hola, hoLA ...”
    """
    print(textSortida)
    paraules = []
    aaa = ""
    with open ("paraules.txt") as fitxer:
        for line in fitxer:
            words = line.strip().split(' ')
            for word in words:
                if word.upper() not in map(str.upper, paraules):
                    paraules.append(word)
    print (sorted(paraules))

elif menuOption == 30:
    textSortida = """
    30. Escriu un programa que donada una paraula escrigui per pantalla tots els seus
        anagrames. Un anagrama és el resultat de construir una nova paraula a partir de la
        reordenació de les lletres d’una paraula.
        paraula = input("Escriu una paraula: ")
    """
    print(textSortida)
    paraula = input("Introdueix una paraula: ")
    perms = sorted(set(["".join(perm) for perm in itertools.permutations(paraula)]))
    print(perms)

elif menuOption == 31:
    textSortida = """
    31. La criba d’Eratóstenes. És un mètode per trobar els nombres primers menors que un
        número donat.
    """
    print(textSortida)
    xifra = int(input("Introdueix una xifra: "))
    llista = [x+1 for x in range(1, xifra)]
    cosins = []

    for i in range(len(llista)):
        if not llista[i] == 0:
            cosins.append(llista[i])
            aux = llista[i]
            for j in range(len(llista)):
                if llista[j] % aux == 0:
                    llista[j] = 0
    print(cosins)

elif menuOption == 33:
    textSortida = """
    33. Mòduls urllib/requests i Beautiful Soup.
        
        Escriu un programa que es connecti a la pàgina web https://isitchristmas.com/ i parsegi
        el contingut de la pàgina per saber si avui és el dia de Nadal.
        Podeu fer servir els mòduls urllib o requests.
        Podeu parsejar la pàgina “a mà” o podeu fer servir el mòdul BeautifulSoup…
    """
    print(textSortida)
    url = 'https://isitchristmas.com/'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    sortida = soup.find(id = 'answer')
    print (sortida.text)

else: 
    print("Error: wrong data")
    