from menu import *
from llibreta import *
import menu

llibreta_clients = llibreta.Llibreta
my_menu = menu.Menu(llibreta_clients)

opcio = 0

while opcio != 5:
    opcio = menu.mostrar_menu_principal()
    if opcio == 1:
        nom = input("Introdueix un nom: ")
        cognom = input("Introdueix un cognom: ")
        telefon = input("Introdueix un telefon: ")
        correu = input("Introdueix un correu: ")
        adreça = input("Introdueix un adreça: ")
        ciutat = input("Introdueix un ciutat: ")
        my_menu.llibreta.afegir_client(my_menu.llibreta, nom, cognom, telefon, correu, adreça, ciutat)
    elif opcio == 2:
        identificador = input("Introdueix l'identificador del client que vols eliminar: ")
        for client in my_menu.llibreta.cercar_per_id(my_menu.llibreta, identificador):
                    print("Deleted",client.nom, "successfully")
        my_menu.llibreta.eliminar_client(my_menu.llibreta, identificador)
    elif opcio == 3:
        opcio2 = 0
        while opcio2 != 6:
            opcio2 = menu.mostrar_menu_consulta()
            if opcio2 == 1:
                identificador = input("Introdueix l'identificador del client que vols consultar: ")
                for client in my_menu.llibreta.cercar_per_id(my_menu.llibreta, identificador):
                    print(client, "\n")
            elif opcio2 == 2:
                identificador = input("Introdueix el nom del client que vols consultar: ")
                for client in my_menu.llibreta.cercar_per_nom(my_menu.llibreta, identificador):
                    print(client, "\n")
            elif opcio2 == 3:
                identificador = input("Introdueix el cognom del client que vols consultar: ")
                for client in my_menu.llibreta.cercar_per_cognom(my_menu.llibreta, identificador):
                    print(client, "\n")
            elif opcio2 == 4:
                llibreta_clients.get_llista_clients(my_menu.llibreta)
            elif opcio2 == 5:
                for client in sorted(my_menu.llibreta.llista_clients, key=lambda x: x.nom):
                    print(client)
                print()  
    elif opcio == 4:
        identificador = input("Introdueix l'identificador del client que vols eliminar: ")
        if client in my_menu.llibreta.cercar_per_id(my_menu.llibreta, identificador):
            surtBucle = False
            while not surtBucle:
                print("\nQuin paràmetre vols modificar del client", client.identificador)
                entrada = int(input("""
    1. Nom:
    2. Cognom
    3. Telefon
    4. Correu
    5. Adreça
    6. Ciutat
    7. Sortir
    
Enter an option:"""))
                if entrada == 1:
                    nom = input("Introdueix un nou nom: ")
                    client.nom = nom
                elif entrada == 2:
                    cognom = input("Introdueix un nou cognom: ")
                    client.cognom = cognom
                elif entrada == 3:
                    telefon = input("Introdueix un nou telefon: ")
                    client.telefon = telefon
                elif entrada == 4:
                    correu = input("Introdueix un nou correu: ")
                    client.correu = correu
                elif entrada == 5:
                    adreça = input("Introdueix un nou adreça: ")
                    client.adreça = adreça
                elif entrada == 6:
                    ciutat = input("Introdueix un nou ciutat: ")
                    client.ciutat = ciutat
                elif entrada == 7:
                    surtBucle = True
