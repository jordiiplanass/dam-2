class Client:
    identificador = ""
    nom = ""
    cognom = ""
    telefon = ""
    correu = ""
    adreça = ""
    ciutat = ""
    def __init__(self, identificador, nom, cognom, telefon, correu, adreça, ciutat): #el constructor
        self.identificador = identificador
        self.nom = nom
        self.cognom = cognom
        self.telefon = telefon
        self.correu = correu
        self.adreça = adreça
        self.ciutat = ciutat
    def __str__(self): 
        sortida = "Identificador: " + str(self.identificador) + "\nNom: " + self.nom + "\tCognom: " + self.cognom + "\tTelefon: " + self.telefon + "\tCorreu: " + self.correu + "\tAdreça: " + self.adreça + "\tCiutat: " + self.ciutat
        return sortida

