import uuid
import client
class Llibreta:
    llista_clients = []
    id_client = 0

    def __init__(self):
        self.llista_clients = []
        self.id_client = ""

    def __init__(self,llista_clients):
        self.llista_clients = llista_clients

    def __init__(self,llista_clients,id_client):
        self.llista_clients = llista_clients
        self.id_client = id_client

    def get_llista_clients(self):
        for client in self.llista_clients:
            print(client)

    def afegir_client(self, nom, cognom, telefon, correu, adreça, ciutat): 
        identificador = uuid.uuid4()
        clientNou = client.Client(identificador, nom, cognom, telefon, correu, adreça, ciutat)
        self.llista_clients.append(clientNou)

    def eliminar_client(self, identificador):
        for client in self.llista_clients:
            if str(client.identificador) == identificador:
                self.llista_clients.remove(client)

    def cercar_per_id(self, identificador):
        sortida = []
        for client in self.llista_clients:
            if str(client.identificador) == identificador:
                sortida.append(client)
        return sortida

    def cercar_per_nom(self, nom):
        sortida = []
        for client in self.llista_clients:
            if client.nom == nom:
                sortida.append(client)
        return sortida

    def cercar_per_cognom(self, cognom):
        sortida = []
        for client in self.llista_clients:
            if client.cognom == cognom:
                sortida.append(client)
        return sortida