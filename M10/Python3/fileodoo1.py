import psycopg2
try:
    conn = psycopg2.connect("dbname='m1' user='odoo' host='172.17.0.1' password='odoo'")
    cur = conn.cursor()
    cur.execute("SELECT * from res_users where id>%s",(1,))
    rows = cur.fetchall()
    for row in rows:
        print (row)
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()
cur.execute("DROP TABLE IF EXISTS Cars")
cur.execute("CREATE TABLE Cars(Id INT PRIMARY KEY, Name TEXT, Price INT)")
cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num smallint, data varchar);")
cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (100, "abcdef"))
cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (111, "abcdef"))
query = "INSERT INTO Cars (Id, Name, Price) VALUES (%s, %s, %s)"
cars = (
(1, 'Audi', 52642),
(2, 'Mercedes', 57127),
(3, 'Skoda', 9000),
(4, 'Volvo', 29000),
(5, 'Bentley', 350000),
(6, 'Citroen', 21000),
(7, 'Hummer', 41400),
(8, 'Volkswagen', 21600)
)
cur.executemany(query, cars)