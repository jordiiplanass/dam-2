import psycopg2

PSQL_HOST = "localhost"
PSQL_PORT = "5432"
PSQL_USER = "pepito2"
PSQL_PASS = "pepito2"
PSQL_DB = "database1"

try:
    conn = psycopg2.connect("dbname='m1' user='odoo' host='172.17.0.1' password='odoo'")
    cur = conn.cursor()
    cur.execute("SELECT * from res_users where id>%s",(1,))
    rows = cur.fetchall()
    for row in rows:
        print (row) 
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()

cur.execute("DROP TABLE IF EXISTS Cars")
cur.execute("CREATE TABLE Cars(Id INT PRIMARY KEY, Name TEXT, Price INT)")
cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num smallint, data varchar);")
cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (100, "abcdef"))
cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (111, "abcdef"))

connection_address = """
host=%s port=%s user=%s password=%s dbname=%s
""" % (PSQL_HOST, PSQL_PORT, PSQL_USER, PSQL_PASS, PSQL_DB)

connection = psycopg2.connect(connection_address)

cursor = connection.cursor()

SQL = "SELECT * FROM taula1;"
cursor.execute(SQL)

all_values = cursor.fetchall()

cursor.close()
connection.close()

print ('Get values: ',all_values)