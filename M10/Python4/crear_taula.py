import psycopg2

# MADE BY JORDI PLANAS
# iam39426921

PSQL_HOST = '172.17.0.1'
PSQL_PORT = "5432"
PSQL_USER = 'odoo'
PSQL_PASS = 'odoo'
PSQL_DB = 'postgres'

comanda = '''
DROP TABLE IF EXISTS Client;
CREATE TABLE IF NOT EXISTS Client
(
    identificador varchar,
    nom varchar,
    cognom varchar,
    telefon varchar,
    correu varchar,
    dreça varchar,
    ciutat varchar
);
'''
try:
    conn = psycopg2.connect(dbname=PSQL_DB, user=PSQL_USER, host=PSQL_HOST, password=PSQL_PASS)
    cur = conn.cursor()

    cur.execute(comanda)
    
    rows = cur.fetchall()
    for row in rows:
        print (row)
    conn.commit()

except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()