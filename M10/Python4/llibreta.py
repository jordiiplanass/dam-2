import uuid
import psycopg2
import client

PSQL_HOST = '172.17.0.1'
PSQL_PORT = "5432"
PSQL_USER = 'odoo'
PSQL_PASS = 'odoo'
PSQL_DB = 'postgres'

class Llibreta:
    llista_clients = []
    id_client = 0

    def __init__(self):
        self.llista_clients = []
        self.id_client = ""

    def __init__(self,llista_clients):
        self.llista_clients = llista_clients

    def __init__(self,llista_clients,id_client):
        self.llista_clients = llista_clients
        self.id_client = id_client

    def get_llista_clients(self):
        self.realitzar_comanda(self, "SELECT * FROM client;")

    def get_llista_clients_ordenats(self):
        self.realitzar_comanda(self, "SELECT * FROM client ORDER BY nom;")

    def afegir_client(self, nom, cognom, telefon, correu, adreça, ciutat):                                                      # cognom, telefon, correu, adreça, ciutat): 
        self.realitzar_comanda(self, "insert into client (identificador, nom, cognom, telefon, correu, adreça, ciutat) values (" + nom + ", " + cognom + ", "+ telefon + ", "+ correu + ", "+ adreça + ", "+ ciutat + ");")

    def eliminar_client(self, identificador):
        self.realitzar_comanda(self, "DELETE FROM client WHERE identificador = (%s);", (identificador,))

    def cercar_per_id(self, identificador):
        self.realitzar_comanda(self, "SELECT * FROM client WHERE identificador = (%s);", (identificador,))

    def cercar_per_nom(self, nom):
        self.realitzar_comanda(self, "SELECT * FROM client WHERE nom = (%s);", (nom,))

    def cercar_per_cognom(self, cognom):
        self.realitzar_comanda(self, "SELECT * FROM client WHERE cognom = (%s);", (cognom,))
    
    def actualitzar_parametre(self, nom_parametre, nou_parametre, identificador):
        self.realitzar_comanda(self, "UPDATE Client SET (%s) = (%s) WHERE identificador = (%s);", (nom_parametre, nou_parametre, identificador,))
        self.realitzar_comanda(self, "SELECT (%s) FROM Client Where identificador = (%s)", (nou_parametre, identificador,))

    def realitzar_comanda(self, comanda):
        try:
            conn = psycopg2.connect(dbname=PSQL_DB, user=PSQL_USER, host=PSQL_HOST, password=PSQL_PASS)
            cur = conn.cursor()

            
            cur.execute(comanda)
            
            rows = cur.fetchall()
            for row in rows:
                print (row)
            conn.commit()

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
