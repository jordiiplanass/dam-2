import llibreta
class Menu:
    llibreta = llibreta.Llibreta


    def __init__(self):
        self.llibreta

    def __init__(self, llibreta):
        self.llibreta = llibreta

def mostrar_menu_principal():
    textMenu = """MENU PRINCIPAL:
=================================
Selecciona una opció i prem Intro
=================================
    1. Afegir client
    2. Eliminar client
    3. Consultar client
    4. Modificar un camp d'un client (*)
    5. Sortir

Enter an option:"""
    return int(input(textMenu))

def mostrar_menu_consulta():
    textMenu = """MENU CONSULTA:
=================================
Selecciona una opció i prem Intro
=================================
    1. Cercar client per Identificador
    2. Cercar client per Nom
    3. Cercar client per Cognom
    4. Llistar tots els clients
    5. Llistar tots els clients per nom
    6. Enrere

Enter an option:"""
    return int(input(textMenu))