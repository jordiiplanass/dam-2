# DAM-2

## - M03:
[M03 - Programación (Bloque II)](../M03)
<details closed>
<summary>About</summary>
<a href="https://moodle.escoladeltreball.org/course/view.php?id=594"><b>Moodle</b></a>
</details>

## - M05:
[Entornos de desarrollo](../M05)
<details closed>
<summary>About</summary>
<a href="https://moodle.escoladeltreball.org/course/view.php?id=885"><b>Moodle</b></a>
</details>

## - M06:
[Acceso a datos](../M06)
<details closed>
<summary>About</summary>
<a href="https://moodle.escoladeltreball.org/course/view.php?id=1680"><b>Moodle</b></a><br/>
<a href="https://github.com/gerardfp"><b>GitHub</b></a><br/>
<a href="https://gerardfp.github.io/dam-m6/"><b>GitHub IO</b></a>
</details>

## - M08:
[Programación multimedia y dispositivos móviles](../M08)
<details closed>
<summary>About</summary>
<a href="https://moodle.escoladeltreball.org/course/view.php?id=2007"><b>Moodle</b></a><br/>
<a href="https://www.youtube.com/results?search_query=joanseculi"><b>Youtube</b></a>
</details>

## - M10:
[Sistemas de gestión empresarial](../M10)
<details closed>
<summary>About</summary>
<a href="https://moodle.escoladeltreball.org/course/view.php?id=885"><b>Moodle</b></a>
</details>






